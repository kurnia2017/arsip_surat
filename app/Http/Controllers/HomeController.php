<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\Disposisi;
use App\SuratKeluar;
use App\SuratMasuk;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
    {
        $surat_keluar = SuratKeluar::orderBy('created_at', 'desc')->count();
        $surat_masuk = SuratMasuk::orderBy('created_at', 'desc')->count();
        $disposisi = Disposisi::orderBy('created_at', 'desc')->count();

      return view('welcome-user', compact('surat_keluar', 'surat_masuk', 'disposisi'));
    }


    public function memounreadPrint($id)
    {   
        $disposisi = Disposisi::where('disposisis.id', $id)
                                ->leftjoin('bagians', 'bagians.id', '=', 'disposisis.id_bagian')
                                ->leftjoin('surat_masuks', 'surat_masuks.id', '=', 'disposisis.surat_id')
                                ->first();
        $pdf = PDF::loadView('admin.memo.printdetailunread', compact('disposisi'))
        ->setPaper('a4', 'portrait');
        return $pdf->download('invoice.pdf');
    }
}
