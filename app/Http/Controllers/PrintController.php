<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SuratMasuk;
use App\SuratKeluar;

class PrintController extends Controller
{
 	public function printreal(Request $request)
	{
		// print_r($request->all());
		// exit();
		// $tanggal_awal = $request->get('tgl_awal');
		// $tanggal_akhir = $request->get('tgl_akhi');
		$surat_masuks = SuratMasuk::leftJoin('list_kategoris', 'list_kategoris.nama', '=', 'surat_masuks.nm_ls_kategori')
									->leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_masuks.id_sifat_surat')
									->leftJoin('jenis_surats', 'jenis_surats.id', '=', 'surat_masuks.id_jenis_surat')
									->orderBy('surat_masuks.created_at', 'desc')

									->select(
												'surat_masuks.id',
												'surat_masuks.no_agenda',
												'surat_masuks.no_surat',
												'surat_masuks.tgl_pengirim',
												'surat_masuks.tgl_penerima',
												'surat_masuks.sumber',
												'surat_masuks.perihal',
												'surat_masuks.lampiran',
												'surat_masuks.keterangan',
												'surat_masuks.file',
												'surat_masuks.status',
												'list_kategoris.kode',
												'jenis_surats.jenis_surat',
												'sifat_surats.sifat_surat'
											)
									->get();
	// 	$start_page= (($surat_masuks->currentPage()-1) * 10) + 1;
	// 	foreach ($surat_masuks as $key => $surat_masuk) {
	// 		if ($surat_masuk->tgl_penerima >= $tanggal_awal) {
	// 			if ($surat_masuk->tgl_penerima <= $tanggal_akhir) {
	// 				$surat_masuk_looping[] = $surat_masuk; 
	// 			}
	// 		}
	// 	}
	// $surat_masuks = $surat_masuk_looping;

		return view('admin.laporan.printlaporansuratmasuk', compact('surat_masuks'));
     }

 	public function prnsuratkeluar()
	{

     $surat_keluars = SuratKeluar:: leftJoin('users', 'users.id', '=', 'surat_keluars.id_pengolah')
									->leftJoin('penanggung_jawabs', 'penanggung_jawabs.id', '=', 'surat_keluars.id_penanggung_jawab')
									->leftJoin('jenis_surats', 'jenis_surats.id', '=', 'surat_keluars.id_jenis_surat')
									->leftJoin('list_kategoris', 'list_kategoris.nama', '=', 'surat_keluars.nm_ls_kategori')
									->leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_keluars.id_sifat_surat')
									->orderBy('surat_keluars.created_at', 'desc')
									->select(
												'surat_keluars.id',
												'surat_keluars.no_surat',
												'surat_keluars.tgl_surat',
												'surat_keluars.id_pengolah',
												'surat_keluars.tujuan',
												'surat_keluars.perihal',
												'surat_keluars.lampiran',
												'surat_keluars.keterangan',
												'surat_keluars.file',
												'users.name',
												'list_kategoris.kode',
												'penanggung_jawabs.nama as nama_penanggungjawab',
												'jenis_surats.jenis_surat',
												'sifat_surats.sifat_surat'
											)
									->get();
		return view('admin.laporan.printlaporansuratkeluar', compact('surat_keluars'));

	}	

public function prndisposisi(Request $request)
	{
		// print_r($request->all());
		// exit();
		// $tanggal_awal = $request->get('tgl_awal');
		// $tanggal_akhir = $request->get('tgl_akhi');
		$surat_masuks = SuratMasuk::leftJoin ('jenis_surats', 'jenis_surats.id', '=', 'surat_masuks.id_jenis_surat')
									->leftJoin('kategoris', 'kategoris.id', '=', 'surat_masuks.id_kategori')
									->leftJoin('list_kategoris', 'list_kategoris.nama', '=', 'surat_masuks.nm_ls_kategori')
									->leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_masuks.id_sifat_surat')
									->where('surat_masuks.status', 3)
									->select(
												'surat_masuks.id',
												'surat_masuks.no_agenda',
												'surat_masuks.no_surat',
												'surat_masuks.tgl_pengirim',
												'surat_masuks.tgl_penerima',
												'surat_masuks.sumber',
												'surat_masuks.keterangan',
												'surat_masuks.file',
												'surat_masuks.status',
												'list_kategoris.kode',
												'jenis_surats.jenis_surat',
												'sifat_surats.sifat_surat'
											)
									->get();
	// 	$start_page= (($surat_masuks->currentPage()-1) * 10) + 1;
	// 	foreach ($surat_masuks as $key => $surat_masuk) {
	// 		if ($surat_masuk->tgl_penerima >= $tanggal_awal) {
	// 			if ($surat_masuk->tgl_penerima <= $tanggal_akhir) {
	// 				$surat_masuk_looping[] = $surat_masuk; 
	// 			}
	// 		}
	// 	}
	// $surat_masuks = $surat_masuk_looping;

		return view('admin.laporan.printlaporandisposisi', compact('surat_masuks'));
     }
	
	
}
