<?php

namespace App\Http\Controllers\admin;

use DB;
use View;
use Auth;
use Image;
use File;
use Response;
use App\Bagian;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class BagianController extends Controller
{
   public function __construct()
	 {
	    $this->middleware('auth');
	 }
	  
	public function index()
	{
		$bagians = Bagian::orderBy('created_at', 'desc');
		$bagians=$bagians->paginate(10);
		$start_page= (($bagians->currentPage()-1) * 10) + 1;
		return view('admin.bagians.index',array('bagians' => $bagians, 'start_page'=>$start_page));
	} 

	public function create()
	{
	    return View::make('admin.bagians.create');
	}

	public function store(Request $request)
	{

		// $this->validate($request, [
		// 'nama'          => 'required|unique:surat_masuks|max:255',
		// ]);
		// store
		$bagians = new Bagian;
		$bagians->nama_bagian       = Input::get('nama_bagian');
		$bagians->nama_kepala        = Input::get('nama_kepala');
		$bagians->save();

		return Redirect::action('admin\BagianController@index');
	}

	public function show($id)
	{
		$bagians = Bagian::findOrFail($id);
		return view('admin.bagians.show', array('bagians' => $bagians));
	}

	public function edit($id)
	{
		$bagians = Bagian::findOrFail($id);     
		return view('admin.bagians.edit', array('bagians' => $bagians));
	}

	public function update(Request $request, $id)
	{

		// $this->validate($request, [
		// 'name'          	=> 'required|unique:surat_masuks|max:255',
		// 'unit'          	=> 'required|unique:surat_masuks|max:255',
		// ]);   

		$bagians = Bagian::findOrFail($id);
		$bagians->nama_bagian				= Input::get('nama_bagian');
		$bagians->nama_kepala      			= Input::get('nama_kepala');
		$bagians->save();

		// redirect
		return Redirect::action('admin\BagianController@index');
	}

	public function destroy($id)
	{    
		$bagians = Bagian::findOrFail($id);
		$bagians->delete();
		return Redirect::action('admin\BagianController@index')->with('flash-success','The user has been deleted.');
	}

	public function search(Request $request)
	{
		$bagians = Bagian::orderBy('created_at', 'desc');
		if($request->has('search')){
	    $search = $request->get('search');
	    $bagians = $bagians->Where("bagians.nama_bagian", "like", "%".$search."%")
	                       ->orWhere("bagians.nama_kepala", "like", "%".$search."%");
	    }
	    $bagians = $bagians->paginate(10);

	    $start_page= (($bagians->currentPage()-1) * 10) + 1;
		return view('admin.bagians.index', compact('bagians','start_page'));
	}
}
