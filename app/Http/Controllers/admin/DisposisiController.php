<?php

namespace App\Http\Controllers\admin;

use DB;
use View;
use Auth;
use Response;
use App\Disposisi;
use App\SuratMasuk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;


class DisposisiController extends Controller
{
   public function store(Request $request, $id)
	{
		// store
		$bagians = new Disposisi;
		$bagians->id_bagian       = Input::get('id');
		$bagians->catatan        = Input::get('catatan');
		$bagians->save();

		$bagians = SuratMasuk::findOrFail($id);
		$bagians->status				= Input::get('disposisi');
		$bagians->save();

		return Redirect::action('admin\LurahController@index');
	}

	public function laporansurat()
	{
		$surat_masuks = SuratMasuk::leftJoin ('jenis_surats', 'jenis_surats.id', '=', 'surat_masuks.id_jenis_surat')
									->leftJoin('kategoris', 'kategoris.id', '=', 'surat_masuks.id_kategori')
									->leftJoin('list_kategoris', 'list_kategoris.nama', '=', 'surat_masuks.nm_ls_kategori')
									->leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_masuks.id_sifat_surat')
									->where('surat_masuks.status', 3)
									->select(
												'surat_masuks.id',
												'surat_masuks.no_agenda',
												'surat_masuks.no_surat',
												'surat_masuks.tgl_pengirim',
												'surat_masuks.tgl_penerima',
												'surat_masuks.sumber',
												'surat_masuks.keterangan',
												'surat_masuks.file',
												'surat_masuks.status',
												'list_kategoris.kode',
												'jenis_surats.jenis_surat',
												'sifat_surats.sifat_surat'
											);
		$tgls_awal = SuratMasuk::all()->toArray();
    	$tgls_awal = collect(array_merge(['' => ['id' => '','tgl_penerima' => 'Pilih tgl asal']], $tgls_awal));
    	$tgls_awals  = $tgls_awal->pluck('tgl_penerima', 'tgl_penerima');

    	$tgls_akhi = SuratMasuk::all()->toArray();
    	$tgls_akhi = collect(array_merge(['' => ['id' => '','tgl_penerima' => 'Pilih tgl tujuan']], $tgls_akhi));
    	$tgls_akhir  = $tgls_akhi->pluck('tgl_penerima', 'tgl_penerima');
		$surat_masuks=$surat_masuks->paginate(10);
		
/*		echo "<pre>";
		print_r($surat_masuks);
		echo "</pre>";
		exit();*/
		$start_page= (($surat_masuks->currentPage()-1) * 10) + 1;
		return view('admin.Laporan.laporandisposisi',array('tgls_awals'=>$tgls_awals,'tgls_akhir'=>$tgls_akhir,'surat_masuks' => $surat_masuks, 'start_page'=>$start_page));
	} 

	public function search(Request $request)
	{
		$surat_masuks = SuratMasuk::leftJoin ('jenis_surats', 'jenis_surats.id', '=', 'surat_masuks.id_jenis_surat')
									->leftJoin('kategoris', 'kategoris.id', '=', 'surat_masuks.id_kategori')
									->leftJoin('list_kategoris', 'list_kategoris.nama', '=', 'surat_masuks.nm_ls_kategori')
									->leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_masuks.id_sifat_surat')
									->where('surat_masuks.status', 3)
									->select(
												'surat_masuks.id',
												'surat_masuks.no_agenda',
												'surat_masuks.no_surat',
												'surat_masuks.tgl_pengirim',
												'surat_masuks.tgl_penerima',
												'surat_masuks.sumber',
												'surat_masuks.keterangan',
												'surat_masuks.file',
												'surat_masuks.status',
												'list_kategoris.kode',
												'jenis_surats.jenis_surat',
												'sifat_surats.sifat_surat'
											);
	if($request->has('search')){
    $search = $request->get('search');
    $surat_masuks = $surat_masuks->Where("surat_masuks.no_agenda", "like", "%".$search."%")
                     			->orWhere("surat_masuks.no_surat", "like", "%".$search."%")
                     			->orWhere("surat_masuks.tgl_penerima", "like", "%".$search."%")
                     			->orWhere("surat_masuks.tgl_pengirim", "like", "%".$search."%")
                     			->orWhere("surat_masuks.sumber", "like", "%".$search."%")
                     			->orWhere("surat_masuks.keterangan", "like", "%".$search."%")
                     			->orWhere("surat_masuks.status", "like", "%".$search."%")
                     			->orWhere("jenis_surats.jenis_surat", "like", "%".$search."%")
                     			->orWhere("sifat_surats.sifat_surat", "like", "%".$search."%");
    }
    $surat_masuks = $surat_masuks->paginate(10);

    $start_page= (($surat_masuks->currentPage()-1) * 10) + 1;
		return view('admin.Laporan.laporandisposisi', compact('surat_masuks','start_page'));
	}

		public function searchkategori(Request $request)
	{
		$kategori = Kategori::pluck('nama', 'nama');
		$surat_masuks = SuratMasuk::leftJoin('jenis_surats', 'jenis_surats.id', '=', 'surat_masuks.id_jenis_surat')
									->leftJoin('kategoris', 'kategoris.id', '=', 'surat_masuks.id_kategori')
									->leftJoin('list_kategoris', 'list_kategoris.nama', '=', 'surat_masuks.nm_ls_kategori')
									->leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_masuks.id_sifat_surat')
									->orderBy('surat_masuks.created_at', 'desc')
									->select(
												'surat_masuks.id',
												'surat_masuks.no_agenda',
												'surat_masuks.no_surat',
												'surat_masuks.tgl_pengirim',
												'surat_masuks.tgl_penerima',
												'surat_masuks.sumber',
												'surat_masuks.perihal',
												'surat_masuks.lampiran',
												'surat_masuks.keterangan',
												'surat_masuks.file',
												'surat_masuks.status',
												'kategoris.nama as nama_kategori',
												'kategoris.kode_kategori',
												'list_kategoris.nama as nama_list',
												'list_kategoris.kode',
												'jenis_surats.jenis_surat',
												'sifat_surats.sifat_surat'
											);
	if($request->has('search')){
    $search = $request->get('search');
    $surat_masuks = $surat_masuks->Where("kategoris.nama", "like", "%".$search."%");
    }
    $surat_masuks = $surat_masuks->paginate(10);

    $start_page= (($surat_masuks->currentPage()-1) * 10) + 1;
		return view('admin.surat_masuks.index', compact('surat_masuks','start_page','kategori'));
	}

public function searchlaporandisposisi(Request $request)
	{	

		// print_r('wjwjwjw');
		// exit();
		$tanggal_awal = $request->get('tgls_awal');
		$tanggal_akhir = $request->get('tgl_akhir');
		// echo "<pre>";
		// print_r($tanggal_awal);
		// // print_r($request->all());
		// echo "</pre>";
		// print_r($tanggal_akhir);
		// exit();

		$todayDate = Date("d-m-Y");
		$surat_masuks = SuratMasuk::leftJoin('list_kategoris', 'list_kategoris.nama', '=', 'surat_masuks.nm_ls_kategori')
									->leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_masuks.id_sifat_surat')
									->leftJoin('jenis_surats', 'jenis_surats.id', '=', 'surat_masuks.id_jenis_surat')
									->orderBy('surat_masuks.created_at', 'desc')
									->select(
												'surat_masuks.id',
												'surat_masuks.no_agenda',
												'surat_masuks.no_surat',
												'surat_masuks.tgl_pengirim',
												'surat_masuks.tgl_penerima',
												'surat_masuks.sumber',
												'surat_masuks.perihal',
												'surat_masuks.lampiran',
												'surat_masuks.keterangan',
												'surat_masuks.file',
												'surat_masuks.status',
												'jenis_surats.jenis_surat',
												'sifat_surats.sifat_surat'
											);
									// ->get();

	$surat_masuks=$surat_masuks->paginate(10);
    $start_page= (($surat_masuks->currentPage()-1) * 10) + 1;
		foreach ($surat_masuks as $key => $surat_masuk) {
			if ($surat_masuk->tgl_penerima >= $tanggal_awal) {
				if ($surat_masuk->tgl_penerima <= $tanggal_akhir) {
					$surat_masuk_looping[] = $surat_masuk; 
				}
			}
		}
	$surat_masuks = $surat_masuk_looping;
		// echo "<pre>";
		// print_r($surat_masuks);
		// // print_r($request->all());
		// echo "</pre>";
		// exit();

	$tgls_awal = SuratMasuk::all()->toArray();
    $tgls_awal = collect(array_merge(['' => ['id' => '','tgl_penerima' => 'Pilih tgl asal']], $tgls_awal));
    $tgls_awals  = $tgls_awal->pluck('tgl_penerima', 'tgl_penerima');

    $tgls_akhi = SuratMasuk::all()->toArray();
    $tgls_akhi = collect(array_merge(['' => ['id' => '','tgl_penerima' => 'Pilih tgl tujuan']], $tgls_akhi));
    $tgls_akhir  = $tgls_akhi->pluck('tgl_penerima', 'tgl_penerima');

	return view('admin.Laporan.laporandisposisi', compact('surat_masuks','start_page', 'todayDate', 'tgls_awals', 'tgls_akhir'));
	}
	
}
