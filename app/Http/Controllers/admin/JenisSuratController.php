<?php

namespace App\Http\Controllers\admin;

use DB;
use View;
use Auth;
use Response;
use App\JenisSurat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;


class JenisSuratController extends Controller
{
    public function __construct()
	 {
	    $this->middleware('auth');
	 }
	  
	public function index()
	{
		$jenis_surats = JenisSurat::orderBy('created_at', 'desc');
		$jenis_surats=$jenis_surats->paginate(10);
		$start_page= (($jenis_surats->currentPage()-1) * 10) + 1;

		return view('admin.jenis_surat.index',array('jenis_surats' => $jenis_surats, 'start_page'=>$start_page));
	} 

	public function create()
	{
	    return View::make('admin.jenis_surat.create');
	}

	public function store(Request $request)
	{
/*		$this->validate($request, [
		'nama'          => 'required|unique:jenis_surats|max:255',
		]);
	*/	// store
		$jenis_surats = new JenisSurat;
		$jenis_surats->jenis_surat     		= Input::get('jenis_surat');
		$jenis_surats->save();

		// redirect
		return Redirect::action('admin\JenisSuratController@index');
	}

	public function show($id)
	{
		$jenis_surats = JenisSurat::findOrFail($id);
		return view('admin.jenis_surat.show', array('jenis_surats' => $jenis_surats));
	}

	public function edit($id)
	{
		$jenis_surats = JenisSurat::findOrFail($id);     
		return view('admin.jenis_surat.edit', array('jenis_surats' => $jenis_surats));
	}

	public function update(Request $request, $id)
	{
		// $this->validate($request, [
		// 'name'          	=> 'required|unique:perihals|max:255',
		// 'unit'          	=> 'required|unique:perihals|max:255',
		// ]);   

		$jenis_surats = JenisSurat::findOrFail($id);
		$jenis_surats->jenis_surat     		= Input::get('jenis_surat');
		$jenis_surats->save();

		// redirect
		return Redirect::action('admin\JenisSuratController@index');
	}

	public function destroy($id)
	{    
		$jenis_surats = JenisSurat::findOrFail($id);
		$jenis_surats->delete();
		return Redirect::action('admin\JenisSuratController@index')->with('flash-success','The user has been deleted.');
	}

	public function search(Request $request)
	{
		$search = $request->get('search');
		$jenis_surats = JenisSurat::where('jenis_surat','LIKE','%'.$search.'%');
		$jenis_surats = $jenis_surats->paginate(10);

		$start_page= (($jenis_surats->currentPage()-1) * 10) + 1;
		return view('admin.jenis_surat.index', compact('jenis_surats','start_page'));
	}
}