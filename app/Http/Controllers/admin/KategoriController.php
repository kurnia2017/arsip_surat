<?php

namespace App\Http\Controllers\admin;

use DB;
use View;
use Auth;
use Image;
use File;
use Response;
use App\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class KategoriController extends Controller
{
    public function __construct()
	 {
	    $this->middleware('auth');
	 }
	  
	public function index()
	{
		$kategoris = Kategori::orderBy('created_at', 'desc');
		$kategoris=$kategoris->paginate(10);
		$start_page= (($kategoris->currentPage()-1) * 10) + 1;
		return view('admin.kategoris.index',array('kategoris' => $kategoris, 'start_page'=>$start_page));
	} 

	public function create()
	{
	    return View::make('admin.kategoris.create');
	}

	public function store(Request $request)
	{

		// $this->validate($request, [
		// 'nama'          => 'required|unique:surat_masuks|max:255',
		// ]);
		// store
		$kategoris = new Kategori;
		$kategoris->nama 			= Input::get('nama');
		$kategoris->kode_kategori   = Input::get('kode_kategori');
		$kategoris->save();

		return Redirect::action('admin\KategoriController@index');
	}

	public function show($id)
	{
		$kategoris = Kategori::findOrFail($id);
		return view('admin.kategoris.show', array('kategoris' => $kategoris));
	}

	public function edit($id)
	{
		$kategoris = Kategori::findOrFail($id);     
		return view('admin.kategoris.edit', array('kategoris' => $kategoris));
	}

	public function update(Request $request, $id)
	{

		// $this->validate($request, [
		// 'name'          	=> 'required|unique:surat_masuks|max:255',
		// 'unit'          	=> 'required|unique:surat_masuks|max:255',
		// ]);   

		$kategoris = Kategori::findOrFail($id);
		$kategoris->nama     		= Input::get('nama');
		$kategoris->kode_kategori   = Input::get('kode_kategori');
		$kategoris->save();

		// redirect
		return Redirect::action('admin\KategoriController@index');
	}

	public function destroy($id)
	{    
		$kategoris = Kategori::findOrFail($id);
		$kategoris->delete();
		return Redirect::action('admin\KategoriController@index')->with('flash-success','The user has been deleted.');
	}

	public function search(Request $request)
	{
		$kategoris = Kategori::orderBy('created_at', 'desc');
		if($request->has('search')){
	    $search = $request->get('search');
	    $kategoris = $kategoris->Where("kategoris.id", "like", "%".$search."%")
	                       ->orWhere("kategoris.nama", "like", "%".$search."%")
	                       ->orWhere("kategoris.kode_kategori", "like", "%".$search."%");
	    }
	    $kategoris = $kategoris->paginate(10);

	    $start_page= (($kategoris->currentPage()-1) * 10) + 1;
		return view('admin.kategoris.index', compact('kategoris','start_page'));
	}
    
}
