<?php

namespace App\Http\Controllers\admin;

use DB;
use View;
use Auth;
use Response;
use App\KodeSurat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;


class KodeSuratController extends Controller
{
	public function __construct()
	 {
	    $this->middleware('auth');
	 }
	  
	public function index()
	{
		$kode_surats = KodeSurat::orderBy('created_at', 'desc');
		$kode_surats=$kode_surats->paginate(10);
		$start_page= (($kode_surats->currentPage()-1) * 10) + 1;

		return view('admin.kode_surats.index',array('kode_surats' => $kode_surats, 'start_page'=>$start_page));
	} 

	public function create()
	{
	    return View::make('admin.kode_surats.create');
	}

	public function store(Request $request)
	{
		$this->validate($request, [
		'nama'          => 'required|unique:kode_surats|max:255',
		]);
		// store
		$kode_surats = new KodeSurat;
		$kode_surats->nama     		  = Input::get('nama');

		$kode_surats->no_kode_surat       = Input::get('no_kode_surats');
		$kode_surats->save();

		// redirect
		return Redirect::action('admin\KodeSuratController@index');
	}

	public function show($id)
	{
		$kode_surats = KodeSurat::findOrFail($id);
		return view('admin.kode_surats.show', array('kode_surats' => $kode_surats));
	}

	public function edit($id)
	{
		$kode_surats = KodeSurat::findOrFail($id);     
		return view('admin.kode_surats.edit', array('kode_surats' => $kode_surats));
	}

	public function update(Request $request, $id)
	{
		// $this->validate($request, [
		// 'name'          	=> 'required|unique:perihals|max:255',
		// 'unit'          	=> 'required|unique:perihals|max:255',
		// ]);   

		$kode_surats = KodeSurat::findOrFail($id);
		$kode_surats->nama     		 	= Input::get('nama');
		$kode_surats->no_kode_surats 	= Input::get('no_kode_surat');
		$kode_surats->save();

		// redirect
		return Redirect::action('admin\KodeSuratController@index');
	}

	public function destroy($id)
	{    
		$kode_surats = KodeSurat::findOrFail($id);
		$kode_surats->delete();
		return Redirect::action('admin\KodeSuratController@index')->with('flash-success','The user has been deleted.');
	}

	public function search(Request $request)
	{
		$search = $request->get('search');
		$kode_surats = KodeSurat::where('nama','LIKE','%'.$search.'%');
		$kode_surats = $kode_surats->paginate(10);

		$start_page= (($kode_surats->currentPage()-1) * 10) + 1;
		return view('admin.kode_surats.index', compact('kode_surats','start_page'));
	}
}