<?php

namespace App\Http\Controllers\admin;

use DB;
use View;
use Auth;
use Image;
use File;
use Response;
use App\ListKategori;
use App\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;;

class ListKategoriController extends Controller
{
    public function __construct()
	 {
	    $this->middleware('auth');
	 }
	  
	public function index()
	{
		$list_kategoris = ListKategori::leftjoin('kategoris', 'kategoris.id', '=', 'list_kategoris.id_kategori') 
										->orderBy('created_at', 'desc')
										->select(
													'list_kategoris.*', 
													'Kategoris.nama as name', 
													'kategoris.kode_kategori'
												);
		$list_kategoris=$list_kategoris->paginate(10);
		$start_page= (($list_kategoris->currentPage()-1) * 10) + 1;
 		// echo "<pre>";
 		// print_r(response()->json($list_kategoris));
 		// echo "</pre>";
 		// exit();
		return view('admin.list_kategoris.index',array('list_kategoris' => $list_kategoris, 'start_page'=>$start_page));
	} 

	public function create()
	{
	    $kategori = Kategori::pluck('nama', 'id');
	    return View::make('admin.list_kategoris.create', compact('kategori'));
	}

	public function store(Request $request)
	{
		/*echo "<pre>";
		print_r ($request->all());
		echo "</pre>";
		exit();*/

		// $this->validate($request, [
		// 'nama'          => 'required|unique:surat_masuks|max:255',
		// ]);
		// store
		$list_kategoris = new ListKategori;
		$list_kategoris->nama 				= Input::get('nama');
		$list_kategoris->kode 				= Input::get('kode');
		$list_kategoris->id_kategori 		= Input::get('id');
		$list_kategoris->save();

		return Redirect::action('admin\ListKategoriController@index');
	}

	public function show($id)
	{
		$list_kategoris = ListKategori::findOrFail($id);
		return view('admin.list_kategoris.show', array('list_kategoris' => $list_kategoris));
	}

	public function edit($id)
	{
		$list_kategoris = ListKategori::findOrFail($id);     
		return view('admin.list_kategoris.edit', array('list_kategoris' => $list_kategoris));
	}

	public function update(Request $request, $id)
	{

		// $this->validate($request, [
		// 'name'          	=> 'required|unique:surat_masuks|max:255',
		// 'unit'          	=> 'required|unique:surat_masuks|max:255',
		// ]);   

		$list_kategoris = ListKategori::findOrFail($id);
		$list_kategoris->nama 				= Input::get('nama');
		$list_kategoris->kode 				= Input::get('kode');
		$list_kategoris->id_kategori 		= Input::get('id_kategori');
		$list_kategoris->save();

		// redirect
		return Redirect::action('admin\ListKategoriController@index');
	}

	public function destroy($id)
	{    
		$list_kategoris = ListKategori::findOrFail($id);
		$list_kategoris->delete();
		return Redirect::action('admin\ListKategoriController@index')->with('flash-success','The user has been deleted.');
	}

	public function search(Request $request)
	{
		$list_kategoris = ListKategori::leftjoin('kategoris', 'kategoris.id', '=', 'list_kategoris.id_kategori') 
										->orderBy('created_at', 'desc')
										->select(
													'list_kategoris.*', 
													'Kategoris.nama as name', 
													'kategoris.kode_kategori'
												);
		if($request->has('search')){
	    $search = $request->get('search');
	    $list_kategoris = $list_kategoris->Where("list_kategoris.nama", "like", "%".$search."%")
	                       ->orWhere("list_kategoris.kode", "like", "%".$search."%");
	    }
	    $list_kategoris = $list_kategoris->paginate(10);

	    $start_page= (($list_kategoris->currentPage()-1) * 10) + 1;
		return view('admin.list_kategoris.index', compact('list_kategoris','start_page'));
	}

	public function ajax_getcitbyprov(Request $request)
    { 
 		$list_kategoris = ListKategori::where('id_kategori','=',$request->get('id_kategori'))->get();
    return response()->json($list_kategoris);

  	}
}
