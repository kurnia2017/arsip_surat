<?php

namespace App\Http\Controllers\admin;
use DB;
use View;
use Auth;
use Image;
use File;
use Response;
use App\SuratMasuk;
use App\SuratKeluar;
use App\kategori;
use App\Bagian;
use App\Tanggapan;
use App\Disposisi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class LurahController extends Controller
{
   public function index()
	{
		$kategori = Kategori::pluck('nama', 'nama');
		$surat_masuks = SuratMasuk::leftJoin('jenis_surats', 'jenis_surats.id', '=', 'surat_masuks.id_jenis_surat')
									->leftJoin('kategoris', 'kategoris.id', '=', 'surat_masuks.id_kategori')
									->leftJoin('list_kategoris', 'list_kategoris.nama', '=', 'surat_masuks.nm_ls_kategori')
									->leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_masuks.id_sifat_surat')
									->orderBy('surat_masuks.created_at', 'desc')
									->select(
												'surat_masuks.id',
												'surat_masuks.no_agenda',
												'surat_masuks.no_surat',
												'surat_masuks.tgl_pengirim',
												'surat_masuks.tgl_penerima',
												'surat_masuks.sumber',
												'surat_masuks.perihal',
												'surat_masuks.lampiran',
												'surat_masuks.keterangan',
												'surat_masuks.file',
												'surat_masuks.status',
												'kategoris.nama as nama_kategori',
												'kategoris.kode_kategori',
												'list_kategoris.nama as nama_list',
												'list_kategoris.kode',
												'jenis_surats.jenis_surat',
												'sifat_surats.sifat_surat'
											);

		$surat_masuks=$surat_masuks->paginate(10);
		$start_page= (($surat_masuks->currentPage()-1) * 10) + 1;
		return view('admin.lurah.index', compact('kategori'),array('surat_masuks' => $surat_masuks, 'start_page'=>$start_page));
	} 
	   public function index1()
	{
		$kategori = Kategori::pluck('nama', 'nama');
		$surat_keluars = SuratKeluar::leftJoin('users', 'users.id', '=', 'surat_keluars.id_pengolah')
									->leftJoin('jenis_surats', 'jenis_surats.id', '=', 'surat_keluars.id_jenis_surat')
									->leftJoin('kategoris', 'kategoris.id', '=', 'surat_keluars.id_kategori')
									->leftJoin('list_kategoris', 'list_kategoris.nama', '=', 'surat_keluars.nm_ls_kategori')
									->leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_keluars.id_sifat_surat')
									->leftJoin('penanggung_jawabs', 'penanggung_jawabs.id', '=', 'surat_keluars.id_penanggung_jawab')
									->orderBy('surat_keluars.created_at', 'desc')
									->select(
												'surat_keluars.id',
												'surat_keluars.no_surat',
												'surat_keluars.tgl_surat',
												'surat_keluars.id_pengolah',
												'surat_keluars.tujuan',
												'surat_keluars.perihal',
												'surat_keluars.lampiran',
												'surat_keluars.keterangan',
												'surat_keluars.file',
												'users.name',
												'list_kategoris.kode',
												'penanggung_jawabs.nama as nama_penanggungjawab',
												'jenis_surats.jenis_surat',
												'sifat_surats.sifat_surat',
												'kategoris.nama as nama_kategori',
												'kategoris.kode_kategori'
											);
		$surat_keluars=$surat_keluars->paginate(10);
		$start_page= (($surat_keluars->currentPage()-1) * 10) + 1;

		return view('admin.lurah.suratkeluar', compact('kategori'), array('surat_keluars' => $surat_keluars, 'start_page'=>$start_page));
	} 

	public function prosesreas(Request $request, $id)
	{
		$surat_masuks = SuratMasuk::findOrFail($id);
		$surat_masuks->status     		  = Input::get('disposisi');
		$surat_masuks->save();

		// redirect
		return Redirect::action('admin\LurahController@index');
	}

	public function edit($id)
	{
		$surat_masuks = SuratMasuk::findOrFail($id);     
		
		$bagian = Bagian::pluck('nama_bagian', 'id');
		$tanggapan = Tanggapan::pluck('tanggapan', 'id');
		return view('admin.lurah.create', array('bagian' => $bagian, 'surat_masuks' => $surat_masuks, 'tanggapan' => $tanggapan));
	}
	public function proses(Request $request, $id)
	{
		// echo "<pre>";
		// print_r($request->all());
		// echo "</pre>";
		// exit();
		$surat_masuks = SuratMasuk::findOrFail($id);
		$surat_masuks->status     		  = Input::get('disposisi');
		$surat_masuks->save();

		$bagians = new Disposisi;
		$bagians->id_bagian       	= Input::get('id3');
		$bagians->id_tanggapan      = Input::get('id4');
		$bagians->catatan        	= Input::get('catatan');
		$bagians->surat_id        	= Input::get('surat_id');
		$bagians->user_id       	= Auth::user()->id;
		$bagians->status        	= Input::get('status');
		$bagians->save();

		// redirect
		return Redirect::action('admin\LurahController@index');
	}
		public function show($id)
	{
		// print_r("wkwwkkw");
		// exit();
		$surat_masuks = SuratMasuk::leftJoin('jenis_surats', 'jenis_surats.id', '=', 'surat_masuks.id_jenis_surat')
									->leftJoin('kategoris', 'kategoris.id', '=', 'surat_masuks.id_kategori')
									->leftJoin('list_kategoris', 'list_kategoris.nama', '=', 'surat_masuks.nm_ls_kategori')
									->leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_masuks.id_sifat_surat')
									->orderBy('surat_masuks.created_at', 'desc')
									->select(
												'surat_masuks.id',
												'surat_masuks.no_agenda',
												'surat_masuks.no_surat',
												'surat_masuks.tgl_pengirim',
												'surat_masuks.tgl_penerima',
												'surat_masuks.sumber',
												'surat_masuks.perihal',
												'surat_masuks.lampiran',
												'surat_masuks.keterangan',
												'surat_masuks.file',
												'surat_masuks.status',
												'kategoris.nama as nama_kategori',
												'kategoris.kode_kategori',
												'list_kategoris.nama as nama_list',
												'list_kategoris.kode',
												'jenis_surats.jenis_surat',
												'sifat_surats.sifat_surat'
											)
									->findOrFail($id);
		return view('admin.lurah.show', array('surat_masuks' => $surat_masuks));
	}

public function search(Request $request)
	{
		$kategori = Kategori::pluck('nama', 'nama');
		$surat_masuks = SuratMasuk::leftJoin('jenis_surats', 'jenis_surats.id', '=', 'surat_masuks.id_jenis_surat')
									->leftJoin('kategoris', 'kategoris.id', '=', 'surat_masuks.id_kategori')
									->leftJoin('list_kategoris', 'list_kategoris.nama', '=', 'surat_masuks.nm_ls_kategori')
									->leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_masuks.id_sifat_surat')
									->orderBy('surat_masuks.created_at', 'desc')
									->select(
												'surat_masuks.id',
												'surat_masuks.no_agenda',
												'surat_masuks.no_surat',
												'surat_masuks.tgl_pengirim',
												'surat_masuks.tgl_penerima',
												'surat_masuks.sumber',
												'surat_masuks.perihal',
												'surat_masuks.lampiran',
												'surat_masuks.keterangan',
												'surat_masuks.file',
												'surat_masuks.status',
												'kategoris.nama as nama_kategori',
												'kategoris.kode_kategori',
												'list_kategoris.nama as nama_list',
												'list_kategoris.kode',
												'jenis_surats.jenis_surat',
												'sifat_surats.sifat_surat'
											);
	if($request->has('search')){
    $search = $request->get('search');
    $surat_masuks = $surat_masuks->Where("surat_masuks.no_agenda", "like", "%".$search."%")
                     			->orWhere("surat_masuks.no_surat", "like", "%".$search."%")
                     			->orWhere("surat_masuks.tgl_penerima", "like", "%".$search."%")
                     			->orWhere("surat_masuks.tgl_pengirim", "like", "%".$search."%")
                     			->orWhere("surat_masuks.sumber", "like", "%".$search."%")
                     			->orWhere("surat_masuks.perihal", "like", "%".$search."%")
                     			->orWhere("surat_masuks.lampiran", "like", "%".$search."%")
                     			->orWhere("surat_masuks.keterangan", "like", "%".$search."%")
                     			->orWhere("surat_masuks.status", "like", "%".$search."%")
                     			->orWhere("list_kategoris.nama", "like", "%".$search."%")
                     			->orWhere("list_kategoris.kode", "like", "%".$search."%")
                     			->orWhere("jenis_surats.jenis_surat", "like", "%".$search."%")
                     			->orWhere("kategoris.kode_kategori", "like", "%".$search."%")
                     			->orWhere("kategoris.nama", "like", "%".$search."%");
    }
    $surat_masuks = $surat_masuks->paginate(10);

    $start_page= (($surat_masuks->currentPage()-1) * 10) + 1;
		return view('admin.lurah.index', compact('surat_masuks','start_page','kategori'));
	}

	public function searchkategory(Request $request)
	{
		$kategori = Kategori::pluck('nama', 'nama');
		$surat_masuks = SuratMasuk::leftJoin('jenis_surats', 'jenis_surats.id', '=', 'surat_masuks.id_jenis_surat')
									->leftJoin('kategoris', 'kategoris.id', '=', 'surat_masuks.id_kategori')
									->leftJoin('list_kategoris', 'list_kategoris.nama', '=', 'surat_masuks.nm_ls_kategori')
									->leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_masuks.id_sifat_surat')
									->orderBy('surat_masuks.created_at', 'desc')
									->select(
												'surat_masuks.id',
												'surat_masuks.no_agenda',
												'surat_masuks.no_surat',
												'surat_masuks.tgl_pengirim',
												'surat_masuks.tgl_penerima',
												'surat_masuks.sumber',
												'surat_masuks.perihal',
												'surat_masuks.lampiran',
												'surat_masuks.keterangan',
												'surat_masuks.file',
												'surat_masuks.status',
												'kategoris.nama as nama_kategori',
												'kategoris.kode_kategori',
												'list_kategoris.nama as nama_list',
												'list_kategoris.kode',
												'jenis_surats.jenis_surat',
												'sifat_surats.sifat_surat'
											);
	if($request->has('search')){
    $search = $request->get('search');
    $surat_masuks = $surat_masuks->Where("kategoris.nama", "like", "%".$search."%");
    }
    $surat_masuks = $surat_masuks->paginate(10);

    $start_page= (($surat_masuks->currentPage()-1) * 10) + 1;
		return view('admin.lurah.index', compact('surat_masuks','start_page', 'kategori'));
	}

public function searchlurahsuratkeluar(Request $request)
	{
/*		print_r('wkwkwk');
		exit();*/
		$kategori = Kategori::pluck('nama', 'nama');
		$surat_keluars = SuratKeluar::leftJoin('users', 'users.id', '=', 'surat_keluars.id_pengolah')
									->leftJoin('jenis_surats', 'jenis_surats.id', '=', 'surat_keluars.id_jenis_surat')
									->leftJoin('kategoris', 'kategoris.id', '=', 'surat_keluars.id_kategori')
									->leftJoin('list_kategoris', 'list_kategoris.nama', '=', 'surat_keluars.nm_ls_kategori')
									->leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_keluars.id_sifat_surat')
									->leftJoin('penanggung_jawabs', 'penanggung_jawabs.id', '=', 'surat_keluars.id_penanggung_jawab')
									->orderBy('surat_keluars.created_at', 'desc')
									->select(
												'surat_keluars.id',
												'surat_keluars.no_surat',
												'surat_keluars.tgl_surat',
												'surat_keluars.id_pengolah',
												'surat_keluars.tujuan',
												'surat_keluars.perihal',
												'surat_keluars.lampiran',
												'surat_keluars.keterangan',
												'surat_keluars.file',
												'users.name',
												'list_kategoris.kode',
												'penanggung_jawabs.nama as nama_penanggungjawab',
												'jenis_surats.jenis_surat',
												'sifat_surats.sifat_surat'
											);
	if($request->has('search')){
    $search = $request->get('search');
    $surat_keluars = $surat_keluars->Where("kategoris.nama", "like", "%".$search."%");
    }
    $surat_keluars = $surat_keluars->paginate(10);

    $start_page= (($surat_keluars->currentPage()-1) * 10) + 1;
		return view('admin.lurah.index', compact('surat_keluars','start_page', 'kategori'));
	}

}

