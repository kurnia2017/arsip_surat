<?php

namespace App\Http\Controllers\admin;

use DB;
use View;
use Auth;
use Image;
use File;
use Response;
use PDF;
use App\Bagian;
use App\Disposisi;
use App\SuratKeluar;
use App\SuratMasuk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;


class MemoController extends Controller
{
    public function __construct()
	 {
	    $this->middleware('auth');
	 }
	  
	public function index()
	{
		$disposisi = Disposisi::leftjoin('bagians', 'bagians.id', '=', 'disposisis.id_bagian')
								->orderBy('disposisis.created_at', 'desc')
								->select(
											'disposisis.*',
											'bagians.nama_bagian',
											'bagians.nama_kepala'
										)
								->get();
		$jumlah_belum = Disposisi::where('status','=','1')->count();
		$jumlah_sudah = Disposisi::where('status','=','2')->count();
		
		return view('admin.memo.pesanmemo',array('disposisi' => $disposisi, 'jumlah_belum' => $jumlah_belum, 'jumlah_sudah' => $jumlah_sudah));
	} 

	public function read()
	{
		$disposisi = Disposisi::leftjoin('bagians', 'bagians.id', '=', 'disposisis.id_bagian')
								->leftjoin('users', 'users.id', '=', 'disposisis.user_id')
								->leftjoin('surat_masuks', 'surat_masuks.id', '=', 'disposisis.surat_id')
								->where('disposisis.status', "2")
								->select(
											'disposisis.*',
											'users.name',
											'surat_masuks.sumber'

										)
								->paginate(10);
		// echo "<pre>";
		// print_r($disposisi);
		// echo "</pre>";
		// exit();
		$start_page= (($disposisi->currentPage()-1) * 10) + 1;
		return view('admin.memo.read',array('disposisi' => $disposisi, 'start_page' => $start_page));
	} 

	 public function unread()
	{
			$disposisi = Disposisi::leftjoin('bagians', 'bagians.id', '=', 'disposisis.id_bagian')
								->leftjoin('users', 'users.id', '=', 'disposisis.user_id')
								->leftjoin('surat_masuks', 'surat_masuks.id', '=', 'disposisis.surat_id')
								->where('disposisis.status', "1")
								->select(
											'disposisis.*',
											'users.name',
											'surat_masuks.sumber'

										)
								->paginate(10);
		// echo "<pre>";
		// print_r($disposisi);
		// echo "</pre>";
		// exit();
		$start_page= (($disposisi->currentPage()-1) * 10) + 1;
		return view('admin.memo.unread',array('disposisi' => $disposisi, 'start_page' => $start_page));
	} 

	public function detailread($id)
	{
		// print_r('wkwkw');
		// exit();
		$disposisi = Disposisi::leftjoin('bagians', 'bagians.id', '=', 'disposisis.id_bagian')
								->leftjoin('surat_masuks', 'surat_masuks.id', '=', 'disposisis.surat_id')
								->orderBy('disposisis.status', "1")
								->select(
											'disposisis.*',
											'bagians.nama_bagian',
											'bagians.nama_kepala',
											'surat_masuks.sumber',
											'surat_masuks.tgl_penerima',
											'surat_masuks.no_surat'

										)
								->findOrFail($id);
		

		return view('admin.memo.detailread',array('disposisi' => $disposisi));
	}

	public function detailunread($id)
	{
		// print_r('wkwkw');
		// exit();
		$disposisi = Disposisi::leftjoin('bagians', 'bagians.id', '=', 'disposisis.id_bagian')
								->leftjoin('surat_masuks', 'surat_masuks.id', '=', 'disposisis.surat_id')
								->leftjoin('kategoris', 'kategoris.id', '=', 'surat_masuks.id_kategori')
								->leftjoin('list_kategoris', 'list_kategoris.nama', '=', 'surat_masuks.nm_ls_kategori')
								->orderBy('disposisis.status', "1")
								->select(
											'disposisis.*',
											'bagians.nama_bagian',
											'bagians.nama_kepala',
											'surat_masuks.sumber',
											'surat_masuks.tgl_penerima',
											'surat_masuks.no_surat',
											'surat_masuks.no_agenda',
											'kategoris.nama',
											'list_kategoris.nama as nama_list',
											'list_kategoris.kode'
										)
								->findOrFail($id);
			// echo "<pre>";
			// print_r($disposisi);
			// echo "</pre>";
			// exit();
		return view('admin.memo.detailunread',array('disposisi' => $disposisi));
	} 

	public function wkwk(Request $request)
	{
		/*print_r('wkwk');
		exit();*/
		$disposisi = Disposisi::leftjoin('bagians', 'bagians.id', '=', 'disposisis.id_bagian')
								->leftjoin('users', 'users.id', '=', 'disposisis.user_id')
								->leftjoin('surat_masuks', 'surat_masuks.id', '=', 'disposisis.surat_id')
								->orderBy('disposisis.status', "2")
								->select(
											'disposisis.*',
											'users.name',
											'surat_masuks.sumber'

										);
						
	if($request->has('search')){
    $search = $request->get('search');
    $disposisi = $disposisi->Where("disposisi.catatan", "like", "%".$search."%")
                     			->orWhere("users.name", "like", "%".$search."%")
                     			->orWhere("surat_masuks.sumber", "like", "%".$search."%");
                     		
    }
    $disposisi = $disposisi->paginate(10);

    $start_page= (($disposisi->currentPage()-1) * 10) + 1;
		return view('admin.memo.read', compact('disposisi','start_page'));
	}

	public function prosesreas(Request $request, $id)
	{
		$disposisi = Disposisi::findOrFail($id);
		$disposisi->status     		  = Input::get('status');
		$disposisi->save();

		// redirect
		return Redirect('memo/unread');
	}


	public function document()
	{
		$umum = SuratKeluar::where('id_kategori', 1)->count();
		$umums = SuratMasuk::where('id_kategori', 1)->count();
		$jumlah_umum = $umum + $umums;

		$pemerintahan = SuratKeluar::where('id_kategori', 2)->count();
		$pemerintahans = SuratMasuk::where('id_kategori', 2)->count();
		$jumlah_pemerintahan = $pemerintahan + $pemerintahans;

		$politik = SuratKeluar::where('id_kategori', 3)->count();
		$politiks = SuratMasuk::where('id_kategori', 3)->count();
		$jumlah_politik = $politik + $politiks;

		$keamanan = SuratKeluar::where('id_kategori', 4)->count();
		$keamanans = SuratMasuk::where('id_kategori', 4)->count();
		$jumlah_keamanan = $keamanan + $keamanans;

		$kesejahteraan = SuratKeluar::where('id_kategori', 5)->count();
		$kesejahteraans = SuratMasuk::where('id_kategori', 5)->count();
		$jumlah_kesejahteraan = $kesejahteraan + $kesejahteraans;

		$perekonomian = SuratKeluar::where('id_kategori', 6)->count();
		$perekonomians = SuratMasuk::where('id_kategori', 6)->count();
		$jumlah_perekonomian = $perekonomian + $perekonomians;

		$pekerjaanumum = SuratKeluar::where('id_kategori', 7)->count();
		$pekerjaanumums = SuratMasuk::where('id_kategori', 7)->count();
		$jumlah_pekerjaanumum = $pekerjaanumum + $pekerjaanumums;

		$pengawasan = SuratKeluar::where('id_kategori', 8)->count();
		$pengawasans = SuratMasuk::where('id_kategori', 8)->count();
		$jumlah_pengawasan = $pengawasan + $pengawasans;

		$kepegawaian = SuratKeluar::where('id_kategori', 9)->count();
		$kepegawaians = SuratMasuk::where('id_kategori', 9)->count();
		$jumlah_kepegawaian = $kepegawaian + $kepegawaians;

		$keuangan = SuratKeluar::where('id_kategori', 10)->count();
		$keuangans = SuratMasuk::where('id_kategori', 10)->count();
		$jumlah_keuangan = $keuangan + $keuangans;

/*		echo "<pre>";
		print_r($jumlah_pemerintahan); 
		echo "</pre>";		
		exit();*/
		// $jumlah_belum = Disposisi::where('status','=','1')->count();
		// $jumlah_sudah = Disposisi::where('status','=','2')->count();
		
		return view('dokumen',compact('jumlah_umum', 'jumlah_pemerintahan',  'jumlah_politik', 'jumlah_keamanan', 'jumlah_kesejahteraan', 'jumlah_perekonomian', 'jumlah_pekerjaanumum', 'jumlah_pengawasan', 'jumlah_kepegawaian', 'jumlah_keuangan'));
	} 

	public function kategori(Request $request){
		// $a = $request->invisible;
		echo "<pre>";
		print_r($request->all()); 
		echo "</pre>";
		exit();
	}

	
}
