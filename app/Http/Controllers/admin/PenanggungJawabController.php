<?php

namespace App\Http\Controllers\admin;

use DB;
use View;
use Auth;
use Image;
use File;
use Response;
use App\PenanggungJawab;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;


class PenanggungJawabController extends Controller
{
public function __construct()
	 {
	    $this->middleware('auth');
	 }
	  
	public function index()
	{
		$penanggung_jawabs = PenanggungJawab::orderBy('created_at', 'desc');
		$penanggung_jawabs=$penanggung_jawabs->paginate(10);
		$start_page= (($penanggung_jawabs->currentPage()-1) * 10) + 1;
		return view('admin.penanggung_jawabs.index',array('penanggung_jawabs' => $penanggung_jawabs, 'start_page'=>$start_page));
	} 

	public function create()
	{
	    return View::make('admin.penanggung_jawabs.create');
	}

	public function store(Request $request)
	{

		// $this->validate($request, [
		// 'nama'          => 'required|unique:surat_masuks|max:255',
		// ]);
		// store
		$penanggung_jawabs = new PenanggungJawab;
		$penanggung_jawabs->nama     = Input::get('nama');
		$penanggung_jawabs->save();

		return Redirect::action('admin\PenanggungJawabController@index');
	}

	public function show($id)
	{
		$penanggung_jawabs = PenanggungJawab::findOrFail($id);
		return view('admin.penanggung_jawabs.show', array('penanggung_jawabs' => $penanggung_jawabs));
	}

	public function edit($id)
	{
		$penanggung_jawabs = PenanggungJawab::findOrFail($id);     
		return view('admin.penanggung_jawabs.edit', array('penanggung_jawabs' => $penanggung_jawabs));
	}

	public function update(Request $request, $id)
	{

		// $this->validate($request, [
		// 'name'          	=> 'required|unique:surat_masuks|max:255',
		// 'unit'          	=> 'required|unique:surat_masuks|max:255',
		// ]);   

		$penanggung_jawabs = PenanggungJawab::findOrFail($id);
		$penanggung_jawabs->nama				= Input::get('nama');
		$penanggung_jawabs->save();

		// redirect
		return Redirect::action('admin\PenanggungJawabController@index');
	}

	public function destroy($id)
	{    
		$penanggung_jawabs = PenanggungJawab::findOrFail($id);
		$penanggung_jawabs->delete();
		return Redirect::action('admin\PenanggungJawabController@index')->with('flash-success','The user has been deleted.');
	}

	public function search(Request $request)
	{
		 $search = $request->get('search');
      $penanggung_jawabs = PenanggungJawab::where('nama','LIKE','%'.$search.'%')->paginate(10);
	    $start_page= (($penanggung_jawabs->currentPage()-1) * 10) + 1;
		return view('admin.penanggung_jawabs.index', compact('penanggung_jawabs','start_page'));
	}
}
 