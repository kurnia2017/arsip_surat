<?php

namespace App\Http\Controllers\admin;

use DB;
use View;
use Auth;
use Image;
use File;
use Response;
use App\SuratMasuk;
use App\SuratKeluar;
use App\Referensi;
use App\Bagian;
use App\Tanggapan;
use App\Disposisi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class ReferensiController extends Controller
{
    
	  
	public function index()
	{
		$referensies = Referensi::orderBy('created_at', 'desc');
		$referensies=$referensies->paginate(10);
		//  echo "<pre>";
		// print_r($referensies->all());
		// echo "</pre>";
		// exit();
		$start_page= (($referensies->currentPage()-1) * 10) + 1;
		return view('admin.referensies.index',array('referensies' => $referensies, 'start_page'=>$start_page));
	} 

	public function create()
	{
	    return View::make('admin.referensies.create');
	}

	public function store(Request $request)
	{

		// $this->validate($request, [
		// 'nama'          => 'required|unique:surat_masuks|max:255',
		// ]);
		// store
		$referensies = new Referensi;
		$referensies->id       = Input::get('id');
		$referensies->nama        = Input::get('nama');
		$referensies->save();

		return Redirect::action('admin\ReferensiController@index');
	}

	public function show($id)
	{
		$referensies = Referensi::findOrFail($id);
		return view('admin.referensies.show', array('referensies' => $referensies));
	}

	public function edit($id)
	{
		$referensies = Referensi::findOrFail($id);     
		return view('admin.referensies.edit', array('referensies' => $referensies));
	}

	public function update(Request $request, $id)
	{

		// $this->validate($request, [
		// 'name'          	=> 'required|unique:surat_masuks|max:255',
		// 'unit'          	=> 'required|unique:surat_masuks|max:255',
		// ]);   

		$referensies = Referensi::findOrFail($id);
		$referensies->id				= Input::get('id');
		$referensies->nama      		= Input::get('nama');
		$referensies->save();

		// redirect
		return Redirect::action('admin\ReferensiController@index');
	}

	public function destroy($id)
	{    
		$referensies = Referensi::findOrFail($id);
		$referensies->delete();
		return Redirect::action('admin\ReferensiController@index')->with('flash-success','The user has been deleted.');
	}

	public function search(Request $request)
	{
		$referensies = Referensi::orderBy('created_at', 'desc');
		if($request->has('search')){
	    $search = $request->get('search');
	    $referensies = $referensies->Where("referensies.id", "like", "%".$search."%")
	                       ->orWhere("referensies.nama", "like", "%".$search."%");
	    }
	    $referensies = $referensies->paginate(10);

	    $start_page= (($referensies->currentPage()-1) * 10) + 1;
		return view('admin.referensies.index', compact('referensies','start_page'));
	}
}
