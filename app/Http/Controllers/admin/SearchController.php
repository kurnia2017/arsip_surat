<?php

namespace App\Http\Controllers\admin;

use App\SuratKeluar;
use App\SuratMasuk;
use App\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class SearchController extends Controller
{
	public function umum(){
		$umum = SuratMasuk::where('id_kategori', '1')
						->get();
		$umums = SuratKeluar::where('id_kategori', '1')
						->get();

		return view('admin.perkaktegori.umum',array('umum' => $umum, 'umums' => $umums));
	}

	public function pemerintahan(){
		$pemerintahan = SuratMasuk::where('id_kategori', '2')
						->get();
		$pemerintahans = SuratKeluar::where('id_kategori', '2')
						->get();

		return view('admin.perkaktegori.pemerintahan',array('pemerintahan' => $pemerintahan, 'pemerintahans' => $pemerintahans));
	}

	public function politik(){
		$politik = SuratMasuk::where('id_kategori', '3')
						->get();
		$politiks = SuratKeluar::where('id_kategori', '3')
						->get();

		return view('admin.perkaktegori.politik',array('politik' => $politik, 'politiks' => $politiks));
	}

	public function keamanan_dan_ketertiban(){
		$keamanan_dan_ketertiban = SuratMasuk::where('id_kategori', '4')
						->get();
		$keamanan_dan_ketertibans = SuratKeluar::where('id_kategori', '4')
						->get();

		return view('admin.perkaktegori.keamanan_dan_ketertiban',array('keamanan_dan_ketertiban' => $keamanan_dan_ketertiban, 'keamanan_dan_ketertibans' => $keamanan_dan_ketertibans));
	}

	public function kesejahtraan_rakyat(){
		$kesejahtraan_rakyat = SuratMasuk::where('id_kategori', '5')
						->get();
		$kesejahtraan_rakyats = SuratKeluar::where('id_kategori', '5')
						->get();

		return view('admin.perkaktegori.kesejahtraan_rakyat',array('kesejahtraan_rakyat' => $kesejahtraan_rakyat, 'kesejahtraan_rakyats' => $kesejahtraan_rakyats));
	}

	public function prekonomian(){
		$prekonomian = SuratMasuk::where('id_kategori', '6')
						->get();
		$prekonomians = SuratKeluar::where('id_kategori', '6')
						->get();

		return view('admin.perkaktegori.prekonomian',array('prekonomian' => $prekonomian, 'prekonomians' => $prekonomians));
	}

		public function pekerjaan_umum_dan_ketenagaan(){
		$pekerjaan_umum_dan_ketenagaan = SuratMasuk::where('id_kategori', '7')
						->get();
		$pekerjaan_umum_dan_ketenagaans = SuratKeluar::where('id_kategori', '7')
						->get();

		return view('admin.perkaktegori.pekerjaan_umum_dan_ketenagaan',array('pekerjaan_umum_dan_ketenagaan' => $pekerjaan_umum_dan_ketenagaan, 'pekerjaan_umum_dan_ketenagaans' => $pekerjaan_umum_dan_ketenagaans));
	}

		public function pengawasan(){
		$pengawasan = SuratMasuk::where('id_kategori', '8')
						->get();
		$pengawasans = SuratKeluar::where('id_kategori', '8')
						->get();

		return view('admin.perkaktegori.pengawasan',array('pengawasan' => $pengawasan, 'pengawasans' => $pengawasans));
	}

		public function kepegawaian(){
		$kepegawaian = SuratMasuk::where('id_kategori', '9')
						->get();
		$kepegawaians = SuratKeluar::where('id_kategori', '9')
						->get();

		return view('admin.perkaktegori.kepegawaian',array('kepegawaian' => $kepegawaian, 'kepegawaians' => $kepegawaians));
	}

		public function keuangan(){
		$keuangan = SuratMasuk::where('id_kategori', '10')
						->get();
		$keuangans = SuratKeluar::where('id_kategori', '10')
						->get();

		return view('admin.perkaktegori.keuangan',array('keuangan' => $keuangan, 'keuangans' => $keuangans));
	}


	public function search_umum(Request $request)
	{
		$umum = SuratMasuk::where('id_kategori', '1');
		if($request->has('search')){
	    $search = $request->get('search');
	    $umum = $umum->Where("surat_masuks.no_surat", "like", "%".$search."%");
	    }
	    $umum = $umum->get();

	    $umums = SuratKeluar::where('id_kategori', '1');
		if($request->has('search')){
	    $search = $request->get('search');
	    $umums = $umums->Where("surat_keluars.no_surat", "like", "%".$search."%");
	    }
	    $umums = $umums->get();

		return view('admin.perkaktegori.umum', compact('umum', 'umums'));
	}

	public function search_pemerintahan(Request $request)
	{
		$pemerintahan = SuratMasuk::where('id_kategori', '2');
		if($request->has('search')){
	    $search = $request->get('search');
	    $pemerintahan = $pemerintahan->Where("surat_masuks.no_surat", "like", "%".$search."%");
	    }
	    $pemerintahan = $pemerintahan->get();

	    $pemerintahans = SuratKeluar::where('id_kategori', '2');
		if($request->has('search')){
	    $search = $request->get('search');
	    $pemerintahans = $pemerintahans->Where("surat_keluars.no_surat", "like", "%".$search."%");
	    }
	    $pemerintahans = $pemerintahans->get();

		return view('admin.perkaktegori.pemerintahan', compact('pemerintahan', 'pemerintahans'));
	}

	public function search_politik(Request $request)
	{
		$politik = SuratMasuk::where('id_kategori', '3');
		if($request->has('search')){
	    $search = $request->get('search');
	    $politik = $politik->Where("surat_masuks.no_surat", "like", "%".$search."%");
	    }
	    $politik = $politik->get();

	    $politiks = SuratKeluar::where('id_kategori', '3');
		if($request->has('search')){
	    $search = $request->get('search');
	    $politiks = $politiks->Where("surat_keluars.no_surat", "like", "%".$search."%");
	    }
	    $politiks = $politiks->get();

		return view('admin.perkaktegori.politik', compact('politik', 'politiks'));
	}

		public function search_keamanan_dan_ketertiban(Request $request)
	{
		$keamanan_dan_ketertiban = SuratMasuk::where('id_kategori', '4');
		if($request->has('search')){
	    $search = $request->get('search');
	    $keamanan_dan_ketertiban = $keamanan_dan_ketertiban->Where("surat_masuks.no_surat", "like", "%".$search."%");
	    }
	    $keamanan_dan_ketertiban = $keamanan_dan_ketertiban->get();

	    $keamanan_dan_ketertibans = SuratKeluar::where('id_kategori', '4');
		if($request->has('search')){
	    $search = $request->get('search');
	    $keamanan_dan_ketertibans = $keamanan_dan_ketertibans->Where("surat_keluars.no_surat", "like", "%".$search."%");
	    }
	    $keamanan_dan_ketertibans = $keamanan_dan_ketertibans->get();

		return view('admin.perkaktegori.keamanan_dan_ketertiban', compact('keamanan_dan_ketertiban', 'keamanan_dan_ketertibans'));
	}

		public function search_kesejahtraan_rakyat(Request $request)
	{
		$kesejahtraan_rakyat = SuratMasuk::where('id_kategori', '5');
		if($request->has('search')){
	    $search = $request->get('search');
	    $kesejahtraan_rakyat = $kesejahtraan_rakyat->Where("surat_masuks.no_surat", "like", "%".$search."%");
	    }
	    $kesejahtraan_rakyat = $kesejahtraan_rakyat->get();

	    $kesejahtraan_rakyats = SuratKeluar::where('id_kategori', '5');
		if($request->has('search')){
	    $search = $request->get('search');
	    $kesejahtraan_rakyats = $kesejahtraan_rakyats->Where("surat_keluars.no_surat", "like", "%".$search."%");
	    }
	    $kesejahtraan_rakyats = $kesejahtraan_rakyats->get();

		return view('admin.perkaktegori.kesejahtraan_rakyat', compact('kesejahtraan_rakyat', 'kesejahtraan_rakyats'));
	}

		public function search_prekonomian(Request $request)
	{
		$prekonomian = SuratMasuk::where('id_kategori', '6');
		if($request->has('search')){
	    $search = $request->get('search');
	    $prekonomian = $prekonomian->Where("surat_masuks.no_surat", "like", "%".$search."%");
	    }
	    $prekonomian = $prekonomian->get();

	    $prekonomians = SuratKeluar::where('id_kategori', '6');
		if($request->has('search')){
	    $search = $request->get('search');
	    $prekonomians = $prekonomians->Where("surat_keluars.no_surat", "like", "%".$search."%");
	    }
	    $prekonomians = $prekonomians->get();

		return view('admin.perkaktegori.prekonomian', compact('prekonomian', 'prekonomians'));
	}

		public function search_pekerjaan_umum_dan_ketenagaan(Request $request)
	{
		$pekerjaan_umum_dan_ketenagaan = SuratMasuk::where('id_kategori', '7');
		if($request->has('search')){
	    $search = $request->get('search');
	    $pekerjaan_umum_dan_ketenagaan = $pekerjaan_umum_dan_ketenagaan->Where("surat_masuks.no_surat", "like", "%".$search."%");
	    }
	    $pekerjaan_umum_dan_ketenagaan = $pekerjaan_umum_dan_ketenagaan->get();

	    $pekerjaan_umum_dan_ketenagaans = SuratKeluar::where('id_kategori', '7');
		if($request->has('search')){
	    $search = $request->get('search');
	    $pekerjaan_umum_dan_ketenagaans = $pekerjaan_umum_dan_ketenagaans->Where("surat_keluars.no_surat", "like", "%".$search."%");
	    }
	    $pekerjaan_umum_dan_ketenagaans = $pekerjaan_umum_dan_ketenagaans->get();

		return view('admin.perkaktegori.pekerjaan_umum_dan_ketenagaan', compact('pekerjaan_umum_dan_ketenagaan', 'pekerjaan_umum_dan_ketenagaans'));
	}

		public function search_pengawasan(Request $request)
	{
		$pengawasan = SuratMasuk::where('id_kategori', '8');
		if($request->has('search')){
	    $search = $request->get('search');
	    $pengawasan = $pengawasan->Where("surat_masuks.no_surat", "like", "%".$search."%");
	    }
	    $pengawasan = $pengawasan->get();

	    $pengawasans = SuratKeluar::where('id_kategori', '8');
		if($request->has('search')){
	    $search = $request->get('search');
	    $pengawasans = $pengawasans->Where("surat_keluars.no_surat", "like", "%".$search."%");
	    }
	    $pengawasans = $pengawasans->get();

		return view('admin.perkaktegori.pengawasan', compact('pengawasan', 'pengawasans'));
	}

		public function search_kepegawaian(Request $request)
	{
		$kepegawaian = SuratMasuk::where('id_kategori', '9');
		if($request->has('search')){
	    $search = $request->get('search');
	    $kepegawaian = $kepegawaian->Where("surat_masuks.no_surat", "like", "%".$search."%");
	    }
	    $kepegawaian = $kepegawaian->get();

	    $kepegawaians = SuratKeluar::where('id_kategori', '9');
		if($request->has('search')){
	    $search = $request->get('search');
	    $kepegawaians = $kepegawaians->Where("surat_keluars.no_surat", "like", "%".$search."%");
	    }
	    $kepegawaians = $kepegawaians->get();

		return view('admin.perkaktegori.kepegawaian', compact('kepegawaian', 'kepegawaians'));
	}
		public function search_keuangan(Request $request)
	{
		$keuangan = SuratMasuk::where('id_kategori', '9');
		if($request->has('search')){
	    $search = $request->get('search');
	    $keuangan = $keuangan->Where("surat_masuks.no_surat", "like", "%".$search."%");
	    }
	    $keuangan = $keuangan->get();

	    $keuangans = SuratKeluar::where('id_kategori', '9');
		if($request->has('search')){
	    $search = $request->get('search');
	    $keuangans = $keuangans->Where("surat_keluars.no_surat", "like", "%".$search."%");
	    }
	    $keuangans = $keuangans->get();

		return view('admin.perkaktegori.keuangan', compact('keuangan', 'keuangans'));
	}

    //
}
