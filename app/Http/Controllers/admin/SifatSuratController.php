<?php

namespace App\Http\Controllers\admin;

use DB;
use View;
use Auth;
use Response;
use App\SifatSurat;
use App\kategori;
use App\SuratMasuk;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;


class SifatSuratController extends Controller
{
     public function __construct()
	 {
	    $this->middleware('auth');
	 }
	  
	public function index()
	{
		$sifat_surats = SifatSurat::orderBy('created_at', 'desc');
		$sifat_surats=$sifat_surats->paginate(10);
		$start_page= (($sifat_surats->currentPage()-1) * 10) + 1;

		return view('admin.sifat_surat.index',array('sifat_surats' => $sifat_surats, 'start_page'=>$start_page));
	} 

	public function create()
	{
	    return View::make('admin.sifat_surat.create');
	}

	public function store(Request $request)
	{
/*		$this->validate($request, [
		'nama'          => 'required|unique:sifat_surats|max:255',
		]);
	*/	// store
		$sifat_surats = new SifatSurat;
		$sifat_surats->sifat_surat     		= Input::get('sifat_surat');
		$sifat_surats->save();

		// redirect
		return Redirect::action('admin\SifatSuratController@index');
	}

	public function show($id)
	{
		$sifat_surats = SifatSurat::findOrFail($id);
		return view('admin.sifat_surat.show', array('sifat_surats' => $sifat_surats));
	}

	public function edit($id)
	{
		$sifat_surats = SifatSurat::findOrFail($id);     
		return view('admin.sifat_surat.edit', array('sifat_surats' => $sifat_surats));
	}

	public function update(Request $request, $id)
	{
		// $this->validate($request, [
		// 'name'          	=> 'required|unique:perihals|max:255',
		// 'unit'          	=> 'required|unique:perihals|max:255',
		// ]);   

		$sifat_surats = SifatSurat::findOrFail($id);
		$sifat_surats->sifat_surat     		= Input::get('sifat_surat');
		$sifat_surats->save();

		// redirect
		return Redirect::action('admin\SifatSuratController@index');
	}

	public function destroy($id)
	{    
		$sifat_surats = SifatSurat::findOrFail($id);
		$sifat_surats->delete();
		return Redirect::action('admin\SifatSuratController@index')->with('flash-success','The user has been deleted.');
	}

	public function search(Request $request)
	{
		$search = $request->get('search');
		$sifat_surats = SifatSurat::where('sifat_surat','LIKE','%'.$search.'%');
		$sifat_surats = $sifat_surats->paginate(10);

		$start_page= (($sifat_surats->currentPage()-1) * 10) + 1;
		return view('admin.sifat_surat.index', compact('sifat_surats','start_page'));
	}

}
