<?php

namespace App\Http\Controllers\admin;

use DB;
use thiagoalessio\TesseractOCR\TesseractOCR;
use View;
use Auth;
use Image;
use File;
use Response;
use App\SuratKeluar;
use App\PenanggungJawab;
use App\Referensi;
use App\JenisSurat;
use App\SifatSurat;
use App\Kategori;
use App\User;
use App\FilePdf;
use Spatie\PdfToText\Pdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class SuratKeluarController extends Controller
{
	public function __construct()
	 {
	    $this->middleware('auth');
	 }
	  
	public function index()
	{
	$surat_keluars = SuratKeluar:: leftJoin('users', 'users.id', '=', 'surat_keluars.id_pengolah')
									->leftJoin('penanggung_jawabs', 'penanggung_jawabs.id', '=', 'surat_keluars.id_penanggung_jawab')
									->leftJoin('jenis_surats', 'jenis_surats.id', '=', 'surat_keluars.id_jenis_surat')
									->leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_keluars.id_sifat_surat')
									->leftJoin('list_kategoris', 'list_kategoris.nama', '=', 'surat_keluars.nm_ls_kategori')
									->leftJoin('kategoris', 'kategoris.id', '=', 'surat_keluars.id_kategori')
									->orderBy('surat_keluars.created_at', 'desc')
									->select(
												'surat_keluars.id',
												'surat_keluars.no_surat',
												'surat_keluars.tgl_surat',
												'surat_keluars.id_pengolah',
												'surat_keluars.tujuan',
												'surat_keluars.perihal',
												'surat_keluars.lampiran',
												'surat_keluars.keterangan',
												'surat_keluars.file',
												'users.name',
												'list_kategoris.kode',
												'penanggung_jawabs.nama as nama_penanggungjawab',
												'jenis_surats.jenis_surat',
												'sifat_surats.sifat_surat',
												'kategoris.nama as nama_kategori'

											);

		$surat_keluars=$surat_keluars->paginate(10);
		// 		echo "<pre>";
		// print_r($surat_keluars);
		// echo "</pre>";
		// exit();
		  $tgl_surat = SuratKeluar::all()->toArray();
    	$tgl_surat = collect(array_merge(['' => ['id' => '','tgl_surat' => 'Masukan TGL']], $tgl_surat));
    	$tgl_surats  = $tgl_surat->pluck('tgl_surat', 'tgl_surat');
		$start_page= (($surat_keluars->currentPage()-1) * 10) + 1;

		return view('admin.surat_keluars.index',array('surat_keluars' => $surat_keluars, 'start_page'=>$start_page, 'tgl_surats'=> $tgl_surats));
	} 

	public function create()
	{
		$user = User::pluck('name', 'id');
		$jenis_surat = JenisSurat::pluck('jenis_surat', 'id');
		$sifat_surats = SifatSurat::pluck('sifat_surat', 'id');
		$penanggung_jawab = PenanggungJawab::pluck('nama', 'id');
		$kategori = Kategori::pluck('nama', 'id');
		$surat_keluars  = Kategori::leftJoin('list_kategoris', 'list_kategoris.id_kategori', '=', 'kategoris.id')
									->leftJoin('surat_keluars', 'surat_keluars.id_kategori', '=', 'kategoris.id')
									->leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_keluars.id_sifat_surat')
									->leftJoin('jenis_surats', 'jenis_surats.id', '=', 'surat_keluars.id_jenis_surat')
									->leftJoin('penanggung_jawabs', 'penanggung_jawabs.id', '=', 'surat_keluars.id_penanggung_jawab')
									->orderBy('surat_keluars.created_at', 'desc')
									->select(
												'surat_keluars.id',
												'surat_keluars.no_surat',
												'surat_keluars.tgl_surat',
												'surat_keluars.id_pengolah',
												'surat_keluars.tujuan',
												'surat_keluars.perihal',
												'surat_keluars.lampiran',
												'surat_keluars.keterangan',
												'surat_keluars.file',
												'kategoris.nama as nama_kategori',
												'kategoris.kode_kategori',
												'list_kategoris.nama as nama_list',
												'list_kategoris.id as id_list',
												'list_kategoris.kode',
												'penanggung_jawabs.nama as nama_penanggungjawab',
												'sifat_surats.sifat_surat'

											)
									->first();

// echo "<pre>";
// 		print_r ($surat_keluars);
// 		echo "</pre>";
// 		exit();


	   return View::make('admin.surat_keluars.create', compact('user','surat_keluars', 'penanggung_jawab', 'jenis_surat','kategori', 'sifat_surats'));
	}

	public function store(Request $request)
	{
		
	 	$noUrutAkhir = \App\SuratKeluar::max('id');
		$surat= Input::get('nama_list');


		$generateregister = $surat."/".sprintf("%04s", abs($noUrutAkhir + 1))."/"."Kel-Skjt";

		$surat_keluars = new SuratKeluar;
		//$surat_keluars->no_surat       		= $generateregister;
		$surat_keluars->tgl_surat       	= Input::get('tgl_surat');
		$surat_keluars->id_pengolah       	= Input::get('id');
		$surat_keluars->id_jenis_surat      = Input::get('id_jenis_surat');
		$surat_keluars->id_sifat_surat      = Input::get('id3');
		$surat_keluars->id_penanggung_jawab = Input::get('id3');
		// $surat_keluars->tujuan       		= Input::get('tujuan');
		// $surat_keluars->perihal       		= Input::get('perihal');
		$surat_keluars->lampiran       		= Input::get('lampiran');
		$surat_keluars->keterangan       	= Input::get('keterangan');
		$surat_keluars->nm_ls_kategori  	= Input::get('nama_list');
		$surat_keluars->id_kategori 	   	= Input::get('kategory');
		$surat_keluars->nm_ls_kategori  	= Input::get('nama_list');

		$files = Input::file('file');
	

		$name=$files->getClientOriginalName();
		$files->move(public_path().'/pdftotext/', $name);
		$path = public_path('/pdftotext/'. $name);
		$reader = new \Asika\Pdf2text;
		$output = $reader->decode($path);
		$surat_keluars->file_upload	   = $name;
		$data[] = $name;
		$outputs  = str_replace(array("\n" , "\r"), '', trim($output));

		//prihal
		$arr_kalimat = explode ("        ",$outputs);
		$arr_kalimat2 = explode ("    ",$outputs);
		$perihal = $arr_kalimat2[1];		
		$arr_kalimat1 = explode(": ",$perihal);
		$hasil_prihal = $arr_kalimat1[2];
		$hasil_finall = explode("  Kepada Yth Bapak / Ibu",$hasil_prihal);
		$hasil_prihal_v2 = $hasil_finall[0];
// echo "<pre>";
// 		print_r ($hasil_prihal_v2);
		
// 		echo "</pre>";
// 		exit();
		//end prihal

		//no_surat
		$arr_nosurat = explode ("        ",$outputs);
		$arr_nosurat2 = explode ("    ",$outputs);
		$no = $arr_nosurat2[1];
		$a = explode (":",$no);
		$kosongs = $a[0];
		$kosongse = "";
		if ($kosongs == null ) {
			$c = '-';
		}else{
			if ($a[1] == null) {
				$c = '-';
			}else{
				$b = $a[1];
				$perihals = "Perihal"; 
				$c = str_replace($perihals,"",$b);		
			}
			
		}
		//no_surat
		

		//yth
		$arr_nosurat1 = explode ("        ",$outputs);
		$arr_nosurat2 = explode ("    ",$outputs);
		$yth = $arr_nosurat2[4];
		$ass = explode ("  Assalamu’alaikum Wr. Wb  Segala Puji milik Allah SWT, yang telah memberi Taufiq dan Kesempatan-Nya kepada kita.  Sholawat salam kepada Nabi Muhammad SAW. Semoga kita selalu dalam lindungan-Nya.  Bersama ini kami mengundang Bapak/Ibu dalam acara Rapat Persiapan Dana Desa yang Insya Allah akan dilaksanakan pada :  Hari / Tanggal",$yth);
		$yths = $ass[0];
		$hilang_yth = "Kepada Yth "; 
		$ke_yth = str_replace($hilang_yth,"",$yths);

		

		// dd($output);

		$reqcolaborate []= DB::table("referensis")
                     ->distinct()
                     ->pluck("nama");

		$data_retrun = [];
			foreach ($reqcolaborate as $i => $value) {
				if ($value) {
					foreach($value as $k_2 =>$v_2){
						$data_retrun[] = $v_2;
					}
					
				}
			}	

				for($i = 0; $i < count($data_retrun); $i++) {
		   				$string = str_replace($data_retrun[$i],"",$outputs);
				}
		//start pemanggilan data referensi 

		$array_referensi = Referensi::get();

		//end pemanggilan data referensi 

		
		//$jumlah_kata_sebelum_filter = count(explode(" ",$outputs)); 
		$jumlah_kata_sebelum_filter = str_word_count($outputs);
		$jumlah_kata_sesudah_filter = 0;
		 
		// start menjumlah seluruh kata dari data array
		$array_kata = explode(" ",$outputs);
		// end menjumlah seluruh kata dari data array

		foreach ($array_kata as $key => $value) {
			foreach ($array_referensi as $key => $value_array_referensi) {
				if (strtolower(trim($value_array_referensi->nama)) == strtolower(trim($value))) {
					$jumlah_kata_sesudah_filter ++;
				}
			}
		}
		
		$string_kata_setelah_filter = $string;


		foreach ($array_referensi as $key => $value_array_referensi) {
			$string_kata_setelah_filter = str_replace($value_array_referensi->nama,"",$string_kata_setelah_filter);
		}
			

		$surat_keluars->file_kata_akhir 	= $string_kata_setelah_filter;
		$surat_keluars->file 				= $string;
		$surat_keluars->tujuan       		= $ke_yth;
		$surat_keluars->perihal       		= $hasil_prihal_v2;
		$surat_keluars->no_surat       		= $c;
		$surat_keluars->kata_awal       	= $jumlah_kata_sebelum_filter;
		$surat_keluars->kata_akhir      	= $jumlah_kata_sebelum_filter - $jumlah_kata_sesudah_filter;
		$surat_keluars->save();

		// redirect
		return Redirect::action('admin\SuratKeluarController@index');
	}

	public function show($id)
	{
	$surat_keluars = SuratKeluar::leftJoin('users', 'users.id', '=', 'surat_keluars.id_pengolah')
									->leftJoin('penanggung_jawabs', 'penanggung_jawabs.id', '=', 'surat_keluars.id_penanggung_jawab')
									->leftJoin('jenis_surats', 'jenis_surats.id', '=', 'surat_keluars.id_jenis_surat')
									->leftJoin('list_kategoris', 'list_kategoris.nama', '=', 'surat_keluars.nm_ls_kategori')
									->leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_keluars.id_sifat_surat')
									->orderBy('surat_keluars.created_at', 'desc')
									->select(
												'surat_keluars.id',
												'surat_keluars.no_surat',
												'surat_keluars.tgl_surat',
												'surat_keluars.id_pengolah',
												'surat_keluars.tujuan',
												'surat_keluars.perihal',
												'surat_keluars.lampiran',
												'surat_keluars.keterangan',
												'surat_keluars.file',
												'users.name',
												'penanggung_jawabs.nama as nama_penanggungjawab',
												'jenis_surats.jenis_surat',
												'sifat_surats.sifat_surat',
												'list_kategoris.kode'

											)
									->findOrFail($id);
		return view('admin.surat_keluars.show', array('surat_keluars' => $surat_keluars));
	}

	public function edit($id)
	{
		$surat_keluars = SuratKeluar::leftJoin('users', 'users.id', '=', 'surat_keluars.id_pengolah')
									->leftJoin('penanggung_jawabs', 'penanggung_jawabs.id', '=', 'surat_keluars.id_penanggung_jawab')
									->leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_keluars.id_sifat_surat')
									->leftJoin('jenis_surats', 'jenis_surats.id', '=', 'surat_keluars.id_jenis_surat')
									->orderBy('surat_keluars.created_at', 'desc')
									->select(
												'surat_keluars.id',
												'surat_keluars.no_surat',
												'surat_keluars.tgl_surat',
												'surat_keluars.id_pengolah',
												'surat_keluars.tujuan',
												'surat_keluars.perihal',
												'surat_keluars.lampiran',
												'surat_keluars.keterangan',
												'surat_keluars.file',
												'users.name',
												'penanggung_jawabs.nama as nama_penanggungjawab',
												'jenis_surats.jenis_surat',
												'sifat_surats.sifat_surat'

											)
									->findOrFail($id);  
		$user = User::pluck('name', 'id');
		$jenis_surat = JenisSurat::pluck('jenis_surat', 'id');
		$kategori = Kategori::pluck('nama', 'id');
		$penanggung_jawab = PenanggungJawab::pluck('nama', 'id');
		$sifat_surats = SifatSurat::pluck('sifat_surat', 'id'); 
		return view('admin.surat_keluars.edit', array('kategori' => $kategori,'jenis_surat' => $jenis_surat,'surat_keluars' => $surat_keluars, 'user' => $user, 'penanggung_jawab' =>$penanggung_jawab, 'sifat_surats' =>$sifat_surats));
	}

	public function update(Request $request, $id)
	{

// 		echo "<pre>";
// print_r($request->all());
// echo "</pre>";
// exit(); 

		$noUrutAkhir = $id;
        $today_now = date('Y'); 
		$surat= Input::get('id2');
		$bulanRomawi = array("", "I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
        $today_now_a = date('Y'); 

		$generateregister = $surat."/".sprintf("%04s", abs($noUrutAkhir))."/"."Kel-Skjt";

		$surat_keluars = SuratKeluar::findOrFail($id);
		$surat_keluars->no_surat       		= $generateregister;
		$surat_keluars->tgl_surat       	= Input::get('tgl_surat');
		$surat_keluars->id_pengolah       	= Input::get('id');
		$surat_keluars->id_jenis_surat      = Input::get('id_jenis_surat');
		$surat_keluars->id_sifat_surat      = Input::get('id3');
		$surat_keluars->id_penanggung_jawab = Input::get('id3');
		$surat_keluars->tujuan       		= Input::get('tujuan');
		$surat_keluars->perihal       		= Input::get('perihal');
		$surat_keluars->lampiran       		= Input::get('lampiran');
		$surat_keluars->keterangan       	= Input::get('keterangan');
		$surat_keluars->nm_ls_kategori  	= Input::get('nama_list');
		$surat_keluars->id_kategori 	   	= Input::get('kategory');

		$files = Input::file('file');
	      if(isset($files)){
	        $fileName = str_random(10).'.'.$files->getClientOriginalExtension();
	        $destinationPath = public_path('/file/suratkeluar');
	        if(!File::exists($destinationPath)){
	          if(File::makeDirectory($destinationPath,0777,true)){
	              throw new \Exception("Unable to upload to invoices directory make sure it is read / writable.");  
	          }
	        }
        	$files->move($destinationPath,$fileName);
        	$surat_keluars->file   = $fileName;
	    }
		$surat_keluars->save();
		// redirect
		return Redirect::action('admin\SuratKeluarController@index');
	}

	public function destroy($id)
	{    
		$surat_keluars = SuratKeluar::findOrFail($id);
		$surat_keluars->delete();
		return Redirect::action('admin\SuratKeluarController@index')->with('flash-success','The user has been deleted.');
	}

	public function search(Request $request)
	{
		// print_r('aakaka');
		// exit();
	$surat_keluars = SuratKeluar:: leftJoin('users', 'users.id', '=', 'surat_keluars.id_pengolah')
									->leftJoin('penanggung_jawabs', 'penanggung_jawabs.id', '=', 'surat_keluars.id_penanggung_jawab')
									->leftJoin('jenis_surats', 'jenis_surats.id', '=', 'surat_keluars.id_jenis_surat')
									->leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_keluars.id_sifat_surat')
									->leftJoin('list_kategoris', 'list_kategoris.nama', '=', 'surat_keluars.nm_ls_kategori')
									->leftJoin('kategoris', 'kategoris.id', '=', 'surat_keluars.id_kategori')
									->orderBy('surat_keluars.created_at', 'desc')
									->select(
												'surat_keluars.id',
												'surat_keluars.no_surat',
												'surat_keluars.tgl_surat',
												'surat_keluars.id_pengolah',
												'surat_keluars.tujuan',
												'surat_keluars.perihal',
												'surat_keluars.lampiran',
												'surat_keluars.keterangan',
												'surat_keluars.file',
												'users.name',
												'list_kategoris.kode',
												'penanggung_jawabs.nama as nama_penanggungjawab',
												'jenis_surats.jenis_surat',
												'sifat_surats.sifat_surat',
												'kategoris.nama as nama_kategori'

											);

    if($request->has('search')){
    $search = $request->get('search');
    $surat_keluars = $surat_keluars->Where("surat_keluars.no_surat", "like", "%".$search."%")
                     			->orWhere("jenis_surats.jenis_surat", "like", "%".$search."%")
                     			->orWhere("sifat_surats.sifat_surat", "like", "%".$search."%")
                     			->orWhere("surat_keluars.tgl_surat", "like", "%".$search."%")
                     			->orWhere("surat_keluars.tujuan", "like", "%".$search."%")
                     			->orWhere("surat_keluars.perihal", "like", "%".$search."%")
                     			->orWhere("surat_keluars.lampiran", "like", "%".$search."%")
                     			->orWhere("surat_keluars.keterangan", "like", "%".$search."%")
                     			->orWhere("penanggung_jawabs.nama", "like", "%".$search."%")
                     			->orWhere("kategoris.nama", "like", "%".$search."%")
                     			->orWhere("users.name", "like", "%".$search."%");
    }
    $surat_keluars = $surat_keluars->paginate(10);

    $tgl_surat = SuratKeluar::all()->toArray();
    $tgl_surat = collect(array_merge(['' => ['id' => '','tgl_surat' => 'Masukan TGL']], $tgl_surat));
    $tgl_surats  = $tgl_surat->pluck('tgl_surat', 'tgl_surat');

    $start_page= (($surat_keluars->currentPage()-1) * 10) + 1;
		return view('admin.Laporan.laporansuratkeluar', compact('surat_keluars','start_page', 'tgl_surats'));
	}

	public function searchss(Request $request)
	{
		// print_r('aakaka');
		// exit();
	$surat_keluars = SuratKeluar:: leftJoin('users', 'users.id', '=', 'surat_keluars.id_pengolah')
									->leftJoin('penanggung_jawabs', 'penanggung_jawabs.id', '=', 'surat_keluars.id_penanggung_jawab')
									->leftJoin('jenis_surats', 'jenis_surats.id', '=', 'surat_keluars.id_jenis_surat')
									->leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_keluars.id_sifat_surat')
									->leftJoin('list_kategoris', 'list_kategoris.nama', '=', 'surat_keluars.nm_ls_kategori')
									->leftJoin('kategoris', 'kategoris.id', '=', 'surat_keluars.id_kategori')
									->orderBy('surat_keluars.created_at', 'desc')
									->select(
												'surat_keluars.id',
												'surat_keluars.no_surat',
												'surat_keluars.tgl_surat',
												'surat_keluars.id_pengolah',
												'surat_keluars.tujuan',
												'surat_keluars.perihal',
												'surat_keluars.lampiran',
												'surat_keluars.keterangan',
												'surat_keluars.file',
												'users.name',
												'list_kategoris.kode',
												'penanggung_jawabs.nama as nama_penanggungjawab',
												'jenis_surats.jenis_surat',
												'sifat_surats.sifat_surat',
												'kategoris.nama as nama_kategori'

											);

    if($request->has('search')){
    $search = $request->get('search');
    $surat_keluars = $surat_keluars->Where("surat_keluars.no_surat", "like", "%".$search."%")
                     			->orWhere("jenis_surats.jenis_surat", "like", "%".$search."%")
                     			->orWhere("sifat_surats.sifat_surat", "like", "%".$search."%")
                     			->orWhere("surat_keluars.tgl_surat", "like", "%".$search."%")
                     			->orWhere("surat_keluars.tujuan", "like", "%".$search."%")
                     			->orWhere("surat_keluars.perihal", "like", "%".$search."%")
                     			->orWhere("surat_keluars.lampiran", "like", "%".$search."%")
                     			->orWhere("surat_keluars.keterangan", "like", "%".$search."%")
                     			->orWhere("penanggung_jawabs.nama", "like", "%".$search."%")
                     			->orWhere("kategoris.nama", "like", "%".$search."%")
                     			->orWhere("users.name", "like", "%".$search."%");
    }
    $surat_keluars = $surat_keluars->paginate(10);

    $tgl_surat = SuratKeluar::all()->toArray();
    	$tgl_surat = collect(array_merge(['' => ['id' => '','tgl_surat' => 'Masukan TGL']], $tgl_surat));
    	$tgl_surats  = $tgl_surat->pluck('tgl_surat', 'tgl_surat');
		
    $start_page= (($surat_keluars->currentPage()-1) * 10) + 1;
		return view('admin.surat_keluars.index', compact('surat_keluars','start_page', 'tgl_surats'));
	}

public function searchs(Request $request)
	{
		// print_r('aakaka');
		// exit();
	$surat_keluars = SuratKeluar:: leftJoin('users', 'users.id', '=', 'surat_keluars.id_pengolah')
									->leftJoin('penanggung_jawabs', 'penanggung_jawabs.id', '=', 'surat_keluars.id_penanggung_jawab')
									->leftJoin('jenis_surats', 'jenis_surats.id', '=', 'surat_keluars.id_jenis_surat')
									->leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_keluars.id_sifat_surat')
									->leftJoin('list_kategoris', 'list_kategoris.nama', '=', 'surat_keluars.nm_ls_kategori')
									->leftJoin('kategoris', 'kategoris.id', '=', 'surat_keluars.id_kategori')
									->orderBy('surat_keluars.created_at', 'desc')
									->select(
												'surat_keluars.id',
												'surat_keluars.no_surat',
												'surat_keluars.tgl_surat',
												'surat_keluars.id_pengolah',
												'surat_keluars.tujuan',
												'surat_keluars.perihal',
												'surat_keluars.lampiran',
												'surat_keluars.keterangan',
												'surat_keluars.file',
												'users.name',
												'list_kategoris.kode',
												'penanggung_jawabs.nama as nama_penanggungjawab',
												'jenis_surats.jenis_surat',
												'sifat_surats.sifat_surat',
												'kategoris.nama as nama_kategori'

											);

    if($request->has('search')){
    $search = $request->get('search');
    $surat_keluars = $surat_keluars->Where("surat_keluars.no_surat", "like", "%".$search."%")
                     			->orWhere("jenis_surats.jenis_surat", "like", "%".$search."%")
                     			->orWhere("sifat_surats.sifat_surat", "like", "%".$search."%")
                     			->orWhere("surat_keluars.tgl_surat", "like", "%".$search."%")
                     			->orWhere("surat_keluars.tujuan", "like", "%".$search."%")
                     			->orWhere("surat_keluars.perihal", "like", "%".$search."%")
                     			->orWhere("surat_keluars.lampiran", "like", "%".$search."%")
                     			->orWhere("surat_keluars.keterangan", "like", "%".$search."%")
                     			->orWhere("penanggung_jawabs.nama", "like", "%".$search."%")
                     			->orWhere("kategoris.nama", "like", "%".$search."%")
                     			->orWhere("users.name", "like", "%".$search."%");
    }
    $surat_keluars = $surat_keluars->paginate(10);

    $tgl_surat = SuratKeluar::all()->toArray();
    	$tgl_surat = collect(array_merge(['' => ['id' => '','tgl_surat' => 'Masukan TGL']], $tgl_surat));
    	$tgl_surats  = $tgl_surat->pluck('tgl_surat', 'tgl_surat');
		
    $start_page= (($surat_keluars->currentPage()-1) * 10) + 1;
		return view('admin.surat_keluars.index', compact('surat_keluars','start_page', 'tgl_surats'));
	}

	public function laporansurat(Request $request)
	{
	$todayDate = Date("d-m-Y");
	$surat_keluars = SuratKeluar:: leftJoin('users', 'users.id', '=', 'surat_keluars.id_pengolah')
									->leftJoin('penanggung_jawabs', 'penanggung_jawabs.id', '=', 'surat_keluars.id_penanggung_jawab')
									->leftJoin('jenis_surats', 'jenis_surats.id', '=', 'surat_keluars.id_jenis_surat')
									->leftJoin('list_kategoris', 'list_kategoris.nama', '=', 'surat_keluars.nm_ls_kategori')
									->leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_keluars.id_sifat_surat')
									->orderBy('surat_keluars.created_at', 'desc')
									->select(
												'surat_keluars.id',
												'surat_keluars.no_surat',
												'surat_keluars.tgl_surat',
												'surat_keluars.id_pengolah',
												'surat_keluars.tujuan',
												'surat_keluars.perihal',
												'surat_keluars.lampiran',
												'surat_keluars.keterangan',
												'surat_keluars.file',
												'users.name',
												'list_kategoris.kode',
												'penanggung_jawabs.nama as nama_penanggungjawab',
												'jenis_surats.jenis_surat',
												'sifat_surats.sifat_surat'
											);
	    
		$tgl_awal = SuratKeluar::all()->toArray();
    	$tgl_awal = collect(array_merge(['' => ['id' => '','tgl_surat' => 'Pilih tgl asal']], $tgl_awal));
    	$tgls_awal  = $tgl_awal->pluck('tgl_surat', 'tgl_surat');

    	$tgl_akhir = SuratKeluar::all()->toArray();
    	$tgl_akhir = collect(array_merge(['' => ['id' => '','tgl_surat' => 'Pilih tgl tujuan']], $tgl_akhir));
    	$tgls_akhir  = $tgl_akhir->pluck('tgl_surat', 'tgl_surat');

    	   			$surat_keluars=$surat_keluars->paginate(10);
/*
		echo "<pre>";
		print_r ($surat_keluars);
		echo "</pre>";
		exit();*/
		$start_page= (($surat_keluars->currentPage()-1) * 10) + 1;

		return view('admin.Laporan.laporansuratkeluar',array('todayDate' => $todayDate, 'tgls_awals' => $tgls_awal, 'tgls_akhir' => $tgls_akhir, 'surat_keluars' => $surat_keluars, 'start_page'=>$start_page));
	}

public function searchlaporansurat(Request $request)
	{

		/*echo "<pre>";
		print_r($request->all());
		echo "</pre>";
		exit();*/
		$todayDate = Date("d-m-Y");
		$surat_keluars = SuratKeluar:: leftJoin('users', 'users.id', '=', 'surat_keluars.id_pengolah')
									->leftJoin('penanggung_jawabs', 'penanggung_jawabs.id', '=', 'surat_keluars.id_penanggung_jawab')
									->leftJoin('jenis_surats', 'jenis_surats.id', '=', 'surat_keluars.id_jenis_surat')
									->leftJoin('list_kategoris', 'list_kategoris.nama', '=', 'surat_keluars.nm_ls_kategori')
									->leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_keluars.id_sifat_surat')
									->orderBy('surat_keluars.created_at', 'desc')
									->select(
												'surat_keluars.id',
												'surat_keluars.no_surat',
												'surat_keluars.tgl_surat',
												'surat_keluars.id_pengolah',
												'surat_keluars.tujuan',
												'surat_keluars.perihal',
												'surat_keluars.lampiran',
												'surat_keluars.keterangan',
												'surat_keluars.file',
												'users.name',
												'list_kategoris.kode',
												'penanggung_jawabs.nama as nama_penanggungjawab',
												'jenis_surats.jenis_surat',
												'sifat_surats.sifat_surat'
											);
	    
	$tgl_awal = SuratKeluar::all()->toArray();
    $tgl_awal = collect(array_merge(['' => ['id' => '','tgl_surat' => 'Pilih tgl asal']], $tgl_awal));
    $tgls_awals = $tgl_awal->pluck('tgl_surat', 'tgl_surat');

    $tgl_akhi = SuratKeluar::all()->toArray();
    $tgl_akhi = collect(array_merge(['' => ['id' => '','tgl_surat' => 'Pilih tgl tujuan']], $tgl_akhi));
    $tgls_akhir  = $tgl_akhi->pluck('tgl_surat', 'tgl_surat');

	$surat_keluars=$surat_keluars->paginate(10);

    $start_page= (($surat_keluars->currentPage()-1) * 10) + 1;
		return view('admin.Laporan.laporansuratkeluar',array('todayDate' => $todayDate, 'tgls_awals' => $tgls_awals, 'tgls_akhir' => $tgls_akhir, 'surat_keluars' => $surat_keluars, 'start_page'=>$start_page));
	}

	public function download($file)
	{
		$filepath = public_path('/file/suratkeluar/').$file;
		// print_r($filepath);
		// exit();
		return Response::download($filepath);

	}

}