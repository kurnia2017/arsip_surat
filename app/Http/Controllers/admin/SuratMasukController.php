<?php

namespace App\Http\Controllers\admin;

use DB;
use thiagoalessio\TesseractOCR\TesseractOCR;
use View;
use Auth;
use Image;
use File;
use Response;
use App\SuratMasuk;
use App\JenisSurat;
use App\Kategori;
use App\SifatSurat;
use App\Referensi;
use App\User;
use App\FilePdf;
use Spatie\PdfToText\Pdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
class SuratMasukController extends Controller
{
	public function __construct()
	 {
	    $this->middleware('auth');
	 }
	  
	public function index()
	{
		$kategori = Kategori::pluck('nama', 'nama');
		$surat_masuks = SuratMasuk::leftJoin('jenis_surats', 'jenis_surats.id', '=', 'surat_masuks.id_jenis_surat')
									->leftJoin('kategoris', 'kategoris.id', '=', 'surat_masuks.id_kategori')
									->leftJoin('list_kategoris', 'list_kategoris.nama', '=', 'surat_masuks.nm_ls_kategori')
									->leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_masuks.id_sifat_surat')
									->orderBy('surat_masuks.created_at', 'desc')
									->select(
												'surat_masuks.*',
												'kategoris.nama as nama_kategori',
												'kategoris.kode_kategori',
												'list_kategoris.nama as nama_list',
												'list_kategoris.kode',
												'jenis_surats.jenis_surat',
												'sifat_surats.sifat_surat'
											);
		$surat_masuks=$surat_masuks->paginate(10);
        $surat = SuratMasuk::orderBy('created_at', 'desc')->count();

        $tgls_awal = SuratMasuk::all()->toArray();
    	$tgls_awal = collect(array_merge(['' => ['id' => '','tgl_penerima' => 'Masukan TGL']], $tgls_awal));
    	$tgls_awals  = $tgls_awal->pluck('tgl_penerima', 'tgl_penerima');


		// echo "<pre>";
		// print_r($surat_masuks);
		// echo "</pre>";
		// exit();
		$start_page= (($surat_masuks->currentPage()-1) * 10) + 1;
		return view('admin.surat_masuks.index',array('surat_masuks' => $surat_masuks, 'start_page'=>$start_page, 'kategori'=> $kategori, 'tgls_awals'=> $tgls_awals));
	} 

	/*public function metode()
	{
		//segera
        $dinas_pangan_segera = SuratMasuk::leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_masuks.id_sifat_surat')
        					->Where('surat_masuks.sumber', 'dinas pangan')
        					->Where('sifat_surats.sifat_surat', 'segera')
        					->count();

		$diskom_info_segera = SuratMasuk::leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_masuks.id_sifat_surat')
        					->Where('surat_masuks.sumber', 'dinas komunikasi dan informatika')
        					->Where('sifat_surats.sifat_surat', 'segera')
        					->count();

        $sekretariat_daerah_segera = SuratMasuk::leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_masuks.id_sifat_surat')
        					->Where('surat_masuks.sumber', 'sekretariat daerah')
        					->Where('sifat_surats.sifat_surat', 'segera')
        					->count();
        //biasa
        $sekretariat_daerah_biasa = SuratMasuk::leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_masuks.id_sifat_surat')
        					->Where('surat_masuks.sumber', 'sekretariat daerah')
        					->Where('sifat_surats.sifat_surat', 'biasa')
        					->count();

         $walikota_bandung_biasa = SuratMasuk::leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_masuks.id_sifat_surat')
        					->Where('surat_masuks.sumber', 'walikota bandung')
        					->Where('sifat_surats.sifat_surat', 'biasa')
        					->count();
        //sangatpenting
		$diskom_info_sangat_penting = SuratMasuk::leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_masuks.id_sifat_surat')
        					->Where('surat_masuks.sumber', 'dinas komunikasi dan informatika')
        					->Where('sifat_surats.sifat_surat', 'sangat penting')
        					->count();

		$walikota_sangat_penting = SuratMasuk::leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_masuks.id_sifat_surat')
        					->Where('surat_masuks.sumber', 'walikota bandung')
        					->Where('sifat_surats.sifat_surat', 'sangat penting')
        					->count();
        //penting
        $sekretariat_penting = SuratMasuk::leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_masuks.id_sifat_surat')
        					->Where('surat_masuks.sumber', 'sekretariat daerah')
        					->Where('sifat_surats.sifat_surat', 'penting')
        					->count();

        $buahbatu_penting = SuratMasuk::leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_masuks.id_sifat_surat')
        					->Where('surat_masuks.sumber', 'kec buah batu')
        					->Where('sifat_surats.sifat_surat', 'penting')
        					->count();



		// echo "<pre>";
		// echo "<pre>";
		// print_r($surat);
		// echo "</pre>";
		// exit();
		return view('admin.surat_masuks.metode',array('dinas_pangan_segera' => $dinas_pangan_segera, 'diskom_info_segera' => $diskom_info_segera, 'sekretariat_daerah_segera' => $sekretariat_daerah_segera,  'sekretariat_daerah_biasa' => $sekretariat_daerah_biasa, 'walikota_bandung_biasa' => $walikota_bandung_biasa, 'diskom_info_sangat_penting' => $diskom_info_sangat_penting, 'walikota_sangat_penting' => $walikota_sangat_penting,'sekretariat_penting' => $sekretariat_penting, 'buahbatu_penting' => $buahbatu_penting ));
	} 
*/

	public function create()
	{
		$kategori = Kategori::pluck('nama', 'id');
		$jenis_surat = JenisSurat::pluck('jenis_surat', 'id');
		$sifat_surats = SifatSurat::pluck('sifat_surat', 'id');
		$surat_masuks = Kategori::leftJoin('list_kategoris', 'list_kategoris.id_kategori', '=', 'kategoris.id')
									->leftJoin('surat_masuks', 'surat_masuks.id_kategori', '=', 'kategoris.id')
									->leftJoin('jenis_surats', 'jenis_surats.id', '=', 'surat_masuks.id_jenis_surat')
									->leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_masuks.id_sifat_surat')
									->orderBy('kategoris.created_at', 'desc')
									->select(
												'jenis_surats.id',
												'surat_masuks.no_agenda',
												'surat_masuks.no_surat',
												'surat_masuks.tgl_pengirim',
												'surat_masuks.tgl_penerima',
												'surat_masuks.sumber',
												'surat_masuks.no_telpon',
												'surat_masuks.perihal',
												'surat_masuks.lampiran',
												'surat_masuks.keterangan',
												'surat_masuks.file',
												'surat_masuks.status',
												'kategoris.nama as nama_kategori',
												'kategoris.kode_kategori',
												'list_kategoris.nama as nama_list',
												'list_kategoris.id as id_list',
												'list_kategoris.kode',
												'jenis_surats.jenis_surat',
												'sifat_surats.sifat_surat'
											)
									->first();
		/*echo "<pre>";
		print_r($request->all());
		echo "</pre>";
		exit();*/
	    return View::make('admin.surat_masuks.create', compact( 'jenis_surat', 'surat_masuks', 'kategori', 'sifat_surats'));
	}

	public function store(Request $request)
	{


		$surat_masuks = new SuratMasuk;
		//$surat_masuks->no_surat        = Input::get('no_surat');
		$surat_masuks->id_jenis_surat  = Input::get('jenis_surat');
		$surat_masuks->id_sifat_surat  = Input::get('id3');
		$surat_masuks->no_agenda       = sprintf("%04s", abs($surat_masuks->max('id') + 1));
		$surat_masuks->tgl_pengirim	   = Input::get('tgl_pengirim');
		$surat_masuks->tgl_penerima	   = Input::get('tgl_penerima');
		//$surat_masuks->sumber          = Input::get('sumber');		
		//$surat_masuks->no_telpon       = Input::get('no_telpon');
		$surat_masuks->perihal         = Input::get('perihal');
		$surat_masuks->lampiran        = Input::get('lampiran');
		$surat_masuks->keterangan	   = Input::get('keterangan');
		$surat_masuks->id_kategori 	   = Input::get('kategory');
		$surat_masuks->nm_ls_kategori  = Input::get('nama_list');
		$surat_masuks->status 			= 1;

		$files = Input::file('file');
		// $filess = $files;
 	// 	if(isset($filess)){
	 //        $fileName = str_random(10).'.'.$filess->getClientOriginalExtension();
	 //       // $destinationPath = public_path('');
	 //        $destinationPath = public_path() . '/file/pdf/';
	 //        if(!File::exists($destinationPath)){
	 //          if(File::makeDirectory($destinationPath,0777,true)){
	 //              throw new \Exception("Unable to upload to invoices directory make sure it is read / writable.");  
	 //          }
	 //        }
  //       	$filess->move($destinationPath,$fileName);

	 //    }


		$name=$files->getClientOriginalName();
		$files->move(public_path().'/pdftotext/', $name);
		$path = public_path('/pdftotext/'. $name);
		$reader = new \Asika\Pdf2text;
		$output = $reader->decode($path);
        $surat_masuks->file_upload	   = $name;
		$data[] = $name;
		$outputs  = str_replace(array("\n" , "\r"), '', trim($output));


		
		//prihal
		$arr_kalimat = explode ("        ",$outputs);
		$arr_kalimat2 = explode ("    ",$outputs);
		
		$perihal = $arr_kalimat2[3];		
		$arr_kalimat1 = explode(": ",$perihal);
		$hasil_prihal = $arr_kalimat1[1];
		//end prihal


		//sumber
		$sumberr = $arr_kalimat[0];
		$arr_kalimats = explode("LANGSUNG SAJA KUNJUNGI KOSNGOSAN ",$sumberr);
		$sumberrs = $arr_kalimats[1];
		$arr_kalimatss = explode("Jl.",$sumberrs);
		$hasil_sumber = $arr_kalimatss[0];
		//end suber

		
		//no_surat
		$arr_nosurat = explode ("        ",$outputs);
		$arr_nosurat2 = explode ("    ",$outputs);
		$no = $arr_nosurat2[1];
		$a = explode (":",$no);
		$kosongs = $a[0];
		$kosongse = "";
		if ($kosongs == null ) {
			$c = '-';
		}else{
			if ($a[1] == null) {
				$c = '-';
			}else{
				$b = $a[1];
				$perihals = "Perihal"; 
				$c = str_replace($perihals,"",$b);		
			}
			
		}
		//no_surat

		//yth
		$arr_nosurat1 = explode ("        ",$outputs);
		$arr_nosurat2 = explode ("    ",$outputs);
		$yth = $arr_nosurat2[0];
		$ass = explode ("   ………………, ….  Juli 20… Nomor",$yth);
		$yths = $ass[0];
		//end_yth

		$reqcolaborate []= DB::table("referensis")
                     ->distinct()
                     ->pluck("nama");

		$data_retrun = [];
			foreach ($reqcolaborate as $i => $value) {
				if ($value) {
					foreach($value as $k_2 =>$v_2){
						$data_retrun[] = $v_2;
					}
					
				}
			}	

				for($i = 0; $i < count($data_retrun); $i++) {
		   				$string = str_replace($data_retrun[$i],"",$outputs);
				}
		//start pemanggilan data referensi 

		$array_referensi = Referensi::get();

		//end pemanggilan data referensi 

		
		//$jumlah_kata_sebelum_filter = count(explode(" ",$outputs)); 
		$jumlah_kata_sebelum_filter = str_word_count($outputs);
		$jumlah_kata_sesudah_filter = 0;
		 
		// start menjumlah seluruh kata dari data array

		$array_kata = explode(" ",$outputs);
		
		// end menjumlah seluruh kata dari data array

		foreach ($array_kata as $key => $value) {
			foreach ($array_referensi as $key => $value_array_referensi) {
				if (strtolower(trim($value_array_referensi->nama)) == strtolower(trim($value))) {
					$jumlah_kata_sesudah_filter ++;
				}
			}
		}
		
		$string_kata_setelah_filter = $string;


		foreach ($array_referensi as $key => $value_array_referensi) {
			$string_kata_setelah_filter = str_replace($value_array_referensi->nama,"",$string_kata_setelah_filter);
		}
			

		$surat_masuks->file_kata_akhir 		= $string_kata_setelah_filter;
		$surat_masuks->file 				= $string;
		$surat_masuks->sumber       		= $hasil_sumber;
		$surat_masuks->perihal       		= $hasil_prihal;
		$surat_masuks->no_surat       		= $c;
		$surat_masuks->kata_awal       	    = $jumlah_kata_sebelum_filter;
		$surat_masuks->kata_akhir      		= $jumlah_kata_sebelum_filter - $jumlah_kata_sesudah_filter;
		$surat_masuks->save();
 
		


		return Redirect::action('admin\SuratMasukController@index');
	}

	public function show($id)
	{
		$surat_masuks = SuratMasuk::leftJoin('kategoris', 'kategoris.id', '=', 'surat_masuks.id_kategori')
									->leftJoin('list_kategoris', 'list_kategoris.nama', '=', 'surat_masuks.nm_ls_kategori')
									->leftJoin('jenis_surats', 'jenis_surats.id', '=', 'surat_masuks.id_jenis_surat')
									->leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_masuks.id_sifat_surat')
									->orderBy('surat_masuks.created_at', 'desc')
									->select(
												'surat_masuks.id',
												'surat_masuks.no_agenda',
												'surat_masuks.no_surat',
												'surat_masuks.tgl_pengirim',
												'surat_masuks.tgl_penerima',
												'surat_masuks.sumber',
												'surat_masuks.no_telpon',
												'surat_masuks.perihal',
												'surat_masuks.lampiran',
												'surat_masuks.keterangan',
												'surat_masuks.file',
												'surat_masuks.status',
												'kategoris.nama as nama_kategori',
												'kategoris.kode_kategori',
												'list_kategoris.nama as nama_list',
												'list_kategoris.id as id_list',
												'list_kategoris.kode',
												'jenis_surats.jenis_surat',
												'sifat_surats.sifat_surat'
											)
									->findOrFail($id);

		return view('admin.surat_masuks.show', array('surat_masuks' => $surat_masuks));
		// echo "<pre>";
		// print_r($request->all());
		// echo "</pre>";
		// exit();
	}

	public function edit($id)
	{
		/*echo "<pre>";
		print_r($request->all());
		echo "</pre>";
		exit();*/
		$surat_masuks = SuratMasuk::findOrFail($id); 
		$kategori = Kategori::pluck('nama', 'id');
		$jenis_surat = JenisSurat::pluck('jenis_surat', 'id'); 
		$sifat_surats = SifatSurat::pluck('sifat_surat', 'id');
		return view('admin.surat_masuks.edit', array('surat_masuks' => $surat_masuks, 'kategori' => $kategori, 'jenis_surat' => $jenis_surat, 'sifat_surats' => $sifat_surats));
	}

	public function update(Request $request, $id)
	{

		// $this->validate($request, [
		// 'name'          	=> 'required|unique:surat_masuks|max:255',
		// 'unit'          	=> 'required|unique:surat_masuks|max:255',
		// ]);   

		$noUrutAkhir = $id;

		$surat_masuks = SuratMasuk::findOrFail($id);
		$surat_masuks->no_surat        = Input::get('no_surat');
		$surat_masuks->id_jenis_surat  = Input::get('id2');
		$surat_masuks->id_sifat_surat  = Input::get('id3');
		$surat_masuks->no_agenda       = sprintf("%04s", abs($noUrutAkhir));
		$surat_masuks->tgl_pengirim	   = Input::get('tgl_pengirim');
		$surat_masuks->tgl_penerima	   = Input::get('tgl_penerima');
		$surat_masuks->sumber          = Input::get('sumber');
		$surat_masuks->no_telpon       = Input::get('no_telpon');
		$surat_masuks->perihal         = Input::get('perihal');
		$surat_masuks->lampiran        = Input::get('lampiran');
		$surat_masuks->keterangan	   = Input::get('keterangan');
		$surat_masuks->id_kategori 	   = Input::get('kategory');
		$surat_masuks->nm_ls_kategori  = Input::get('nama_list');

		$files = Input::file('file');
	      if(isset($files)){
	        $fileName = str_random(10).'.'.$files->getClientOriginalExtension();
	        $destinationPath = public_path('/file/asset');
	        if(!File::exists($destinationPath)){
	          if(File::makeDirectory($destinationPath,0777,true)){
	              throw new \Exception("Unable to upload to invoices directory make sure it is read / writable.");  
	          }
	        }
        	$files->move($destinationPath,$fileName);
        	$surat_masuks->file   = $fileName;
	    }
		$surat_masuks->save();


		// redirect
		return Redirect::action('admin\SuratMasukController@index');
			// echo "<pre>";
			// print_r($surat_masuks);
			// echo "</pre>";
			// exit();
	}

	public function destroy($id)
	{    
		$surat_masuks = SuratMasuk::findOrFail($id);
		$surat_masuks->delete();
		return Redirect::action('admin\SuratMasukController@index')->with('flash-success','The user has been deleted.');
	}

	public function search(Request $request)
	{
		$kategori = Kategori::pluck('nama', 'nama');
		$surat_masuks = SuratMasuk::leftJoin('jenis_surats', 'jenis_surats.id', '=', 'surat_masuks.id_jenis_surat')
									->leftJoin('kategoris', 'kategoris.id', '=', 'surat_masuks.id_kategori')
									->leftJoin('list_kategoris', 'list_kategoris.nama', '=', 'surat_masuks.nm_ls_kategori')
									->leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_masuks.id_sifat_surat')
									->orderBy('surat_masuks.created_at', 'desc')
									->select(
												'surat_masuks.id',
												'surat_masuks.no_agenda',
												'surat_masuks.no_surat',
												'surat_masuks.tgl_pengirim',
												'surat_masuks.tgl_penerima',
												'surat_masuks.sumber',
												'surat_masuks.no_telpon',
												'surat_masuks.perihal',
												'surat_masuks.lampiran',
												'surat_masuks.keterangan',
												'surat_masuks.file',
												'surat_masuks.status',
												'kategoris.nama as nama_kategori',
												'kategoris.kode_kategori',
												'list_kategoris.nama as nama_list',
												'list_kategoris.kode',
												'jenis_surats.jenis_surat',
												'sifat_surats.sifat_surat'
											);
	if($request->has('search')){
    $search = $request->get('search');
    $surat_masuks = $surat_masuks->Where("surat_masuks.no_agenda", "like", "%".$search."%")
                     			->orWhere("surat_masuks.no_surat", "like", "%".$search."%")
                     			->orWhere("jenis_surats.jenis_surat", "like", "%".$search."%")
                     			->orWhere("sifat_surats.sifat_surat", "like", "%".$search."%")
                     			->orWhere("surat_masuks.tgl_penerima", "like", "%".$search."%")
                     			->orWhere("surat_masuks.tgl_pengirim", "like", "%".$search."%")
                     			->orWhere("surat_masuks.sumber", "like", "%".$search."%")
                     			->orWhere("surat_masuks.no_telpon", "like", "%".$search."%")
                     			->orWhere("surat_masuks.perihal", "like", "%".$search."%")
                     			->orWhere("surat_masuks.lampiran", "like", "%".$search."%")
                     			->orWhere("surat_masuks.file", "like", "%".$search."%")
                     			->orWhere("surat_masuks.keterangan", "like", "%".$search."%")
                     			->orWhere("surat_masuks.status", "like", "%".$search."%")
                     			->orWhere("kategoris.nama", "like", "%".$search."%")
                     			->orWhere("kategoris.kode_kategori", "like", "%".$search."%")
                     			->orWhere("list_kategoris.kode", "like", "%".$search."%")
                     			->orWhere("list_kategoris.nama", "like", "%".$search."%");
    }
    $surat_masuks = $surat_masuks->paginate(10);

    $tgls_awal = SuratMasuk::all()->toArray();
    $tgls_awal = collect(array_merge(['' => ['id' => '','tgl_penerima' => 'Masukan TGL']], $tgls_awal));
    $tgls_awals  = $tgls_awal->pluck('tgl_penerima', 'tgl_penerima');

    $start_page= (($surat_masuks->currentPage()-1) * 10) + 1;
		return view('admin.surat_masuks.index', compact('surat_masuks','start_page', 'kategori', 'tgls_awals'));
	}


	public function searchtgl(Request $request)
	{
		$surat_masuks = SuratMasuk::leftJoin('jenis_surats', 'jenis_surats.id', '=', 'surat_masuks.id_jenis_surat')
									->leftJoin('kategoris', 'kategoris.id', '=', 'surat_masuks.id_kategori')
									->leftJoin('list_kategoris', 'list_kategoris.nama', '=', 'surat_masuks.nm_ls_kategori')
									->leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_masuks.id_sifat_surat')
									->orderBy('surat_masuks.created_at', 'desc')
									->select(
												'surat_masuks.id',
												'surat_masuks.no_agenda',
												'surat_masuks.no_surat',
												'surat_masuks.tgl_pengirim',
												'surat_masuks.tgl_penerima',
												'surat_masuks.sumber',
												'surat_masuks.no_telpon',
												'surat_masuks.perihal',
												'surat_masuks.lampiran',
												'surat_masuks.keterangan',
												'surat_masuks.file',
												'surat_masuks.status',
												'kategoris.nama as nama_kategori',
												'kategoris.kode_kategori',
												'list_kategoris.nama as nama_list',
												'list_kategoris.kode',
												'jenis_surats.jenis_surat',
												'sifat_surats.sifat_surat'
											);
	if($request->has('search')){
    $search = $request->get('search');
    $surat_masuks = $surat_masuks->Where("surat_masuks.tgl_penerima", "like", "%".$search."%");
    }
    $surat_masuks = $surat_masuks->paginate(10);

    $tgls_awal = SuratMasuk::all()->toArray();
    $tgls_awal = collect(array_merge(['' => ['id' => '','tgl_penerima' => 'Masukan TGL']], $tgls_awal));
    $tgls_awals  = $tgls_awal->pluck('tgl_penerima', 'tgl_penerima');

    $start_page= (($surat_masuks->currentPage()-1) * 10) + 1;
		return view('admin.surat_masuks.index', compact('surat_masuks','start_page', 'kategori', 'tgl', 'tgls_awals'));
	}

	public function searchkategori(Request $request)
	{
		$kategori = Kategori::pluck('nama', 'nama');
		$surat_masuks = SuratMasuk::leftJoin('jenis_surats', 'jenis_surats.id', '=', 'surat_masuks.id_jenis_surat')
									->leftJoin('kategoris', 'kategoris.id', '=', 'surat_masuks.id_kategori')
									->leftJoin('list_kategoris', 'list_kategoris.nama', '=', 'surat_masuks.nm_ls_kategori')
									->leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_masuks.id_sifat_surat')
									->orderBy('surat_masuks.created_at', 'desc')
									->select(
												'surat_masuks.id',
												'surat_masuks.no_agenda',
												'surat_masuks.no_surat',
												'surat_masuks.tgl_pengirim',
												'surat_masuks.tgl_penerima',
												'surat_masuks.sumber',
												'surat_masuks.no_telpon',
												'surat_masuks.perihal',
												'surat_masuks.lampiran',
												'surat_masuks.keterangan',
												'surat_masuks.file',
												'surat_masuks.status',
												'kategoris.nama as nama_kategori',
												'kategoris.kode_kategori',
												'list_kategoris.nama as nama_list',
												'list_kategoris.kode',
												'jenis_surats.jenis_surat',
												'sifat_surats.sifat_surat'
											);
	if($request->has('search')){
    $search = $request->get('search');
    $surat_masuks = $surat_masuks->Where("kategoris.nama", "like", "%".$search."%");
    }
    $surat_masuks = $surat_masuks->paginate(10);

    $start_page= (($surat_masuks->currentPage()-1) * 10) + 1;
		return view('admin.surat_masuks.index', compact('surat_masuks','start_page','kategori'));
	}
	
	public function laporansurat(Request $request)
	{

		$todayDate = Date("d-m-Y");
		$surat_masuks = SuratMasuk::leftJoin('list_kategoris', 'list_kategoris.nama', '=', 'surat_masuks.nm_ls_kategori')
									->leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_masuks.id_sifat_surat')
									->leftJoin('jenis_surats', 'jenis_surats.id', '=', 'surat_masuks.id_jenis_surat')
									->leftJoin('kategoris', 'kategoris.id', '=', 'surat_masuks.id_kategori')
									->orderBy('surat_masuks.created_at', 'desc')

									->select(
												'surat_masuks.id',
												'surat_masuks.no_agenda',
												'surat_masuks.no_surat',
												'surat_masuks.tgl_pengirim',
												'surat_masuks.tgl_penerima',
												'surat_masuks.sumber',
												'surat_masuks.no_telpon',
												'surat_masuks.perihal',
												'surat_masuks.lampiran',
												'surat_masuks.keterangan',
												'surat_masuks.file',
												'surat_masuks.status',
												'list_kategoris.kode',
												'jenis_surats.jenis_surat',
												'sifat_surats.sifat_surat',
												'kategoris.nama as nama_kategori',
												'kategoris.kode_kategori'
											);
									// ->get();

		$tgls_awal = SuratMasuk::all()->toArray();
    	$tgls_awal = collect(array_merge(['' => ['id' => '','tgl_penerima' => 'Pilih tgl asal']], $tgls_awal));
    	$tgls_awals  = $tgls_awal->pluck('tgl_penerima', 'tgl_penerima');

    	$tgls_akhi = SuratMasuk::all()->toArray();
    	$tgls_akhi = collect(array_merge(['' => ['id' => '','tgl_penerima' => 'Pilih tgl tujuan']], $tgls_akhi));
    	$tgls_akhir  = $tgls_akhi->pluck('tgl_penerima', 'tgl_penerima');

		$surat_masuks=$surat_masuks->paginate(10);
/*
		echo "<pre>";
		print_r ($surat_masuks);
		echo "</pre>";
		exit();*/
		$start_page= (($surat_masuks->currentPage()-1) * 10) + 1;
		return view('admin.Laporan.laporansuratmasuk',array('todayDate' => $todayDate, 'tgls_awals' => $tgls_awals, 'tgls_akhir' => $tgls_akhir, 'surat_masuks' => $surat_masuks, 'start_page'=>$start_page));
	} 

	public function searchlaporansurat(Request $request)
	{	
		$tanggal_awal = $request->get('tgl_awal');
		$tanggal_akhir = $request->get('tgl_akhi');
		// echo "<pre>";
		// print_r($tanggal_awal);
		// // print_r($request->all());
		// echo "</pre>";
		// print_r($tanggal_akhir);
		// exit();

		$todayDate = Date("d-m-Y");
		$surat_masuks = SuratMasuk::leftJoin('list_kategoris', 'list_kategoris.nama', '=', 'surat_masuks.nm_ls_kategori')
									->leftJoin('sifat_surats', 'sifat_surats.id', '=', 'surat_masuks.id_sifat_surat')
									->leftJoin('jenis_surats', 'jenis_surats.id', '=', 'surat_masuks.id_jenis_surat')
									->leftJoin('kategoris', 'kategoris.id', '=', 'surat_masuks.id_kategori')
									->orderBy('surat_masuks.created_at', 'desc')
									->select(
												'surat_masuks.id',
												'surat_masuks.no_agenda',
												'surat_masuks.no_surat',
												'surat_masuks.tgl_pengirim',
												'surat_masuks.tgl_penerima',
												'surat_masuks.sumber',
												'surat_masuks.no_telpon',
												'surat_masuks.perihal',
												'surat_masuks.lampiran',
												'surat_masuks.keterangan',
												'surat_masuks.file',
												'surat_masuks.status',
												'jenis_surats.jenis_surat',
												'sifat_surats.sifat_surat',
												'kategoris.nama as nama_kategori'

											);
									// ->get();

	$surat_masuks=$surat_masuks->paginate(10);
    $start_page= (($surat_masuks->currentPage()-1) * 10) + 1;
		foreach ($surat_masuks as $key => $surat_masuk) {
			if ($surat_masuk->tgl_penerima >= $tanggal_awal) {
				if ($surat_masuk->tgl_penerima <= $tanggal_akhir) {
					$surat_masuk_looping[] = $surat_masuk; 
				}
			}
		}
	$surat_masuks = $surat_masuk_looping;
		// echo "<pre>";
		// print_r($surat_masuks);
		// // print_r($request->all());
		// echo "</pre>";
		// exit();

	$tgls_awal = SuratMasuk::all()->toArray();
    $tgls_awal = collect(array_merge(['' => ['id' => '','tgl_penerima' => 'Pilih tgl asal']], $tgls_awal));
    $tgls_awals  = $tgls_awal->pluck('tgl_penerima', 'tgl_penerima');

    $tgls_akhi = SuratMasuk::all()->toArray();
    $tgls_akhi = collect(array_merge(['' => ['id' => '','tgl_penerima' => 'Pilih tgl tujuan']], $tgls_akhi));
    $tgls_akhir  = $tgls_akhi->pluck('tgl_penerima', 'tgl_penerima');

	return view('admin.Laporan.laporansuratmasuk', compact('surat_masuk_looping', 'surat_masuks','start_page', 'todayDate', 'tgls_awals', 'tgls_akhir'));
	}

	public function download($file)
	{
		$filepath = public_path('/file/asset/').$file;
		// print_r($filepath);
		// exit();
		return Response::download($filepath);

	}
}	