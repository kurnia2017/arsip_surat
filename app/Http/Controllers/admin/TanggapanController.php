<?php

namespace App\Http\Controllers\admin;

use DB;
use View;
use Auth;
use Image;
use File;
use Response;
use App\Tanggapan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class TanggapanController extends Controller
{
    public function __construct()
	 {
	    $this->middleware('auth');
	 }
	  
	public function index()
	{
		$tanggapans = Tanggapan::orderBy('created_at', 'desc');
		$tanggapans=$tanggapans->paginate(10);
		$start_page= (($tanggapans->currentPage()-1) * 10) + 1;
		return view('admin.Tanggapans.index',array('tanggapans' => $tanggapans, 'start_page'=>$start_page));
	} 

	public function create()
	{
	    return View::make('admin.Tanggapans.create');
	}

	public function store(Request $request)
	{

		// $this->validate($request, [
		// 'nama'          => 'required|unique:surat_masuks|max:255',
		// ]);
		// store
		$tanggapans = new Tanggapan;
		$tanggapans->tanggapan      = Input::get('tanggapan');
		$tanggapans->save();

		return Redirect::action('admin\TanggapanController@index');
	}

	public function show($id)
	{
		$tanggapans = Tanggapan::findOrFail($id);
		return view('admin.Tanggapans.show', array('tanggapans' => $tanggapans));
	}

	public function edit($id)
	{
		$tanggapans = Tanggapan::findOrFail($id);     
		return view('admin.Tanggapans.edit', array('tanggapans' => $tanggapans));
	}

	public function update(Request $request, $id)
	{

		// $this->validate($request, [
		// 'name'          	=> 'required|unique:surat_masuks|max:255',
		// 'unit'          	=> 'required|unique:surat_masuks|max:255',
		// ]);   

		$tanggapans = Tanggapan::findOrFail($id);
		$tanggapans->tanggapan				= Input::get('tanggapan');
		$tanggapans->save();

		// redirect
		return Redirect::action('admin\TanggapanController@index');
	}

	public function destroy($id)
	{    
		$tanggapans = Tanggapan::findOrFail($id);
		$tanggapans->delete();
		return Redirect::action('admin\TanggapanController@index')->with('flash-success','The user has been deleted.');
	}

	public function search(Request $request)
	{
		$tanggapans = Tanggapan::orderBy('created_at', 'desc');
		if($request->has('search')){
	    $search = $request->get('search');
	    $tanggapans = $tanggapans->Where("tanggapans.tanggapan", "like", "%".$search."%");
	    }
	    $tanggapans = $tanggapans->paginate(10);

	    $start_page= (($tanggapans->currentPage()-1) * 10) + 1;
		return view('admin.Tanggapans.index', compact('tanggapans','start_page'));
	}

}
