<?php

namespace App\Http\Controllers\admin;

use DB;
use View;
use Auth;
use Response;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class UserController extends Controller
{
    public function index()
	{
		$users = User::leftJoin('groups', 'groups.id', '=', 'users.hak_akses')
									->orderBy('users.created_at', 'desc')
									->select(
											  'users.*',
											  'groups.name as nama'
											);
		$users=$users->paginate(10);
		$start_page= (($users->currentPage()-1) * 10) + 1;

		return view('admin.user.index',array('users' => $users, 'start_page'=>$start_page));
	} 

	public function search(Request $request)
	{
		$search = $request->get('search');
		$users = User::where('nama_belakang','LIKE','%'.$search.'%');
		$users = $users->paginate(10);

		$start_page= (($users->currentPage()-1) * 10) + 1;
		return view('admin.users.index', compact('users','start_page'));
	}

	public function create()
	{
	    return View::make('admin.user.create');
	}

	public function store(Request $request)
	{
/*echo "<pre>";
// print_r($surat);
print_r($generateregister);
// print_r($request->all());
echo "</pre>";
exit();*/
/*		$this->validate($request, [
		'nama'          => 'required|unique:perihals|max:255',
		]);
		// store*/
		$users = new User;
		$users->kode_petugas     	= Input::get('kode_petugas');
		$users->name       			= Input::get('nama_belakang');
		$users->email     		  	= Input::get('email');
		$users->password       		= bcrypt($request['password']);
		$users->user       			= Input::get('name');
		$users->status       		= Input::get('status');
		$users->hak_akses       	= input::get('name');
		$users->save();

		// redirect
		return Redirect::action('admin\UserController@index');
	}

	public function edit($id)
	{
		$users = User::findOrFail($id);     
		return view('admin.user.edit', array('users' => $users));
	}

	public function show($id)
	{
		$users = User::findOrFail($id);
		return view('admin.user.show', array('users' => $users));
	}

	public function update(Request $request, $id)
	{
		// $this->validate($request, [
		// 'name'          	=> 'required|unique:perihals|max:255',
		// 'unit'          	=> 'required|unique:perihals|max:255',
		// ]);   

		$users = User::findOrFail($id);
		$users->kode_petugas     	= Input::get('kode_petugas');
		$users->name       			= Input::get('nama_belakang');
		$users->email     		  	= Input::get('email');
		$users->password       		= bcrypt($request['password']);
		$users->user       			= Input::get('name');
		$users->status       		= Input::get('status');
		$users->hak_akses       	= input::get('name');
		$users->save();

		// redirect
		return Redirect::action('admin\UserController@index');
	}

	public function destroy($id)
	{    
		$users = User::findOrFail($id);
		$users->delete();
		return Redirect::action('admin\UserController@index')->with('flash-success','The user has been deleted.');
	}
}
