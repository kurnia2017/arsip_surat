<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
   
     protected $fillable = ['id', 'nama', 'kode_kategori'
	];
    
	public function listkategori()
    {
        return $this->hasMany('App\ListKategori','id_kategori');
    }
}

