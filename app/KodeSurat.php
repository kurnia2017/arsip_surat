<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KodeSurat extends Model
{
   protected $fillable = ['id','nama','kode_surat'
	];
}
