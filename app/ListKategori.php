<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ListKategori extends Model
{
    protected $fillable = ['id','nama' ,'kode', 'id_kategori'
	];

	public function kategori()
	{
		return $this->belongsTo('App\Kategori','id');
	}
}
