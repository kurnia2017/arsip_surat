<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
   protected $fillable = [
     'name', 'email', 'password'
    
];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function userGroup()
    {
        return $this->hasMany('App\UserGroup','user_id');
    }
    
    public function groups()
    {
      return $this->belongsToMany('App\Group', 'user_groups', 'user_id', 'group_id');
    }
    
    public function hasAnyRole($role)
    {
      if($this->groups()->join('group_roles', 'groups.id', '=', 'group_roles.group_id')->join('roles', 'roles.id', '=', 'group_roles.role_id')->where('roles.name', $role)->first()){
        return true;
      }else{
        return false;
      }
    }

    public function getUserByCategory($arr){
      return User::where($arr)->first();
    }

    public function hasAnyRoleApi($role, $user_id)
    {
      $user = User::join('user_groups','users.id','=','user_groups.user_id')
                  ->join('groups','user_groups.group_id','=','groups.id')
                  ->join('group_roles','groups.id','=','group_roles.group_id')
                  ->join('roles','group_roles.role_id','=','roles.id')
                  ->select('users.*','groups.id','groups.name','roles.id','roles.name')
                  ->where('users.id', $user_id)
                  ->where('roles.name', $role)->first();

      // print_r($user);
      // exit();

      return $user;
    }
}
