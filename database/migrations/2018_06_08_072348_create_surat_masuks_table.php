<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuratMasuksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat_masuks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_agenda')->length(10);
            $table->string('no_surat')->length(30);
            $table->text('tgl_pengirim');
            $table->text('tgl_penerima');
            $table->string('sumber')->length(25);
            $table->string('lampiran')->length(20);
            $table->string('keterangan')->nullable()->length(100);
            $table->text('file');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surat_masuks');
    }
}
