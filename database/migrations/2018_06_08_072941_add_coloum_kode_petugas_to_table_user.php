<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColoumKodePetugasToTableUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users',function ($table) {
            $table->string('status')->after('password');
            $table->string('kode_petugas')->after('id');
            $table->string('user')->after('status');
            $table->integer('hak_akses')->unsigned();
            $table->foreign('hak_akses')->references('id')->on('groups');


        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users',function ($table) {
            $table->dropForeign('users_hak_akses_foreign');
            $table->dropColumn('hak_akses');
            $table->dropColumn('status');
            $table->dropColumn('kode_petugas');
            $table->dropColumn('user');
        });
    }
}
