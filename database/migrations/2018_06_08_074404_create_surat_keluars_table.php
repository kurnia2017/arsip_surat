<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSuratKeluarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('surat_keluars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_surat')->length(30);
            $table->text('tgl_surat');
            $table->integer('id_pengolah')->unsigned();
            $table->string('tujuan')->length(30);
            $table->string('perihal')->length(30);
            $table->string('lampiran')->length(20);
            $table->string('keterangan')->nullable()->length(100);
            $table->text('file');
            $table->foreign('id_pengolah')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('surat_keluars');
    }
}
