<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColoumStatusInTableSuratMasuk extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
           Schema::table('surat_masuks',function ($table) {
            $table->tinyInteger('status')->unsigned()->nullable()->after('file')->length(30);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
          Schema::table('surat_masuks',function ($table) {
            $table->dropColumn('status');
        });
    }
}
