<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColoumPenanggungJawabToTableSuratKeluars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::table('surat_keluars',function ($table) {
            $table->integer('id_penanggung_jawab')->nullable()->after('id_pengolah');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surat_keluars',function ($table) {
            $table->dropColumn('id_penanggung_jawab');
        });
    }
}
