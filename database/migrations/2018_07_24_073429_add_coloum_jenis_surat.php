<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColoumJenisSurat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
               Schema::table('surat_masuks',function ($table) {
            $table->integer('id_jenis_surat')->nullable()->after('no_surat');

        });
                Schema::table('surat_keluars',function ($table) {
            $table->integer('id_jenis_surat')->nullable()->after('no_surat');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::table('surat_masuks',function ($table) {
            $table->dropColumn('id_jenis_surat');
        });
        Schema::table('surat_keluars',function ($table) {
            $table->dropColumn('id_jenis_surat');
        });
    }
}
