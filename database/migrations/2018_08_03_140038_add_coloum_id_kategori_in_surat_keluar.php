<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColoumIdKategoriInSuratKeluar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('surat_keluars',function ($table) {
            $table->integer('id_kategori')->nullable()->after('keterangan');
            $table->string('nm_ls_kategori')->length(50)->nullable()->after('id_kategori');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surat_keluars',function ($table) {
            $table->dropColumn('id_kategori');
            $table->dropColumn('nm_ls_kategori');
        });
    }
}
