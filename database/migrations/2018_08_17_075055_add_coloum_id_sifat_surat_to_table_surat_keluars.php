<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColoumIdSifatSuratToTableSuratKeluars extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::table('surat_keluars',function ($table) {
            $table->integer('id_sifat_surat')->nullable()->after('id_jenis_surat');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('surat_keluars',function ($table) {
            $table->dropColumn('id_sifat_surat');
        });
    }
}
