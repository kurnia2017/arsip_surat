<?php

use Illuminate\Database\Seeder;
use App\Kategori;


class KategoriTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
   public function run()
    {   
        $csv = array_map('str_getcsv', file(base_path()."/database/seeds/csv/kategori.csv"));
        foreach ($csv as $value) {
            $template = new Kategori();
            $template->id = $value[0];
            $template->nama = $value[1];
            $template->kode_kategori = $value[2];
            $template->save();
        }
    }
}
