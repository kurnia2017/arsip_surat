<?php

use Illuminate\Database\Seeder;
use App\ListKategori;

class ListKategoriTableSeeser extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $csv = array_map('str_getcsv', file(base_path()."/database/seeds/csv/listkategori.csv"));
        foreach ($csv as $value) {
            $template = new ListKategori();
            $template->id = $value[0];
            $template->nama = $value[1];
            $template->kode = $value[2];
            $template->id_kategori= $value[3];
            $template->save();
        }
    }
}
