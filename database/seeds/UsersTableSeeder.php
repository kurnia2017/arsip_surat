<?php

use Illuminate\Database\Seeder;
use App\Group;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $group  = Group::where('name', 'Admin Master')->first();
        $user_admin = new User();
        $user_admin->name = 'nia';
        $user_admin->user = 'Admin';
        $user_admin->kode_petugas = 'Poo21';
        $user_admin->email = 'admin@adminsurat.id';
        $user_admin->password = bcrypt('aaa123');
        $user_admin->status = 'aktif';
        $user_admin->hak_akses = '1';
        $user_admin->save();
        $user_admin->groups()->attach($group);

        $group  = Group::where('name', 'Sekretaris')->first();
        $user_admin = new User();
        $user_admin->name = 'kurnia';
        $user_admin->user = 'Sekretaris';
        $user_admin->kode_petugas = 'Poo21';
        $user_admin->email = 'sekretaris@adminsurat.id';
        $user_admin->password = bcrypt('aaa123');
        $user_admin->status = 'aktif';
        $user_admin->hak_akses = '2';
        $user_admin->save();
        $user_admin->groups()->attach($group);

        $group  = Group::where('name', 'lurah')->first();
        $user_admin = new User();
        $user_admin->name = 'Pak Lurah';
        $user_admin->user = 'Lurah';
        $user_admin->kode_petugas = 'Poo31';
        $user_admin->email = 'lurah@adminsurat.id';
        $user_admin->password = bcrypt('aaa123');
        $user_admin->status = 'aktif';
        $user_admin->hak_akses = '3';
        $user_admin->save();
        $user_admin->groups()->attach($group);
    }
}
