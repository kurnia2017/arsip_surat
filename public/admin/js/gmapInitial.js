function googleMap(MyMAP, Latparam, Longparam, LocaTion, isShowOnly){
	// Create a map object and specify the DOM element for display.
	var map,
		marker,
		myLatlng = new google.maps.LatLng(Latparam, Longparam),
		geocoder = new google.maps.Geocoder(),
		infowindow = new google.maps.InfoWindow();

	  var mapOptions = {
	    zoom: 18,
	    center: myLatlng,
	    mapTypeId: google.maps.MapTypeId.ROADMAP,
	    mapTypeControl: true,
	    mapTypeControlOptions: {
	        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
	        position: google.maps.ControlPosition.LEFT_BOTTOM
	    }
	  };

	  map = new google.maps.Map(document.getElementById(MyMAP), mapOptions);


	  marker = new google.maps.Marker({
	    map: map,
	    position: myLatlng,
	    draggable: isShowOnly == true ? false : true 
	  }); 

		google.maps.event.addListener(marker, 'dragend', function() {
			geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
			  if (status == google.maps.GeocoderStatus.OK){
			    if (results[0]) {
						var address = results[0].formatted_address; // LocaTion == null ? results[0].formatted_address : LocaTion;
			  		if(isShowOnly == false){
			        $('#address').val(address);
			        $('#latitude').val(marker.getPosition().lat());
			        $('#longitude').val(marker.getPosition().lng());
			      }
			      infowindow.setContent(address);
			      infowindow.open(map, marker);
			    }
			  }
			});
		});

		geocoder.geocode({'latLng': myLatlng }, function(results, status) { 
		 if(status == google.maps.GeocoderStatus.OK) {
				if(results[0]) {
					var address = LocaTion == null ? results[0].formatted_address : LocaTion;
					if(isShowOnly == false){
						$('#latitude, #longitude').show();
						$('#address').val(address);
						$('#latitude').val(marker.getPosition().lat());
						$('#longitude').val(marker.getPosition().lng());
					}

		     infowindow.setContent(address);
		     infowindow.open(map, marker);
		   }
		 }
		});

		if(isShowOnly == false){
			var searchBox = new google.maps.places.SearchBox(document.getElementById('address'));
			map.controls[google.maps.ControlPosition.TOP_CENTER].push(document.getElementById('address'));

			google.maps.event.addListener(searchBox, 'places_changed', function() {
			   searchBox.set('map', null);
			   var places = searchBox.getPlaces();
			   var bounds = new google.maps.LatLngBounds();
			   var i, place;

				 for (i = 0; place = places[i]; i++) {
					(function(place) {
						marker.setPosition(place.geometry.location);
						marker.bindTo('map', searchBox, 'map');
			   
						geocoder.geocode({'latLng': place.geometry.location}, function(results, status) {
						  if (status == google.maps.GeocoderStatus.OK){
						    if (results[0]) {
									var address = results[0].formatted_address; // LocaTion == null ? results[0].formatted_address : LocaTion;
						  		if(isShowOnly == false){
						        $('#address').val(address);
						        $('#latitude').val(marker.getPosition().lat());
						        $('#longitude').val(marker.getPosition().lng());
						      }
						      infowindow.setContent(address);
						      infowindow.open(map, marker);
						    }
						  }
						});

						google.maps.event.addListener(marker, 'dragend', function() {
						  geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
						    if (status == google.maps.GeocoderStatus.OK){
						      if (results[0]) {
										var address = results[0].formatted_address; // LocaTion == null ? results[0].formatted_address : LocaTion;
						    		if(isShowOnly == false){
						          $('#address').val(address);
						          $('#latitude').val(marker.getPosition().lat());
						          $('#longitude').val(marker.getPosition().lng());
						        }
						        infowindow.setContent(address);
						        infowindow.open(map, marker);
						      }
						    }
						  });
						});
			       google.maps.event.addListener(marker, 'dragend', function() {
			         if (!this.getMap()) {
			           this.unbindAll();
			         }
			       });
			       bounds.extend(place.geometry.location);
			     }(place));
			   }

			   map.fitBounds(bounds);
			   searchBox.set('map', map);
			   map.setZoom(Math.min(map.getZoom(),12));
			});
		}
}

function resetMap(MyMAP, lat, long, LocaTion){
	$('#address').remove();
	$('.hiddenlonglat').append('<input type="text" name="place" class="form-control required" id= "address" v-model="place"/>');
	setTimeout(function(){
		console.log('true');
		var map,
			marker,
			myLatlng = new google.maps.LatLng(lat,long),
			geocoder = new google.maps.Geocoder(),
			infowindow = new google.maps.InfoWindow();	

		var mapOptions = {
		  zoom: 18,
	    center: myLatlng,
	    mapTypeId: google.maps.MapTypeId.ROADMAP,
	    mapTypeControl: true,
	    mapTypeControlOptions: {
	        style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
	        position: google.maps.ControlPosition.LEFT_BOTTOM
	    }
		};

		map = new google.maps.Map(document.getElementById(MyMAP), mapOptions);

		marker = new google.maps.Marker({
		  map: map,
		  position: myLatlng,
		  draggable: true 
		}); 

		google.maps.event.addListener(marker, 'dragend', function() {
		  geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
		    if (status == google.maps.GeocoderStatus.OK){
		      if (results[0]) {
						var address = results[0].formatted_address; // LocaTion == null ? results[0].formatted_address : LocaTion;
	    	    $('#address').val(address);
	          $('#latitude').val(marker.getPosition().lat());
	          $('#longitude').val(marker.getPosition().lng());
		        
		        infowindow.setContent(address);
		        infowindow.open(map, marker);
		      }
		    }
		  });
		});


		var searchBox = new google.maps.places.SearchBox(document.getElementById('address'));

		map.controls[google.maps.ControlPosition.TOP_CENTER].push(document.getElementById('address'));
		
		geocoder.geocode({'latLng': myLatlng }, function(results, status) {
		  if(status == google.maps.GeocoderStatus.OK) {
				if(results[0]) {
					var address = LocaTion == null ? results[0].formatted_address : LocaTion;
					$('#latitude, #longitude').show();
					$('#address').val(address);
					$('#latitude').val(marker.getPosition().lat());
					$('#longitude').val(marker.getPosition().lng());

		      infowindow.setContent(address);
		      infowindow.open(map, marker);
		    }
		  }
		});

		google.maps.event.addListener(searchBox, 'places_changed', function() {
			searchBox.set('map', null);
			var places = searchBox.getPlaces();
			var bounds = new google.maps.LatLngBounds();
			var i, place;

			for (i = 0; place = places[i]; i++) {
				(function(place) {
					marker.setPosition(place.geometry.location);
					marker.bindTo('map', searchBox, 'map');

					geocoder.geocode({'latLng': place.geometry.location }, function(results, status) {
					  if(status == google.maps.GeocoderStatus.OK) {
							if(results[0]) {
								var address = results[0].formatted_address;
								$('#latitude, #longitude').show();
								$('#address').val(address);
								$('#latitude').val(marker.getPosition().lat());
								$('#longitude').val(marker.getPosition().lng());

					      infowindow.setContent(address);
					      infowindow.open(map, marker);
					    }
					  }
					});

					google.maps.event.addListener(marker, 'dragend', function() {
					  geocoder.geocode({'latLng': marker.getPosition()}, function(results, status) {
					    if (status == google.maps.GeocoderStatus.OK){
					      if (results[0]) {
									var address = results[0].formatted_address; // LocaTion == null ? results[0].formatted_address : LocaTion;
					  	    $('#address').val(address);
					        $('#latitude').val(marker.getPosition().lat());
					        $('#longitude').val(marker.getPosition().lng());
					        
					        infowindow.setContent(address);
					        infowindow.open(map, marker);
					      }
					    }
					  });
					});
					 google.maps.event.addListener(marker, 'dragend', function() {
					   if (!this.getMap()) {
					     this.unbindAll();
					   }
					 });
					 bounds.extend(place.geometry.location);
				}(place));
			}

			map.fitBounds(bounds);
			searchBox.set('map', map);
			map.setZoom(Math.min(map.getZoom(),12));
		});
	}, 500);
}