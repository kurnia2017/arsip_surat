@extends('layouts.admin')

@section('content')
	<section>
	<div class="col-md-4">
    <h1>
      Laporan Surat Masuk
    </h1>

  </section>
	<section class="content">
	  <div class="row">
	    <div class="col-md-12">
	    	<div class="col-md-12">
				{!! Form::open(['method'=>'GET','url'=>'searchlaporansuratmasuk','role'=>'search'])  !!}
         			<div class='form-group clearfix'>
              <div class='col-md-4'>
                <div class="input-group custom-search-form form-filter">
                  {{ Form::select('tgl_awal', $tgls_awals, null, [
                      'class' => 'form-control chosen-select'
                      
                  ]) }}
                            {{ Form::select('tgl_akhi', $tgls_akhir, null, [
                      'class' => 'form-control chosen-select'
                  ]) }}
                  <!-- <input type="text" class="form-control" name="search" placeholder="Search..."> -->
                  <span class="input-group-btn">
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i> Cari </button>
                    <a href="{{ url()->current() }}" class="btn btn-default">
                        <i class="fa fa-remove"></i> Ulang
                      </a>
                    @if(Auth::check())
			    		@if(Auth::user()->groups()->where("name", "=", "lurah")->first())

						@else
							<a href="{{ url('/prnsuratkeluar') }}" class="btnprn btn btn-default">Cetak</a></center>
						@endif
		      		@endif
                    </span>
                  </span>
                </div>
                <br>
              </div>
            </div>
          		{!! Form::close() !!}
			</div>
			<br>
			<div class="table-responsive">
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>No.</th>
			       			<th><b>No Agenda</b></th>
			       			<th><b>No Surat</b></th>
			       			<!-- <th><b>Jenis Surat</b></th>
			       			<th><b>Sifat Surat</b></th> -->
			       			<th><b>Tanggal Pengirim</b></th>
			       			<th><b>Tanggal Penerima</b></th>
			       			<th><b>Sumber</b></th>
			       			<th><b>Perihal</b></th>
			       			<th><b>Lampiran</b></th>
			       			<!-- <th><b>Keterangan</b></th> -->
			       			<th><b>Kategori</b></th>
			       			<th><b>File</b></th>
						</tr>
					</thead>
					<tstatus>
				   		@foreach($surat_masuks as $i=>$surat_masuk)
			     			<tr>
                                <td  class="nom-list">{{$start_page}}</td>
				         			<td> {{ $surat_masuk->no_agenda}} </td>     
				         			<td> {{ $surat_masuk->kode}}/{{ $surat_masuk->no_surat}} </td>     
				         			<!-- <td> {{ $surat_masuk->jenis_surat}} </td>     
				         			<td> {{ $surat_masuk->sifat_surat}} </td> -->     
				         			<td> {{ $surat_masuk->tgl_pengirim}} </td>     
				         			<td> {{ $surat_masuk->tgl_penerima}} </td>     
				         			<td> {{ $surat_masuk->sumber}} </td>    
				         			<td> {{ $surat_masuk->perihal}} </td>    
				         			<td> {{ $surat_masuk->lampiran}} </td>    
				         			<!-- <td> {{ $surat_masuk->keterangan}} </td>  -->
				         			<td> {{ $surat_masuk->nama_kategori}} </td> 
	                                <td>{{ $surat_masuk->file }} <br>
                              </td>
			     			</tr>
			     			<?php $start_page = $start_page+1 ?>
				   		@endforeach
					</tstatus>
				</table>
			</div>
				<div class="text-center">
				</div>
			</div>
		</div>
		<br>
      	<div class='col-md-12 text-center'>
     	 <p align="center">
             <a href="{{URL::to('/admin-dashboard')}}" class="btn btn-primary" role="button">kembali</a>
          </p>
        </div>
	</section>
@endsection
@section('js')
<script>
	$( document ).ready(function() {
		var message = '{{session('flash-error')}}';
		if(message!=''){
			alert('{{session('flash-error')}}');
		}
	})
</script>
<script type="text/javascript">
   $(function(){
     $(".datepicker").datepicker({
        format: 'dd-mm-yyyy',
        autoclose: true,
        todayHighlight: true,
    });
    $("#tgl_mulai").on('changeDate', function(selected) {
        var startDate = new Date(selected.date.valueOf());
        $("#tgl_akhir").datepicker('setStartDate', startDate);
        if($("#tgl_mulai").val() > $("#tgl_akhir").val()){
          $("#tgl_akhir").val($("#tgl_mulai").val());
        }
    });
 });
</script>
<script>
	$(document).ready(function(){

	  $('.btnprn').printPage();

	});
</script>
@endsection