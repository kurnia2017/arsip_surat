<!DOCTYPE html>
<html>
<style type="text/css">
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
<body>

<div class="w3-container">
  <h2>LAPORAN SURAT KELUAR</h2>
  <p>The w3-striped class adds zebra-stripes to a table:</p>

	<div class="table-responsive">
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>No.</th>
			       			<th><b>No Surat</b></th>
			       			<th><b>Tanggal Surat</b></th>
			       			<!-- <th><b>Jenis Surat</b></th>
			       			<th><b>Sifat Surat</b></th> -->
<!-- 			       			<th><b>Pengolah</b></th> -->
<!-- 			       			<th><b>PenanggungJawab Pengelola</b></th> -->
			       			<th><b>Tujuan</b></th>
			       			<th><b>Perihal</b></th>
			       			<th><b>Lampiran</b></th>
<!-- 			       			<th><b>Keterangan</b></th> -->
			       			<th><b>File</b></th>
						</tr>
					</thead>
					<tstatus>
				   		@foreach($surat_keluars as $i=>$surat_keluar)
			     			<tr>
								<td> {{ $surat_keluar->id}}</td>
			         			<td> {{ $surat_keluar->kode}}{{ $surat_keluar->no_surat}} </td>     
			         			<td> {{ $surat_keluar->tgl_surat}} </td>     
<!-- 			         			<td> {{ $surat_keluar->jenis_surat}} </td>     
			         			<td> {{ $surat_keluar->sifat_surat}} </td>    -->  
<!-- 			         			<td> {{ $surat_keluar->name}} </td>
			         			<td> {{ $surat_keluar->nama_penanggungjawab}} </td> -->
			         			<td> {{ $surat_keluar->tujuan}} </td>    
			         			<td> {{ $surat_keluar->perihal}} </td>    
			         			<td> {{ $surat_keluar->lampiran}} </td>    
<!-- 			         			<td> {{ $surat_keluar->keterangan}} </td>  -->
                                <td>{{ $surat_keluar->file }} <br>
                              </td>
			     			</tr>
				   		@endforeach
					</tstatus>
				</table>
			</div>
</div>

</body>
</html>
