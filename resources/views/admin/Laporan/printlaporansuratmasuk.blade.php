<!DOCTYPE html>
<html>
<style type="text/css">
table {
    font-family: arial, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    background-color: #dddddd;
}
</style>
<body>

<div class="w3-container">
  <h2>LAPORAN SURAT</h2>
  <p>The w3-striped class adds zebra-stripes to a table:</p>

	<div class="table-responsive">
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>No.</th>
			       			<!-- <th><b>No Agenda</b></th> -->
			       			<th><b>No Surat</b></th>
<!-- 			       			<th><b>Jenis Surat</b></th>
			       			<th><b>Sifat Surat</b></th> -->
			       			<th><b>Tanggal Pengirim</b></th>
			       			<th><b>Tanggal Penerima</b></th>
			       			<th><b>Sumber</b></th>
			       			<th><b>Perihal</b></th>
			       			<th><b>Lampiran</b></th>
			       			<!-- <th><b>Keterangan</b></th> -->
			       			<th><b>File</b></th>
						</tr>
					</thead>
					<tstatus>
				   		@foreach($surat_masuks as $i=>$surat_masuk)
			     			<tr>
                                <td>  {{ $surat_masuk->id}}</td>
				         			<!-- <td> {{ $surat_masuk->no_agenda}} </td>  -->    
				         			<td> {{ $surat_masuk->kode}}/{{ $surat_masuk->no_surat}} </td>     
				         			<!-- <td> {{ $surat_masuk->jenis_surat}} </td>     
				         			<td> {{ $surat_masuk->sifat_surat}} </td>  -->    
				         			<td> {{ $surat_masuk->tgl_pengirim}} </td>     
				         			<td> {{ $surat_masuk->tgl_penerima}} </td>     
				         			<td> {{ $surat_masuk->sumber}} </td>    
				         			<td> {{ $surat_masuk->perihal}} </td>    
				         			<td> {{ $surat_masuk->lampiran}} </td>    
				         			<!-- <td> {{ $surat_masuk->keterangan}} </td>  -->
	                                <td>{{ $surat_masuk->file }} <br>
                              </td>
			     			</tr>
				   		@endforeach
					</tstatus>
				</table>
			</div>
</div>

</body>
</html>
