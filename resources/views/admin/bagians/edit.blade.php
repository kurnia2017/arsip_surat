@extends('layouts.admin')

@section('content')
  <section class="content-header">
    <h1>
      Bagian
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active"><a href="{{URL::to('/bagian/index')}}">Bagian</a></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
      	{!! Form::model($bagians,['method'=>'put','action'=>['admin\BagianController@update',$bagians->id]]) !!}
	        <div class='form-group clearfix'>
	          {{ Form::label("nama_bagian", " Nama Bagian", ['class' => 'col-md-2 control-label']) }}
	            <div class='col-md-4'>
	              {{ Form::text("nama_bagian", null,['class' => 'form-control required']) }}
	              <span class="error">{{$errors->first('nama_bagian')}}</span>
	            </div>
	        </div> 
          <div class='form-group clearfix'>
            {{ Form::label("nama_kepala", "Nama Kepala", ['class' => 'col-md-2 control-label']) }}
              <div class='col-md-4'>
                {{ Form::text("nama_kepala", null,['class' => 'form-control required']) }}
                <span class="error">{{$errors->first('nama_kepala')}}</span>
              </div>
          </div> 
        <div class='form-group'>
        	<div class='col-md-4 col-md-offset-2'>
          	<button class='btn btn-primary' type='submit' name='save' id='save'><span class='glyphicon glyphicon-save'></span> Simpan</button>
        	</div>
      	</div>
      {!! Form::close() !!}
    	</div>
  	</div>
	</section>
@endsection