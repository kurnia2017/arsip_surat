@extends('layouts.admin')

@section('content')
  <section class="content-header">
    <h1>
      Bagian
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><a href="{{URL::to('/bagians/index')}}">Bagian</a></li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <table class="table table-striped table-hover">
          <tr>
            <td>ID</td>
            <td>
              {{{$bagians->id}}}
            </td>
          </tr>
          <tr>
            <td width="200px">Nama Bagian</td>
            <td>
              {{{$bagians->nama_bagian}}}
            </td>
          </tr>
           <tr>
            <td width="200px">Nama Kepala</td>
            <td>
              {{{$bagians->nama_kepala}}}
            </td>
          </tr>
        </table>
        <p align="center">
            <a href="{{URL::to('/bagian/index')}}" class="btn btn-primary" role="button">kembali</a>
          </p>
      </div>
    </div>
  </section>
@endsection