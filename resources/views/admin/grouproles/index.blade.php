@extends('layouts.admin')

@section('content')
	<section>
	<div class="col-md-4">
	<h1>
	  Group Roles
	</h1>
	</div>
	<div class='col-md-2 col-xs-12 pull-right m-b-10 m-t-25'>
		<a href="{{URL::to('/group-roles/create')}}" class="btn btn-primary wod-100"><i class="fa fa-plus-circle"></i> Tambah Data</a>
	</div>
	</section>
	<section class="content">
	  <div class="row">
	    <div class="col-md-12">
	    	<div class="col-md-4 pull-right row">
				<div class="clearfix">
				{!! Form::open(['method'=>'GET','url'=>'search-group-roles','role'=>'search'])  !!}
	              <div class="input-group custom-search-form">
	                <input type="text" class="form-control" name="search" placeholder="Cari...">
	                  <span class="input-group-btn">
	                  	<span class="input-group-btn">
		       				<button class="btn btn-default" type="submit"><i class="fa fa-search"></i> Cari</button>
	     				</span>
	                  </span>
	              </div>
     		 	{!! Form::close() !!}
				</div>
				</div>
				<br>
				<div class="table-responsive">
				<table class="table table-striped table-hover">
					<thead>
						<tr>
						 <th>No.</th>
			       <th><b>Group</b></th>
			       <th><b>Role</b></th>
			       <th class='text-center action'>Aksi</th>
						</tr>
					</thead>
					<tstatus>
				   @foreach($group_roles as $i=>$grouprole)
			     <tr>
		     		 <td class="nom-list">{{$start_page}}</td>
			         <td> {{ $grouprole->group->name }} </td>	         
			         <td> {{ $grouprole->role->name }} </td>
			         <td class="fl-center">
			         	<ul>
						<li><a href='{{URL::action("admin\GroupRoleController@edit",array($grouprole->id))}}'><button class="btn btn-info btn-style">Ubah</button></a></li>
						<li><a href='{{URL::action("admin\GroupRoleController@show",array($grouprole->id))}}'><button class="btn btn-warning btn-style">Lihat</button></a></li>
						<li>
						<form id="delete_grouprole{{$grouprole->id}}" action='{{URL::action("admin\GroupRoleController@destroy",array($grouprole->id))}}' method="POST">
						    <input type="hidden" name="_method" value="delete">
						    <input type="hidden" name="_token" value="{{ csrf_token() }}">
						    <a href="#" onclick="document.getElementById('delete_grouprole{{$grouprole->id}}').submit();"><button class="btn btn-danger btn-style">Hapus</button></a>
						</form>
						</li>
					<ul>
					  </td>
			     	</tr>
			     	<?php $start_page = $start_page+1 ?>
				   @endforeach
					</tstatus>
				</table>
				</div>
				<div class="text-center">
				{!! $group_roles->render() !!}
				</div>
			</div>
		</div>
	</section>
@endsection