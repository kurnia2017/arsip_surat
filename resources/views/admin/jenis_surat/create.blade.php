@extends('layouts.admin')

@section('content')
  <section class="content-header">
    <h1>
      Jenis Surat
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><a href="{{URL::to('/perihal')}}">Jenis Surat</a></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        {!! Form::open(['action' => 'admin\JenisSuratController@store']) !!}
          <div class='form-group clearfix'>
            {{ Form::label("jenis_surat", "Jenis Surat", ['class' => 'col-md-2 control-label']) }}
              <div class='col-md-4'>
                {{ Form::text("jenis_surat", '',['class' => 'form-control required']) }}
                <span class="error">{{$errors->first('jenis_surat')}}</span>
              </div>
          </div>
          <div class='form-group'>
            <div class='col-md-4 col-md-offset-2'>
              <button class='btn btn-primary' type='submit' name='save' id='save'><span class='glyphicon glyphicon-save'></span> Simpan</button>
            </div>
          </div>
        {!! Form::close() !!}    
      </div>
    </div>
  </section>
@endsection