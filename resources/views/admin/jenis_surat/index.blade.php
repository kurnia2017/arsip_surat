@extends('layouts.admin')

@section('content')
	<section>
	<div class="col-md-4">
    <h1>
      Jenis Surat
    </h1>
	</div>
	<div class='col-md-2 col-xs-12 pull-right m-b-10 m-t-25'>
		<a href="{{URL::to('/jenis_surat/create')}}" class="btn btn-primary wod-100"><i class="fa fa-plus-circle"></i>Tambah Data</a>
	</div>
  </section>
	<section class="content">
	  <div class="row">
	    <div class="col-md-12">
	    	<div class="col-md-4 pull-right row">
				<div class="clearfix">
				{!! Form::open(['method'=>'GET','url'=>'searchjenis_surat','role'=>'search'])  !!}
         			<div class="input-perihal custom-search-form">
            			<input type="text" class="form-control" name="search" placeholder="Cari...">
              			<span class="input-perihal-btn">
              				<span class="input-perihal-btn">
			       				<button class="btn btn-default" type="submit"><i class="fa fa-search"></i> Cari</button>
			     			</span>
              			</span>
          			</div>
          		{!! Form::close() !!}
				</div>
			</div>
			<br>
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>No.</th>
			       			<th><b>Jenis Surat</b></th>
			       			<th class='text-center action'>Aksi</th>
						</tr>
					</thead>
					<tstatus>
				   		@foreach($jenis_surats as $i=>$jenis_surat)
			     			<tr>
			     		 		<td  class="nom-list">{{$start_page}}</td>
			         			<td> {{ $jenis_surat->jenis_surat }} </td>	         
			         			<td class="fl-center">
			         				<ul>
			         					<li><a href='{{URL::action("admin\JenisSuratController@edit",array($jenis_surat->id))}}'><button class="btn btn-info btn-style">Ubah</button></a></li>
			         					<li><a href='{{URL::action("admin\JenisSuratController@show",array($jenis_surat->id))}}'><button class="btn btn-warning btn-style">Lihat</button></a></li>
			         					<li>
			         						<form id="delete_jenis_surat{{$jenis_surat->id}}" action='{{URL::action("admin\JenisSuratController@destroy",array($jenis_surat->id))}}' method="POST">
									    <input type="hidden" name="_method" value="delete">
									    <input type="hidden" name="_token" value="{{ csrf_token() }}">
									    <a href="#" onclick="document.getElementById('delete_jenis_surat{{$jenis_surat->id}}').submit();"><button class="btn btn-danger btn-style">Hapus</button></a>
									</form>
										</li>
			         				</ul>
							  	</td>
			     			</tr>
			     			<?php $start_page = $start_page+1 ?>
				   		@endforeach
					</tstatus>
				</table>
				<div class="text-center">
				{!! $jenis_surats->render() !!}
				</div>
			</div>
		</div>
	</section>
@endsection
@section('js')
<script>
	$( document ).ready(function() {
		var message = '{{session('flash-error')}}';
		if(message!=''){
			alert('{{session('flash-error')}}');
		}
	})
</script>
@endsection