@extends('layouts.admin')

@section('content')
  <section class="content-header">
    <h1>
      Jenis Surat
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><a href="{{URL::to('admin.jenis_surat.show')}}">Jenis Surat</a></li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <table class="table table-striped table-hover">
          <tr>
            <td>ID</td>
            <td>
              {{{$jenis_surats->id}}}
            </td>
          </tr>
          <tr>
            <td width="200px">Jenis Surat</td>
            <td>
              {{{$jenis_surats->jenis_surat}}}
            </td>
          </tr>
        </table>
        <p align="center">
            <a href="{{URL::to('/jenis_surat/index')}}" class="btn btn-primary" role="button">kembali</a>
          </p>
      </div>
    </div>
  </section>
@endsection