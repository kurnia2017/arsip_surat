@extends('layouts.admin')

@section('content')
  <section class="content-header">
    <h1>
      Kode Surat
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><a href="{{URL::to('/kode_surat')}}">Kode Surat</a></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        {!! Form::open(['action' => 'admin\KodeSuratController@store']) !!}
          <div class='form-group clearfix'>
            {{ Form::label("nama", "Nama", ['class' => 'col-md-2 control-label']) }}
              <div class='col-md-4'>
                {{ Form::text("nama", '',['class' => 'form-control required']) }}
                <span class="error">{{$errors->first('nama')}}</span>
              </div>
          </div>
          <div class='form-group clearfix'>
            {{ Form::label("no_kode_surat", "Kode Surat", ['class' => 'col-md-2 control-label']) }}
              <div class='col-md-4'>
                {{ Form::text("no_kode_surat", '',['class' => 'form-control required']) }}
                <span class="error">{{$errors->first('no_kode_surat')}}</span>
              </div>
          </div> 
          <div class='form-group'>
            <div class='col-md-4 col-md-offset-2'>
              <button class='btn btn-primary' type='submit' name='save' id='save'><span class='glyphicon glyphicon-save'></span> Simpan</button>
            </div>
          </div>
        {!! Form::close() !!}    
      </div>
    </div>
  </section>
@endsection