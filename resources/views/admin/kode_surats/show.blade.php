@extends('layouts.admin')

@section('content')
  <section class="content-header">
    <h1>
      Kode Surat
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><a href="{{URL::to('/kode_surats')}}">Kode Surat</a></li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <table class="table table-striped table-hover">
          <tr>
            <td>ID</td>
            <td>
              {{{$kode_surats->id}}}
            </td>
          </tr>
          <tr>
            <td width="200px">Kode Surat</td>
            <td>
              {{{$kode_surats->nama}}}
            </td>
          </tr>
           <tr>
            <td width="200px">No Kode Surat</td>
            <td>
              {{{$kode_surats->no_kode_surat}}}
            </td>
          </tr>
        </table>
        <p align="center">
            <a href="{{URL::to('/kode_surat/index')}}" class="btn btn-primary" role="button">kembali</a>
          </p>
      </div>
    </div>
  </section>
@endsection