@extends('layouts.admin')

@section('content')
  <section class="content-header">
    <h1>
     List Kategori
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><a href="{{URL::to('/list_kategori/index')}}">List Kategori</a></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        {!! Form::open(['action' => 'admin\ListKategoriController@store']) !!}
          <div class='form-group clearfix'>
            {{ Form::label("nama", "Nama", ['class' => 'col-md-2 control-label']) }}
              <div class='col-md-4'>
                {{ Form::text("nama", '',['class' => 'form-control required']) }}
                <span class="error">{{$errors->first('nama')}}</span>
              </div>
          </div>
          <div class='form-group clearfix'>
            {{ Form::label("kode", "Kode", ['class' => 'col-md-2 control-label']) }}
              <div class='col-md-4'>
                {{ Form::text("kode", '',['class' => 'form-control required']) }}
                <span class="error">{{$errors->first('kode')}}</span>
              </div>
          </div>
          <div class='form-group clearfix'>
              {{ Form::label("id", "Kategori", ['class' => 'col-md-2 control-label']) }}
              <div class='col-md-4'>
                {{ Form::select('id', $kategori, null,['class' => 'form-control required']) }}
              </div>
              @if ($errors->has('id'))
                <div class="help-block form-text with-errors form-control-feedback">
                  <ul class="list-unstyled text-danger">
                    <li>{{ $errors->first('id') }}</li>
                  </ul>
                </div>
              @endif
          </div>
          <div class='form-group'>
            <div class='col-md-4 col-md-offset-2'>
              <button class='btn btn-primary' type='submit' name='save' id='save'><span class='glyphicon glyphicon-save'></span> Simpan</button>
            </div>
          </div>
        {!! Form::close() !!}    
      </div>
    </div>
  </section>
@endsection