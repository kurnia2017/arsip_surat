@extends('layouts.admin')

@section('content')
  <section class="content-header">
    <h1>
      Rincian Kategori
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active"><a href="{{URL::to('/listkategori/index')}}">Data Rincian kategori</a></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
      	{!! Form::model($list_kategoris,['method'=>'put','action'=>['admin\ListKategoriController@update',$list_kategoris->id]]) !!}
          <div class='form-group clearfix'>
            {{ Form::label("nama", "Nama", ['class' => 'col-md-2 control-label']) }}
              <div class='col-md-4'>
                {{ Form::text("nama", null,['class' => 'form-control required']) }}
                <span class="error">{{$errors->first('nama')}}</span>
              </div>
          </div>
          <div class='form-group clearfix'>
            {{ Form::label("kode", "Kode", ['class' => 'col-md-2 control-label']) }}
              <div class='col-md-4'>
                {{ Form::text("kode", null,['class' => 'form-control required']) }}
                <span class="error">{{$errors->first('kode')}}</span>
              </div>
          </div>
           <div class='form-group clearfix'>
            {{ Form::label("id_kategori", "Id Kategori", ['class' => 'col-md-2 control-label']) }}
              <div class='col-md-4'>
                {{ Form::text("id_kategori", null,['class' => 'form-control required']) }}
                <span class="error">{{$errors->first('id_kategori')}}</span>
              </div>
          </div>  
        <div class='form-group'>
        	<div class='col-md-4 col-md-offset-2'>
          	<button class='btn btn-primary' type='submit' name='save' id='save'><span class='glyphicon glyphicon-save'></span> Simpan</button>
        	</div>
      	</div>
      {!! Form::close() !!}
    	</div>
  	</div>
	</section>
@endsection