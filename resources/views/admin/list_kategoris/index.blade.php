@extends('layouts.admin')

@section('content')
	<section>
	<div class="col-md-4">
    <h1>
      Rincian Data Kategori
    </h1>
	</div>
	@if(Auth::check())
    @if(Auth::user()->groups()->where("name", "=", "Admin Master")->first())
	<div class='col-md-2 col-xs-12 pull-right m-b-10 m-t-25'>
		<a href="{{URL::to('/list_kategori/create')}}" class="btn btn-primary wod-100"><i class="fa fa-plus-circle"></i> Tambah Data</a>
	</div>
	@else
	@endif
    @endif
  </section>
	<section class="content">
	  <div class="row">
	    <div class="col-md-12">
	    	<div class="col-md-4 pull-right row">
				<div class="clearfix">
				{!! Form::open(['method'=>'GET','url'=>'searchlist_kategori','role'=>'search'])  !!}
         			<div class="input-list_kategori custom-search-form">
            			<input type="text" class="form-control" name="search" placeholder="Cari...">
              			<span class="input-list_kategori-btn">
              				<span class="input-list_kategori-btn">
			       				<button class="btn btn-default" type="submit"><i class="fa fa-search"></i> Cari </button>
			     			</span>
              			</span>
          			</div>
          		{!! Form::close() !!}
				</div>
			</div>
			<br>
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>No.</th>
			       			<th><b>Kode</b></th>
			       			<th><b>Nama</b></th>
			       			<th><b>Nama Kategori</b></th>
			       			<th><b>kode Kategori</b></th>
			       			<th class='text-center action'>Aksi</th>
						</tr>
					</thead>
					<tstatus>
				   		@foreach($list_kategoris as $i=>$list_kategori)
			     			<tr>
			     		 		<td  class="nom-list">{{$start_page}}</td>
			         			<td> {{ $list_kategori->kode }} </td>	         
			         			<td> {{ $list_kategori->nama }} </td>	         
			         			<td> {{ $list_kategori->name }} </td>	         
			         			<td> {{ $list_kategori->kode_kategori }} </td>	         
			         			<td class="fl-center">
			         			<ul>
			         				<li><a href='
									{{URL::action("admin\ListKategoriController@edit",array($list_kategori->id))}}'><button class="btn btn-info btn-style">Ubah</button></a></li>
									<li><a href='
									{{URL::action("admin\ListKategoriController@show",array($list_kategori->id))}}'><button class="btn btn-warning btn-style">Lihat</button></a></li>
									<li>	
									<form id="delete_list_kategori{{$list_kategori->id}}" action='{{URL::action("admin\ListKategoriController@destroy",array($list_kategori->id))}}' method="POST">
									    <input type="hidden" name="_method" value="delete">
									    <input type="hidden" name="_token" value="{{ csrf_token() }}">
									    <a href="#" onclick="document.getElementById('delete_list_kategori{{$list_kategori->id}}').submit();"><button class="btn btn-danger btn-style">Hapus</button></a>
									</form>
									</li>
			         			</ul>
							  	</td>
			     			</tr>
			     			<?php $start_page = $start_page+1 ?>
				   		@endforeach
					</tstatus>
				</table>
				<div class="text-center">
				{!! $list_kategoris->render() !!}
				</div>
			</div>
      	<div class='col-md-12 text-center'>
     	 <p align="center">
             <a href="{{URL::to('/admin-dashboard')}}" class="btn btn-primary" role="button">kembali</a>
          </p>
        </div>
		</div>
	</section>
@endsection
@section('js')
<script>
	$( document ).ready(function() {
		var message = '{{session('flash-error')}}';
		if(message!=''){
			alert('{{session('flash-error')}}');
		}
	})
</script>
@endsection