@extends('layouts.admin')

@section('content')
  <section class="content-header">
    <h1>
      Disposisi
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
      {!! Form::model($surat_masuks,['method'=>'put','action'=>['admin\LurahController@proses',$surat_masuks->id]]) !!}
        <div class="modal-body">
          <div class="form-grup col-md-10">
          </div>
               <div class='form-grup'>
                {{ Form::label("id3", "Disposisikan Ke :", ['class' => 'col-md-4 control-label']) }}
                <div class='col-md-8'>
                  {{ Form::select('id3', $bagian, null,['class' => 'form-control required']) }}
                </div>
                @if ($errors->has('id3'))
                  <div class="help-block form-text with-errors form-control-feedback">
                    <ul class="list-unstyled text-danger">
                      <li>{{ $errors->first('id') }}</li>
                    </ul>
                  </div>
                @endif
            </div>
          <div class="form-grup">
            <label class="col-md-4 control-label">Catatan :</label>
            <div class="col-md-8">
              <textarea rows="5" name="catatan" class="form-control required"></textarea>
            </div>
          </div>

           <div class='form-grup'>
                {{ Form::label("id4", "Tanggapan Ke :", ['class' => 'col-md-4 control-label']) }}
                <div class='col-md-8'>
                  {{ Form::select('id4', $tanggapan, null,['class' => 'form-control required']) }}
                </div>
                @if ($errors->has('id4'))
                  <div class="help-block form-text with-errors form-control-feedback">
                    <ul class="list-unstyled text-danger">
                      <li>{{ $errors->first('id') }}</li>
                    </ul>
                  </div>
                @endif
            </div>
          <p></p>
        </div>
        <br>
        <div class="clearfix"></div>
        <input type="hidden" name="surat_id" value="{{$surat_masuks->id}}">
        <input type="hidden" name="status" value="1">
        <input type="hidden" name="disposisi" value="3">
        <div class="col-md-9 text-center">
          <button type="submit" class="btn btn-default" >kirim</button>
        </div>
      </div>
    </div>
  </section>
      {!! Form::close() !!}    
    </div>
  </section>
@endsection