@extends('layouts.admin')

@section('content')
  <section class="content-header">
    <h1>
      Show Surat Masuk
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><a href="{{URL::to('/lurah/index')}}">List Surat Masuk</a></li>
    </ol>
  </section>

  <section class="content">
      {!! Form::model($surat_masuks,['method'=>'put','action'=>['admin\LurahController@prosesreas',$surat_masuks->id]]) !!}
    <div class="row">
      <div class="col-md-12">
        <table class="table table-striped table-hover">
          <tr>
            <td>ID</td>
            <td>
              {{{$surat_masuks->id}}}
            </td>
          </tr>
           <!-- <tr>
            <td width="200px">No Agenda</td>
            <td>
              {{{$surat_masuks->no_agenda}}}
            </td>
          </tr> -->
          <tr>
            <td width="200px">No Surat</td>
            <td>
              {{{$surat_masuks->kode}}}/{{{$surat_masuks->no_surat}}}
            </td>
          </tr>
          <!--  <tr>
            <td width="200px">Jenis Surat</td>
            <td>
              {{{$surat_masuks->jenis_surat}}}
            </td>
          </tr>
           <tr>
            <td width="200px">Sifat Surat</td>
            <td>
              {{{$surat_masuks->sifat_surat}}}
            </td>
          </tr> -->
           <tr>
            <td width="200px">Tanggal Pengirim</td>
            <td>
              {{{$surat_masuks->tgl_pengirim}}}
            </td>
          </tr>
           <tr>
            <td width="200px">Tanggal Penerima</td>
            <td>
              {{{$surat_masuks->tgl_penerima}}}
            </td>
          </tr>
           <tr>
            <td width="200px">Sumber</td>
            <td>
              {{{$surat_masuks->sumber}}}
            </td>
          </tr>
          <tr>
            <td width="200px">Perihal</td>
            <td>
              {{{$surat_masuks->perihal}}}
            </td>
          </tr>
          <tr>
            <td width="200px">Lampiran</td>
            <td>
              {{{$surat_masuks->lampiran}}}
            </td>
          </tr>
         <!--  <tr>
            <td width="200px">Keterangan</td>
            <td>
              {{{$surat_masuks->keterangan}}}
            </td>
          </tr> -->
          <tr>
            <td width="200px">File</td>
            <td>
               <a href="{{URL::to('/file/asset/'.$surat_masuks->file)}}" class="btn btn-large"><i class="icon-download-alt"> </i> <img src="{{URL::to('file/asset').'/'.$surat_masuks->file}}" style="width:100px"> </a> 
                        <td> 

                          <a href="{{asset('pdftotext/'.$surat_masuk->file_upload)}}"  target="_blank">{{$surat_masuk->file_upload}}</a>
                        </td>  
            </td>
          </tr>
        </table>
      </div>
        <div class='form-group'>
          <div class='col-md-6 col-md-offset-3'>
          <input type="hidden" name="disposisi" value="2">
            <button class='btn btn-primary' type='submit' name='save' id='save'><span class='glyphicon glyphicon-save'></span> Kembali</button>
          <div class='col-md-6 col-md-offset-3'>
             <a href='{{URL::action("admin\LurahController@edit",array($surat_masuks->id))}}' align="right"class="btn btn-primary" role="button">Disposisikan</a>
           </div>
        </div>
        </div>
      {!! Form::close() !!}
    </div>
  </section>
@endsection