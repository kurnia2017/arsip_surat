@extends('layouts.admin')

@section('content')
	<section>
	<div class="col-md-4">
    <h1>
      Surat Keluar
    </h1>
	</div>
  </section>
	<section class="content">
	  <div class="row">
	    <div class="col-md-12">
	    	<div class="col-md-6">
				{!! Form::open(['method'=>'GET','url'=>'searchlurahsuratkeluaraaa','role'=>'search'])  !!}
<!--           		<div class="input-group margin">
					{{ Form::select('search', $kategori, null,['class' => 'form-control required']) }}
                    <span class="input-group-btn">
                      <button  type="submit" class="btn btn-default btn-flat">Cari Kategori</button>
                    </span>
              	</div> -->
          		{!! Form::close() !!}
	    	</div>
			<div class="col-md-5 pull-right row">
				<div class="clearfix">
				{!! Form::open(['method'=>'GET','url'=>'searchlurah','role'=>'search'])  !!}
	          		<div class="input-surat_keluar custom-search-form input-group margin">
            			<input type="text" class="form-control" name="search" placeholder="Cari...">
		                    <span class="input-group-btn">
	       						<button class="btn btn-default" type="submit"><i class="fa fa-search"></i> Cari</button>
		                    </span>
	          		</div>
          		{!! Form::close() !!}
				</div>
			</div>
			<br>
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>No.</th>
			       			<th><b>No Surat</b></th>
			       			<!-- <th><b>Jenis Surat</b></th>
			       			<th><b>Sifat Surat</b></th> -->
			       			<th><b>Tanggal Surat</b></th>
			       			<th><b>Pengolah</b></th>
			       			<th><b>Tujuan</b></th>
			       			<th><b>Perihal</b></th>
			       			<th><b>Lampiran</b></th>
			       			<!-- <th><b>Keterangan</b></th> -->
			       			<th><b>Kategori</b></th>
			       			<th><b>File</b></th>
						</tr>
					</thead>
					<tstatus>
				   		@foreach($surat_keluars as $i=>$surat_keluar)
			     			<tr>
			     		 		<td  class="nom-list">{{$start_page}}</td>
			         			<td> {{ $surat_keluar->no_surat}} </td>
			         			<!-- <td> {{ $surat_keluar->jenis_surat}} </td>    
			         			<td> {{ $surat_keluar->sifat_surat}} </td>  -->   
			         			<td> {{ $surat_keluar->tgl_surat}} </td>     
			         			<td> {{ $surat_keluar->name}} </td>     
			         			<td> {{ $surat_keluar->tujuan}} </td>    
			         			<td> {{ $surat_keluar->perihal}} </td>    
			         			<td> {{ $surat_keluar->lampiran}} </td>    
			         			<!-- <td> {{ $surat_keluar->keterangan}} </td> -->
			         			<td> {{ $surat_keluar->nama_kategori}} </td>
                                <td>{{ $surat_keluar->file }}<br>
                                  <a href="{{URL::to('/file/suratkeluar/'.$surat_keluar->file)}}" class="btn btn-large"><i class="icon-download-alt"> </i></a>
                              </td>
                              </td>          
			     			</tr>
			     			<?php $start_page = $start_page+1 ?>
				   		@endforeach
					</tstatus>
				</table>
				<div class="text-center">
				{!! $surat_keluars->render() !!}
				</div>
			</div>
		</div>
		<div class='col-md-12 text-center'>
     	 <p align="center">
             <a href="{{URL::to('/admin-dashboard')}}" class="btn btn-primary" role="button">kembali</a>
          </p>
        </div>
	</section>
@endsection
@section('js')
<script>
	$( document ).ready(function() {
		var message = '{{session('flash-error')}}';
		if(message!=''){
			alert('{{session('flash-error')}}');
		}
	})
</script>
@endsection