@extends('layouts.admin')

@section('content')
  <section class="content-header">
    <h1>
      Disposisi Surat
    </h1>
  </section>

  <section class="content">
    <div class="row">
        <table class="table table-striped table-hover">
          <tr>
          </tr>
           <tr>
            <td width="200px">Tanggal Terima Disposisi</td>
            <td>
            {{{$disposisi->created_at}}}
            </td>
          </tr>
          <tr>
            <td width="200px">Sumber</td>
            <td>
             {{{$disposisi->sumber}}}
            </td>
          </tr>
           <tr>
            <td width="200px">Tanggal Surat/No Surat</td>
            <td>
             {{{$disposisi->tgl_penerima}}}/
             {{{$disposisi->no_surat}}}
            </td>
          </tr>
           <tr>
            <td width="200px">Disposisi Ke</td>
            <td>
            {{{$disposisi->nama_bagian}}} 
            </td>
          </tr>
          <tr>
            <td width="200px">Catatan</td>
            <td>
            {{{$disposisi->catatan}}}
          </td>
          </tr>
        </table>
      <div class="col-md-12">
      <div>
        <p align="center">
            <a href="{{URL::to('/memo/read')}}" class="btn btn-primary" role="button">kembali</a>
            <a href="{{URL::to('/download-memo/'.$disposisi->id)}}" class="btn btn-primary" role="button">Unduh</a>
          </p>
          </div>
      </div>
    </div>
  </section>
@endsection