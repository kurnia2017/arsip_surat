@extends('layouts.admin')

@section('content')
  <section class="content-header">
    <h1>
      Disposisi Surat
    </h1>
  </section>

  <section class="content">
          <div>
            <a href="{{URL::to('/download-memo/'.$disposisi->id)}}"><button>Unduh</button></a>
          </div>
      {!! Form::model($disposisi,['method'=>'put','action'=>['admin\MemoController@prosesreas',$disposisi->id]]) !!}
      <div class="row">
        <div class="col-md-12">
          <table class="table table-striped table-hover m-t-25">
            <tr>
            </tr>
             <tr>
              <td width="200px">Tanggal Terima Disposisi</td>
              <td>
              {{{$disposisi->created_at}}}
              </td>
            </tr>
            <tr>
              <td width="200px">Sumber</td>
              <td>
               {{{$disposisi->sumber}}}
              </td>
            </tr>
             <tr>
              <td width="200px">No Agenda</td>
              <td>
               {{{$disposisi->no_agenda}}}
              </td>
            </tr>
             <tr>
              <td width="200px">Tanggal Surat/No Surat</td>
              <td>
               {{{$disposisi->tgl_penerima}}}/
               {{{$disposisi->kode}}}/
               {{{$disposisi->no_surat}}}
              </td>
            </tr>
             <tr>
              <td width="200px">Disposisi Ke</td>
              <td>
              {{{$disposisi->nama_bagian}}} 
              </td>
            </tr>
            <tr>
              <td width="200px">Catatan</td>
              <td>
              {{{$disposisi->catatan}}}
            </td>
            </tr>
          </table>
        </div>
          <div class='form-group'>
            <div class='col-md-12 text-center'>
            <input type="hidden" name="status" value="2">
              <button class='btn btn-primary' type='submit' name='save' id='save'><span class='glyphicon glyphicon-save'></span> Kembali</button>
            </div>
          </div>
      </div>
      {!! Form::close() !!}
  </section>
@endsection