@extends('layouts.admin')

@section('content')
	<section>
	<div class="col-md-12 col-md-offset-5">
    <h1>
    	Notifikasi
    </h1>
	</div>
  </section>
	<section class="content">
	  <div class="row">
				<div class="col-md-8 col-md-offset-2">
			        <div class="col-lg-6 col-xs-12">
			          <div class="small-box bg-aqua">
			            <div class="inner">
			              <h3>{{$jumlah_belum}}</h3>
			              <p>Belum Dibaca</p>
			            </div>
			            <div class="icon">
			              <i class="fa fa-envelope"></i>
			            </div>
			            <a href="{{URL::to('/memo/unread/')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			          </div>
			        </div>
					<div class="col-lg-6 col-xs-12">
			          <div class="small-box bg-orange">
			            <div class="inner">
			              	<h3>{{$jumlah_sudah}}</h3>
			              	<p>Sudah Dibaca</p>
			            </div>
			            <div class="icon">
			              <i class="fa fa-envelope"></i>
			            </div>
			             <a href="{{URL::to('/memo/read')}}" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
			          </div>
			        </div>
				</div>
				<div class="text-center">
				</div>
		</div>
      <br>
      <div class='col-md-12 text-center'>
      <p align="center">
             <a href="{{URL::to('/admin-dashboard')}}" class="btn btn-primary" role="button">kembali</a>
          </p>
        </div>
	</section>
@endsection
@section('js')
<script>
	$( document ).ready(function() {
		var message = '{{session('flash-error')}}';
		if(message!=''){
			alert('{{session('flash-error')}}');
		}
	})
</script>
@endsection