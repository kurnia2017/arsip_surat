<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<style type="text/css">
  .table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
    padding: 8px;
    line-height: 1.42857143;
    vertical-align: top;
    border: 1px solid #000000;
}
</style>
<body>

<div class="container">
  <h2 style="text-align: center;">KARTU PENERUS - DISPOSISI</h2>       
  <table class="table table-striped">
    <tbody>
      <tr>
        <td style="font-weight: 600;background: darkgray;">Nomor Surat</td>
        <td>{{$disposisi->sumber}}</td>
        <td style="font-weight: 600;background: darkgray;">Tanggal Penyelesaian</td>
        <td>{{$disposisi->tgl_penerima}}</td>
      </tr>
      <tr>
        <td>Perihal</td>
        <td colspan="4">{{$disposisi->catatan}}</td>
      </tr>
      <tr>
        <td>TGL/No.</td>
        <td colspan="4">{{$disposisi->catatan}}</td>
      </tr>
      <tr>
        <td>Asal Surat</td>
        <td colspan="4">{{$disposisi->catatan}}</td>
      </tr>
      <tr>
        <td style="font-weight: 600;background: darkgray;">INSTRUKSI/INFORMASI*)</td>
        <td>{{$disposisi->tgl_pengirim}}</td> 
        <td style="font-weight: 600;background: darkgray;">Diteruskan Kepada :</td>
        <td>{{$disposisi->no_surat}}</td>
      </tr>
      <tr>
        <td style="font-weight: 600;background: darkgray;">Diberikan Kepada</td>
        <td>{{$disposisi->nama_bagian}}</td>
      </tr>
      <tr>
        <td>Catatan</td>
        <td colspan="4">{{$disposisi->catatan}}</td>
      </tr>
      <tr>
        <td style="font-weight: 600;background: darkgray;">Asal Surar</td>
        <td>{{$disposisi->sumber}}</td>
        <td style="font-weight: 600;background: darkgray;">Diterima Tanggal</td>
        <td>{{$disposisi->tgl_penerima}}</td>
      </tr>
      <tr>
        <td style="font-weight: 600;background: darkgray;">Tanggal Surat</td>
        <td>{{$disposisi->tgl_pengirim}}</td> 
        <td style="font-weight: 600;background: darkgray;">Nomor Surat</td>
        <td>{{$disposisi->no_surat}}</td>
      </tr>
      <tr>
        <td style="font-weight: 600;background: darkgray;">Diberikan Kepada</td>
        <td>{{$disposisi->nama_bagian}}</td>
      </tr>
      <tr>
        <td>Catatan</td>
        <td colspan="4">{{$disposisi->catatan}}</td>
      </tr>
    </tbody>
  </table>
</div>

</body>
</html>