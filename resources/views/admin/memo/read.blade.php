@extends('layouts.admin')

@section('content')
	<section>
	<div class="col-md-4">
    <h1>
      Disposisi Surat
    </h1>
	</div>
  </section>
	<section class="content">
	  <div class="row">
	    <div class="col-md-12">
	    	<div class="col-md-4 pull-right row">
				<div class="clearfix">
				{!! Form::open(['method'=>'GET','url'=>'searchmemo','role'=>'search'])  !!}
         			<div class="input-bagian custom-search-form">
            			<input type="text" class="form-control" name="search" placeholder="Search...">
              			<span class="input-bagian-btn">
              				<span class="input-bagian-btn">
			       				<button class="btn btn-default" type="submit"><i class="fa fa-search"></i> Search</button>
			     			</span>
              			</span>
          			</div>
          		{!! Form::close() !!}
				</div>
			</div>
			<br>
				<table class="table table-striped table-hover">
					<tstatus>
				   		@foreach($disposisi as $i=>$disposi)
			     			<tr>
			     		 		<td  class="nom-list">{{$start_page}}</td>
			         			<td>{{ $disposi->name }}: </td>	         
			         			<td><a href="{{URL::to('/memo/detailread/'.$disposi->id)}}">Disposisi surat:{{ $disposi->sumber }} : {{ $disposi->catatan }}</a></td>	         
			         			<td>{{ $disposi->created_at	 }} </td>	         
			     			</tr>
			     			<?php $start_page = $start_page+1 ?>
				   		@endforeach
					</tstatus>
				</table>
				<div class="text-center">
				{!! $disposisi->render() !!}
				</div>
			</div>
		</div>
		<br>
      	<div class='col-md-12 text-center'>
     	 <p align="center">
             <a href="{{URL::to('/memo/index')}}" class="btn btn-primary" role="button">kembali</a>
          </p>
        </div>
	</section>
@endsection
@section('js')
<script>
	$( document ).ready(function() {
		var message = '{{session('flash-error')}}';
		if(message!=''){
			alert('{{session('flash-error')}}');
		}
	})
</script>
@endsection