@extends('layouts.admin')

@section('content')
  <section class="content-header">
    <h1>
      Penanggung Jawab Pengelola
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><a href="{{URL::to('/penanggung_jawabs/index')}}">List Penanggung Jawab Pengelola</a></li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <table class="table table-striped table-hover">
          <tr>
            <td>ID</td>
            <td>
              {{{$penanggung_jawabs->id}}}
            </td>
          </tr>
          <tr>
            <td width="200px">Nama</td>
            <td>
              {{{$penanggung_jawabs->nama}}}
            </td>
          </tr>
        </table>
        <p align="center">
            <a href="{{URL::to('/penanggung_jawab/index')}}" class="btn btn-primary" role="button">kembali</a>
          </p>
      </div>
    </div>
  </section>
@endsection