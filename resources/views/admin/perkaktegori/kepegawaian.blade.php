@extends('layouts.admin')

@section('content')
	<section>
	<div class="col-md-12">
    <h1>
      Klasifikasi Kepegawaian
    </h1>
     <p align="right">
         <a href="{{URL::to('/document')}}" class="btn btn-primary" role="button">kembali</a>
     </p>
	</div>
  </section>
	<div class="col-md-6">
		<div class="box box-info">
	        <div class="box-header with-border">
	          <h3 class="box-title">Surat Masuk</h3>
	        <div class="col-md-6 pull-right">
				{!! Form::open(['method'=>'GET','url'=>'search_kepegawaian','role'=>'search'])  !!}
	          		<div class="input-surat_masuk custom-search-form input-group margin">
            			<input type="text" class="form-control" name="search" placeholder="Cari...">
		                    <span class="input-group-btn">
	       						<button class="btn btn-default" type="submit"><i class="fa fa-search"></i> Cari</button>
		                    </span>
	          		</div>
          		{!! Form::close() !!}
			</div>
	        </div>
	        <!-- /.box-header -->
	        <div class="box-body">
	          <div class="table-responsive">
	          	<table class="table table-striped table-hover">
					<thead>
						<tr>
			       			<th><b>No Surat</b></th>
			       			<th><b>Perihal</b></th>
			       			<th><b>file</b></th>
						</tr>
					</thead>
					<tstatus>
				   		@foreach($kepegawaian as $i=>$kepegawaianss)
			     			<tr>
		         			 <td>{{ $kepegawaianss->no_surat}}</td>              
		         			 <td>{{ $kepegawaianss->perihal}}</td>              
		         			 <td>{{ $kepegawaianss->file}}</td>               
			     			</tr>
				   		@endforeach
					</tstatus>
				</table>
	          </div>
	          <!-- /.table-responsive -->
	        </div>
	        <!-- /.box-body -->
	        <div class="box-footer clearfix">
	        </div>
	        <!-- /.box-footer -->
	  	</div>
  	</div>
  	<div class="col-md-6">
		<div class="box box-info">
	        <div class="box-header with-border">
	          <h3 class="box-title">Surat Keluar</h3>
	          <div class="col-md-6 pull-right">
				{!! Form::open(['method'=>'GET','url'=>'search_kepegawaian','role'=>'search'])  !!}
	          		<div class="input-surat_masuk custom-search-form input-group margin">
            			<input type="text" class="form-control" name="search" placeholder="Cari...">
		                    <span class="input-group-btn">
	       						<button class="btn btn-default" type="submit"><i class="fa fa-search"></i> Cari</button>
		                    </span>
	          		</div>
          		{!! Form::close() !!}
			</div>
	        <!-- /.box-header -->
	        <div class="box-body">
	          <div class="table-responsive">
	          	<table class="table table-striped table-hover">
					<thead>
						<tr>
			       			<th><b>No Surat</b></th>
			       			<th><b>file</b></th>
						</tr>
					</thead>
				   		@foreach($kepegawaians as $i=>$kepegawaia)
			     			<tr>
		         			 <td>{{ $kepegawaia->no_surat}}</td>               
		         			 <td>{{ $kepegawaia->file}}</td>               
			     			</tr>
				   		@endforeach
					<tstatus>
					</tstatus>
				</table>
	          </div>
	          <!-- /.table-responsive -->
	        </div>
	        <!-- /.box-body -->
	        <div class="box-footer clearfix">
	        </div>
	        <!-- /.box-footer -->
	  	</div>
  	</div>
@endsection
@section('js')
<script>
	$( document ).ready(function() {
		var message = '{{session('flash-error')}}';
		if(message!=''){
			alert('{{session('flash-error')}}');
		}
	})
</script>
@endsection