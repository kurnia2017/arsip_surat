@extends('layouts.admin')

@section('content')
	<section>
	<div class="col-md-4">
	<h1>
	  Roles
	</h1>
	</div>
	<div class='col-md-2 col-xs-12 pull-right m-b-10 m-t-25'>
		<a href="{{URL::to('/roles/create')}}" class="btn btn-primary wod-100"><i class="fa fa-plus-circle"></i> Tambah Data</a>
	</div>
	</section>
	<section class="content">
	  <div class="row">
	    <div class="col-md-12">
	    	<div class="col-md-4 pull-right row">
				<div class="clearfix">
				{!! Form::open(['method'=>'GET','url'=>'search-roles','role'=>'search'])  !!}
        			<div class="input-group custom-search-form">
          				<input type="text" class="form-control" name="search" placeholder="Cari...">
	                    <span class="input-group-btn">
	                    	<span class="input-group-btn">
						    	<button class="btn btn-default" type="submit"><i class="fa fa-search"></i> Cari</button>
						    </span>
	                    </span>
        			</div>
      			{!! Form::close() !!}
			</div>
			</div>
			<br>
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						<th>No.</th>
		       			<th><b>Role</b></th>
		       			<th class='text-center action'>Aksi</th>
					</tr>
				</thead>
				<tstatus>
			   		@foreach($roles as $i=>$role)
		     			<tr>
		     		 		<td class="nom-list">{{$start_page}}</td>
		         			<td> {{ $role->name }} </td>	         
		         			<td class="fl-center">
								<a href='{{URL::action("admin\RoleController@edit",array($role->id))}}'>Ubah</a>
								<a href='{{URL::action("admin\RoleController@show",array($role->id))}}'>Lihat</a>
								<form id="delete_role{{$role->id}}" action='{{URL::action("admin\RoleController@destroy",array($role->id))}}' method="POST">
								    <input type="hidden" name="_method" value="delete">
								    <input type="hidden" name="_token" value="{{ csrf_token() }}">
								    <a href="#" onclick="document.getElementById('delete_role{{$role->id}}').submit();">Hapus</a>
								</form>
						  	</td>
		     			</tr>
		     			<?php $start_page = $start_page+1 ?>
			   		@endforeach
				</tstatus>
			</table>
			<div class="text-center">
			{!! $roles->render() !!}
			</div>
			</div>
		</div>
	</section>
@endsection
@section('js')
<script>
	$( document ).ready(function() {
		var message = '{{session('flash-error')}}';
		if(message!=''){
			alert('{{session('flash-error')}}');
		}
	})
</script>
@endsection