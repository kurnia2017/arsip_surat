
@extends('layouts.admin')

@section('content')
	<section>
	<div class="col-md-4">
    <h1>
      Data Surat Penting
    </h1>
	</div>
<!-- 	<div class='col-md-2 col-xs-12 pull-right m-b-10 m-t-25'>
		<a href="{{URL::to('/suratmasuk/create')}}" class="btn btn-primary wod-100"><i class="fa fa-plus-circle"></i> create</a>
	</div> -->
  </section>
	<section class="content">
	  <div class="row">
	    <div class="col-md-12">
	    	<div class="col-md-4">
				{!! Form::open(['method'=>'GET','url'=>'searchlurahkategori','role'=>'search'])  !!}
					{{ Form::select('search', $kategori, null,['class' => 'form-control required']) }}
   					<button class="btn btn-default" type="submit"><i class="fa fa-search"></i>Cari Kategori</button>
          		{!! Form::close() !!}
	    	</div>
	    	<div class="col-md-4">
				<div class="clearfix">
				{!! Form::open(['method'=>'GET','url'=>'searchlurah','role'=>'search'])  !!}
         			<div class="input-surat_masuk custom-search-form">
            			<input type="text" class="form-control" name="search" placeholder="Cari...">
              			<span class="input-surat_masuk-btn">
              				<span class="input-surat_masuk-btn">
			       				<button class="btn btn-default" type="submit"><i class="fa fa-search"></i> Cari</button>
			     			</span>
              			</span>
          			</div>
          		{!! Form::close() !!}
				</div>
			</div>
			<br>
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>No.</th>
			       			<th><b>No Agenda</b></th>
			       			<th><b>No Surat</b></th>
			       			<th><b>Jenis Surat</b></th>
			       			<th><b>Sifat Surat</b></th>
			       			<th><b>Tanggal Pengirim</b></th>
			       			<th><b>Tanggal Penerima</b></th>
			       			<th><b>Sumber</b></th>
			       			<th><b>Perihal</b></th>
			       			<th><b>Lampiran</b></th>
			       			<th><b>Keterangan</b></th>
			       			<th><b>Kategori</b></th>
			       			<th><b>File</b></th>
			       			<th><b>Status</b></th>
						</tr>
					</thead>
					<tstatus>
				   		@foreach($surat_masuks as $i=>$surat_masuk)
			     			<tr>
			     		 		<td  class="nom-list">{{$start_page}}</td>
			         			<td> {{ $surat_masuk->no_agenda}} </td>    
				         		<td> {{ $surat_masuk->kode}}/{{ $surat_masuk->no_surat}} </td>  
			         			<td> {{ $surat_masuk->jenis_surat}} </td>     
			         			<td> {{ $surat_masuk->sifat_surat}} </td>     
			         			<td> {{ $surat_masuk->tgl_pengirim}} </td>     
			         			<td> {{ $surat_masuk->tgl_penerima}} </td>     
			         			<td> {{ $surat_masuk->sumber}} </td>    
			         			<td> {{ $surat_masuk->perihal}} </td>    
			         			<td> {{ $surat_masuk->lampiran}} </td>    
			         			<td> {{ $surat_masuk->keterangan}} </td>
			         			<td> {{ $surat_masuk->nama_kategori}} </td>
                                <td>{{ $surat_masuk->file }} <br>
                                	@if ($surat_masuk->status===1)
                                	@elseif ($surat_masuk->status=== 3)
                                	 <i class="fa fa-share-square"></i><span><b>Sudah Di disposisi<b></span>
                                	@else
                                   <a href='{{URL::action("admin\LurahController@edit",array($surat_masuk->id))}}'>Disposisikan</a>
                                   @endif
                                  <a href='{{URL::action("admin\LurahController@show",array($surat_masuk->id))}}'>Lihat</a>
                              </td>
                              <td>
                                	@if ($surat_masuk->status=== 1)
   										Unread!
									@elseif ($surat_masuk->status=== 2)
									    Read!
									@else
    									Disposisi!
									@endif
                              </td>                      
			     			</tr>
			     			<?php $start_page = $start_page+1 ?>

    <!-- Modal content-->
   
				   		@endforeach
					</tstatus>
				</table>
				<div class="text-center">
				{!! $surat_masuks->render() !!}
				</div>
			</div>
		</div>
		<br>
      	<div class='col-md-12 text-center'>
     	 <p align="center">
             <a href="{{URL::to('/admin-dashboard')}}" class="btn btn-primary" role="button">kembali</a>
          </p>
        </div>
	</section>
	</section>
@endsection
@section('js')
<script>
	$( document ).ready(function() {
		var message = '{{session('flash-error')}}';
		if(message!=''){
			alert('{{session('flash-error')}}');
		}
	})
</script>
@endsection