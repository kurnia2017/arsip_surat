@extends('layouts.admin')

@section('content')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
   <link rel="stylesheet" href="/">
  <section class="content-header">
    <h1>
     Entri Surat Keluar
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><a href="{{URL::to('/surat_masuk')}}">Data Surat Keluar</a></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
         {!! Form::open(['action' => 'admin\SuratKeluarController@store','class' => 'form-horizontal','files'=>true, 'method' => 'post']) !!}
          <!-- <div class='form-group clearfix'>
            {{ Form::label("no_surat", "No Surat", ['class' => 'col-md-2 control-label']) }}
              <div class='col-md-4'>
                {{ Form::text("no_surat", '',['class' => 'form-control required']) }}
                <span class="error">{{$errors->first('no_surat')}}</span>
              </div>
            </div> -->
         
 <!--          </div>
 -->          <div class='form-group clearfix'>
            {{ Form::label("tgl_surat", "Tanggal Surat", ['class' => 'col-md-2 control-label']) }}
               <div class='col-md-4'>
                 <input type="text" id="tgl_surat" name="tgl_surat" class="form-control required datepicker1" placeholder="Masukan Tanggal" )}}/>
                  <span class="error">{{$errors->first('tgl_surat')}}</span>
                </div>
          </div>
<!--           <div class='form-group clearfix'>
              {{ Form::label("id_jenis_surat", "Jenis Surat", ['class' => 'col-md-2 control-label']) }}
              <div class='col-md-4'>
                {{ Form::select('id_jenis_surat', $jenis_surat, null,['class' => 'form-control required']) }}
              </div>
              @if ($errors->has('id_jenis_surat'))
                <div class="help-block form-text with-errors form-control-feedback">
                  <ul class="list-unstyled text-danger">
                    <li>{{ $errors->first('id_jenis_surat') }}</li>
                  </ul>
                </div>
              @endif
          </div> -->
<!--            <div class='form-group clearfix'>
              {{ Form::label("id_2", "Sifat Surat", ['class' => 'col-md-2 control-label']) }}
              <div class='col-md-4'>
                {{ Form::select('id_2', $sifat_surats, null,['class' => 'form-control required']) }}
              </div>
              @if ($errors->has('id_sifat_surat'))
                <div class="help-block form-text with-errors form-control-feedback">
                  <ul class="list-unstyled text-danger">
                    <li>{{ $errors->first('id_2') }}</li>
                  </ul>
                </div>
              @endif
          </div> -->
          <div class='form-group clearfix'>
              {{ Form::label("id", "Pengolah", ['class' => 'col-md-2 control-label']) }}
              <div class='col-md-4'>
                {{ Form::select('id', $user, null,['class' => 'form-control required']) }}
              </div>
              @if ($errors->has('id'))
                <div class="help-block form-text with-errors form-control-feedback">
                  <ul class="list-unstyled text-danger">
                    <li>{{ $errors->first('id') }}</li>
                  </ul>
                </div>
              @endif
          </div>
              <div class='form-group clearfix'>
              {{ Form::label("id3", "Penanggung Jawab Pengelola", ['class' => 'col-md-2 control-label']) }}
              <div class='col-md-4'>
                {{ Form::select('id3', $penanggung_jawab, null,['class' => 'form-control required']) }}
              </div>
              @if ($errors->has('id3'))
                <div class="help-block form-text with-errors form-control-feedback">
                  <ul class="list-unstyled text-danger">
                    <li>{{ $errors->first('id3') }}</li>
                  </ul>
                </div>
              @endif
          </div>
<!--           <div class='form-group clearfix'>
            {{ Form::label("tujuan", "Tujuan", ['class' => 'col-md-2 control-label']) }}
               <div class='col-md-4'>
                 {{ Form::text("tujuan", '',['class' => 'form-control required']) }}
                  <span class="error">{{$errors->first('tujuan')}}</span>
                </div>
          </div>
           <div class='form-group clearfix'>
            {{ Form::label("perihal", "Perihal", ['class' => 'col-md-2 control-label']) }}
               <div class='col-md-4'>
                 {{ Form::text("perihal", '',['class' => 'form-control required']) }}
                  <span class="error">{{$errors->first('perihal')}}</span>
                </div>
          </div> -->
          <div class='form-group clearfix'>
            {{ Form::label("lampiran", "Lampiran", ['class' => 'col-md-2 control-label']) }}
               <div class='col-md-4'>
                 {{ Form::text("lampiran", '',['class' => 'form-control required']) }}
                  <span class="error">{{$errors->first('lampiran')}}</span>
                </div>
          </div>
<!--           <div class='form-group clearfix'>
            {{ Form::label("keterangan", "Keterangan", ['class' => 'col-md-2 control-label']) }}
              <div class='col-md-4'>
                {{ Form::text("keterangan", '',['class' => 'form-control required']) }}
                <span class="error">{{$errors->first('keterangan')}}</span>
              </div>
          </div>  -->
           <div class="form-group clearfix">
              <div class="col-md-2 control-label">
                <b>Kategori*</b>
              </div>
              <div class="col-md-4">
                 {{ Form::select('kategory', $kategori, null,['class' => 'form-control kategory', 'placeholder' => 'Pilih Kategori' ]) }}
              </div>
          </div>
          <div class="form-group clearfix">
              <div class="col-md-2 control-label">
                <b>Nama Kategori* :</b>
              </div>
              <div class="col-md-4 item-inpt">
                {{ Form::select('nama_list',[], null,['class' => 'form-control list-kategori', 'placeholder' =>$surat_keluars->nama_list, 'value'=>$surat_keluars->nama_list]) }}
                
                 <span class="error" style="color: red;">{{$errors->first('nama_list')}}</span>
              </div>
          </div>
          
          <div class="form-group row">
            {{ Form::label("file", "File Surat", ['class' => 'col-md-2 control-label']) }}
              <div class="col-md-4">
              <input type= 'file' class="form-control" name="file" id="exampleInputFile" rows="3">
              </div>
           </div>
          </div>
          <div class='form-group'>
            <div class='col-md-4 col-md-offset-2'>
              <button class='btn btn-primary' type='submit' name='save' id='save'><span class='glyphicon glyphicon-save'></span> Save</button>
            </div>
          </div>
        {!! Form::close() !!}    
      </div>
    </div>
  </section>

@endsection
@section('js')
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
  <script>
  $(function() {
    $(".datepicker").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: '-40:+0',
        dateFormat: "dd-mm-yy",
        // maxDate: '-1d'
    });
    $(".datepicker1").datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: '-40:+0',
        dateFormat: "dd-mm-yy",
        // maxDate: '-1d'
    });
  });
  </script>
  <script type="text/javascript">
    $('.kategory').on('change',function(e){
      var id_kategori = e.target.value;
      console.log(id_kategori);
      $.get('/ajax-kategoris?id_kategori='+id_kategori, function(data){
      console.log(data);
        $('.list-kategori').empty();
        $.each(data,function(index,subcatObj){
          $('.list-kategori').append('<option value="'+subcatObj.kode+'">'+subcatObj.nama+'</option>');
        });
      });
    });
</script>
@endsection
