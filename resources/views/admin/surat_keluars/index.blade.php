@extends('layouts.admin')

@section('content')
	<section>
	<div class="col-md-4">
    <h1>
      Data Surat Keluar
    </h1>
	</div>
	@if(Auth::check())
	<div class='col-md-2 col-xs-12 pull-right m-b-10 m-t-25'>
		<a href="{{URL::to('/SuratKeluar/create')}}" class="btn btn-primary wod-100"><i class="fa fa-plus-circle"></i> Tambah</a>
	</div>
    @endif
  </section>
	<section class="content">
	  <div class="row">
	    <div class="col-md-12">
<div class="col-md-4">
				{!! Form::open(['method'=>'GET','url'=>'searchSuratKeluartgl','role'=>'search'])  !!}
          		<!-- <div class="input-group margin">
					<input class="form-control datepicker" name="search" placeholder="Cari Berdasarkan Tanggal">
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i> Pencarian kronologi </button>
                    </span>
              	</div> -->
          		{!! Form::close() !!}
	    	</div>
	    	<div class="col-md-4 pull-right row">
				<div class="clearfix">
				{!! Form::open(['method'=>'GET','url'=>'searchSuratKeluars','role'=>'search'])  !!}
	          		<div class="input-surat_keluar custom-search-form input-group margin">
            			<input type="text" class="form-control" name="search" placeholder="Cari...">
		                    <span class="input-group-btn">
	       						<button class="btn btn-default" type="submit"><i class="fa fa-search"></i> Cari</button>
		                    </span>
	          		</div>
          		{!! Form::close() !!}
				</div>
			</div>
			<br>
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>No.</th>
			       			<th><b>No Surat</b></th>
			       			<th><b>Perihal</b></th>
			       			<th><b>Tanggal Surat</b></th>
			       			<!-- <th><b>Jenis Surat</b></th> -->
			       			<!-- <th><b>Sifat Surat</b></th> -->
			       			<th><b>Tujuan</b></th>
			       			<th><b>Kategori</b></th>
			       			<th><b>File</b></th>
			       			<th class='text-center action'>Aksi</th>
						</tr>
					</thead>
					<tstatus>
				   		@foreach($surat_keluars as $i=>$surat_keluar)
			     			<tr>
			     		 		<td  class="nom-list">{{$start_page}}</td>
			         			<td> {{ $surat_keluar->kode}}{{ $surat_keluar->no_surat}} </td>     
			         			<td> {{ $surat_keluar->perihal}} </td>    
			         			<td> {{ $surat_keluar->tgl_surat}} </td>     
			         			<!-- <td> {{ $surat_keluar->jenis_surat}} </td> -->     
			         			<!-- <td> {{ $surat_keluar->sifat_surat}} </td>  -->    
			         			<td> {{ $surat_keluar->tujuan}} </td>
					         	<td> {{ $surat_keluar->nama_kategori}} </td> 
					         	<td> 
					         				<a href="{{asset('pdftotext/'.$surat_keluar->file_upload)}}"  target="_blank">{{$surat_keluar->file_upload}}</a>
					         			</td> 
			         			<td class="fl-center">
			         			<ul>
									<li><a href='{{URL::action("admin\SuratKeluarController@edit",array($surat_keluar->id))}}'><button class="btn btn-info btn-style">Ubah</button></a></li>
									<li><a href='{{URL::action("admin\SuratKeluarController@show",array($surat_keluar->id))}}'><button class="btn btn-warning btn-style">Lihat</button></a></li>
									<li>
									<form id="delete_surat_keluar{{$surat_keluar->id}}" action='{{URL::action("admin\SuratKeluarController@destroy",array($surat_keluar->id))}}' method="POST">
									    <input type="hidden" name="_method" value="delete">
									    <input type="hidden" name="_token" value="{{ csrf_token() }}">
									    <a href="#" onclick="document.getElementById('delete_surat_keluar{{$surat_keluar->id}}').submit();"><button class="btn btn-danger btn-style">Hapus</button></a>
									</form>
									</li>
			         			</ul>
							  	</td>
			     			</tr>
			     			<?php $start_page = $start_page+1 ?>
				   		@endforeach
					</tstatus>
				</table>
				<div class="text-center">
				{!! $surat_keluars->render() !!}
				</div>
			</div>
		</div>
		<br>
	</section>
@endsection
@section('js')
<script>
	$( document ).ready(function() {
		var message = '{{session('flash-error')}}';
		if(message!=''){
			alert('{{session('flash-error')}}');
		}
	})
</script>
<script type="text/javascript">
	$('.datepicker').datepicker({
	    format: 'mm-dd-yyyy'
	});
</script>
@endsection
