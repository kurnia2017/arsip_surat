@extends('layouts.admin')

@section('content')
  <section class="content-header">
    <h1>
      Show Surat Keluar
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><a href="{{URL::to('/SuratKeluar/index')}}">Data Surat Keluar</a></li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <table class="table table-striped table-hover">
          <tr>
            <td>ID</td>
            <td>
              {{{$surat_keluars->id}}}
            </td>
          </tr>
           <tr>
            <td width="200px">No Surat</td>
            <td>
              {{{$surat_keluars->kode}}}{{{$surat_keluars->no_surat}}}
            </td>
          </tr>
           <tr>
            <td width="200px">Tanggal Surat</td>
            <td>
              {{{$surat_keluars->tgl_surat}}}
            </td>
          </tr>
<!--           <tr>
            <td width="200px">Jenis Surat</td>
            <td>
              {{{$surat_keluars->jenis_surat}}}
            </td>
          </tr>
 -->          <!-- <tr>
            <td width="200px">Sifat Surat</td>
            <td>
              {{{$surat_keluars->sifat_surat}}}
            </td>
          </tr> -->
<!--            <tr> -->
<!--             <td width="200px">Pengolah</td>
            <td>
              {{{$surat_keluars->name}}}
            </td>
          </tr>
          <tr>
            <td width="200px">Penanggung Jawab Pengelola</td>
            <td>
              {{{$surat_keluars->nama_penanggungjawab}}}
            </td>
          </tr> -->
           <tr>
            <td width="200px">Tujuan</td>
            <td>
              {{{$surat_keluars->tujuan}}}
            </td>
          </tr>
          <tr>
            <td width="200px">Perihal</td>
            <td>
              {{{$surat_keluars->perihal}}}
            </td>
          </tr>
           <tr>
            <td width="200px">Lampiran</td>
            <td>
              {{{$surat_keluars->lampiran}}}
            </td>
          </tr>
<!--           <tr>
            <td width="200px">Keterangan</td>
            <td>
              {{{$surat_keluars->keterangan}}}
            </td>
          </tr> -->
          <tr>
            <td width="200px">File Surat</td>
            <td>
              {{{$surat_keluars->file}}}
              <a href="{{URL::to('/file/asset/'.$surat_masuks->file)}}" class="btn btn-large"><i class="icon-download-alt"> </i> view Atau Download </a>
            </td>
          </tr>
        </table>
        <p align="center">
            <a href="{{URL::to('/SuratKeluar/index')}}" class="btn btn-primary" role="button">kembali</a>
          </p>
      </div>
    </div>
  </section>
@endsection