@extends('layouts.admin')

@section('content')
	<section>
	<div class="col-md-4">
    <h1>
      Data Surat Masuk
    </h1>
	</div>
	 @if(Auth::check())
	<div class='col-md-2 col-xs-10 pull-right m-b-10 m-t-25'>
		<a href="{{URL::to('/suratmasuk/create')}}" class="btn btn-primary wod-100"><i class="fa fa-plus-circle"></i>Tambah</a>
	</div>
    @endif
  </section>
	<section class="content">
	  <div class="row">
	    <div class="col-md-12">
<!-- 	    	<div class="col-md-4">
				{!! Form::open(['method'=>'GET','url'=>'searchsuratmasuktgl','role'=>'search'])  !!}
          		<div class="input-group margin">
					<input class="form-control datepicker" name="search" placeholder="Cari Berdasarkan Tanggal">
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i> Pencarian kronologi </button>
                    </span>
              	</div>
          		{!! Form::close() !!}
	    	</div> -->
	    	<div class="col-md-4 pull-right row">
				<div class="clearfix">
				{!! Form::open(['method'=>'GET','url'=>'searchsuratmasuk','role'=>'search'])  !!}
	          		<div class="input-surat_masuk custom-search-form input-group margin">
            			<input type="text" class="form-control" name="search" placeholder="Cari...">
		                    <span class="input-group-btn">
	       						<button class="btn btn-default" type="submit"><i class="fa fa-search"></i> Cari</button>
		                    </span>
	          		</div>
          		{!! Form::close() !!}
				</div>
			</div>
			<br>
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th>No.</th>
					       			<!-- <th><b>No agenda</b></th>
 -->	<!--  -->				    <th><b>No Surat</b></th>
  						       		<th><b>Perihal</b></th>
					       			<!-- <th><b>Jenis Surat</b></th>
					       			<th><b>Sifat Surat</b></th> -->
					       			<th><b>Tanggal Pengirim</b></th>
					       			<th><b>Tanggal Penerima</b></th>
					       			<th><b>Sumber</b></th>
					       			<th><b>Kategori</b></th>
					       			<th><b>File</b></th>
					       			<th><b>Status</b></th>
					       			<th class='text-center action'>Aksi</th>
								</tr>
							</thead>
							<tstatus>
						   		@foreach($surat_masuks as $i=>$surat_masuk)
					     			<tr>
					     		 		<td  class="nom-list">{{$start_page}}</td>
					         			<!-- <td> {{ $surat_masuk->no_agenda}} </td> -->     
					         			<td> {{ $surat_masuk->kode}}/{{ $surat_masuk->no_surat}} </td> <td> {{ $surat_masuk->perihal}} </td> 
					         			<td> {{ $surat_masuk->tgl_pengirim}} </td>     
					         			<td> {{ $surat_masuk->tgl_penerima}} </td>     
					         			<!-- <td> {{ $surat_masuk->jenis_surat}} </td>     
					         			<td> {{ $surat_masuk->sifat_surat}} </td>  -->    
					         			<td> {{ $surat_masuk->sumber}} </td>     
					         			<td> {{ $surat_masuk->nama_kategori}} </td> 
					         			<td> 
					         				<a href="{{asset('pdftotext/'.$surat_masuk->file_upload)}}"  target="_blank">{{$surat_masuk->file_upload}}</a>
					         			</td>  
		                                <td>
		                                	@if ($surat_masuk->status=== 1)
		   										Unread!
											@elseif ($surat_masuk->status=== 2)
											    Read!
											@else
		    									Disposisi!
											@endif
		                              </td>           
					         			<td class="fl-center">
					         			<ul>
											<li><a href='{{URL::action("admin\SuratMasukController@edit",array($surat_masuk->id))}}'><button class="btn btn-info btn-style">Ubah</button></a></li>
											<li><a href='{{URL::action("admin\SuratMasukController@show",array($surat_masuk->id))}}'><button class="btn btn-warning btn-style">Lihat</button></a></li>
											<li>
												<form id="delete_surat_masuk{{$surat_masuk->id}}" action='{{URL::action("admin\SuratMasukController@destroy",array($surat_masuk->id))}}' method="POST">
											    <input type="hidden" name="_method" value="delete">
											    <input type="hidden" name="_token" value="{{ csrf_token() }}">
											    <a href="#" onclick="document.getElementById('delete_surat_masuk{{$surat_masuk->id}}').submit();"><button class="btn btn-danger btn-style">Hapus</button></a>
											</form>
										</li>
									</ul>
									  	</td>
					     			</tr>
					     			<?php $start_page = $start_page+1 ?>
						   		@endforeach
							</tstatus>
						</table>
					</div>
					<div class="text-center">
					{!! $surat_masuks->render() !!}
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
@section('js')
<script>
	$( document ).ready(function() {
		var message = '{{session('flash-error')}}';
		if(message!=''){
			alert('{{session('flash-error')}}');
		}
	})
</script>
<script type="text/javascript">
	$('.datepicker').datepicker({
	    format: 'mm-dd-yyyy'
	});
</script>
@endsection