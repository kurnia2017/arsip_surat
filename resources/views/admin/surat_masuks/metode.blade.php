@extends('layouts.admin')

@section('content')
	<section>
	<div class="col-md-4">
    <h1>
      Data Surat Metode	
    </h1>
	</div>
	 @if(Auth::check())
	<div class='col-md-2 col-xs-10 pull-right m-b-10 m-t-25'>
		<a href="{{URL::to('/suratmasuk/create')}}" class="btn btn-primary wod-100"><i class="fa fa-plus-circle"></i>Tambah</a>
	</div>
    @endif
  </section>
	<section class="content">
	  <div class="row">
	    <div class="col-md-12">
	    	<div class="col-md-4 pull-right row">
				<div class="clearfix">
				{!! Form::open(['method'=>'GET','url'=>'searchsuratmasuk','role'=>'search'])  !!}
	          		<div class="input-surat_masuk custom-search-form input-group margin">
            			<input type="text" class="form-control" name="search" placeholder="Cari...">
		                    <span class="input-group-btn">
	       						<button class="btn btn-default" type="submit"><i class="fa fa-search"></i> Cari</button>
		                    </span>
	          		</div>
          		{!! Form::close() !!}
				</div>
			</div>
			<br>
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-striped table-hover">
							<thead>
								<tr>
									<th>No.</th>
					       			<th><b>Sifat</b></th>
					       			<th><b>sumber</b></th>
					       			<th><b>Jumlah</b></th>
					       			<th><b>Tahun</b></th>
								</tr>
							</thead>
							<tstatus>
					     			<tr>
					     		 		<td>1</td>
					         			<td> Segera </td>     
					         			<td> dinas pangan</td>     
					         			<td> {{ $dinas_pangan_segera }} </td>     
					         			<td> 2019 </td>     
					     			</tr>
							</tstatus>
							<tstatus>
					     			<tr>
					     		 		<td>2</td>
					         			<td> Segera </td>     
					         			<td> Diskom info</td>     
					         			<td> {{ $diskom_info_segera}} </td>     
					         			<td> 2018 </td>     
					     			</tr>
					     	<tstatus>
					     			<tr>
					     		 		<td>3</td>
					         			<td> Segera </td>     
					         			<td> Sekretariat Daerah</td>     
					         			<td> {{ $sekretariat_daerah_segera}} </td>     
					         			<td> 2018 </td>     
					     			</tr>
							</tstatus>
							<tstatus>
					     			<tr>
					     		 		<td>4</td>
					         			<td> Biasa </td>     
					         			<td>Sekretariat Daerah</td>     
					         			<td> {{ $sekretariat_daerah_biasa}} </td>        
					         			<td> 2017 </td>     
					     			</tr>
							</tstatus>
							<tstatus>
					     			<tr>
					     		 		<td>5</td>
					         			<td> Biasa</td>     
					         			<td> Walikota Bandung</td>     
					         			<td> {{ $walikota_bandung_biasa}} </td>     
					         			<td> 2019 </td>     
					     			</tr>
							</tstatus>
							<tstatus>
					     			<tr>
					     		 		<td>6</td>
					         			<td> Sangat Penting</td>     
					         			<td> Diskom info</td>     
					         			<td> {{ $diskom_info_sangat_penting}} </td>     
					         			<td> 2019 </td>     
					     			</tr>
							</tstatus>
							<tstatus>
					     			<tr>
					     		 		<td>7</td>
					         			<td> Sangat Penting</td>     
					         			<td> Walikota Bandung</td>     
					         			<td> {{ $walikota_sangat_penting}} </td>     
					         			<td> 2019 </td>     
					     			</tr>
							</tstatus>
							<tstatus>
					     			<tr>
					     		 		<td>8</td>
					         			<td> Penting</td>     
					         			<td> Sekretariat Daerah</td>     
					         			<td> {{ $sekretariat_penting}} </td>     
					         			<td> 2019 </td>     
					     			</tr>
							</tstatus>
							<tstatus>
					     			<tr>
					     		 		<td>9</td>
					         			<td> Penting</td>     
					         			<td> Kecamatan Buah batu</td>     
					         			<td> {{ $buahbatu_penting}} </td>     
					         			<td> 2019 </td>     
					     			</tr>
							</tstatus>
						</table>
					</div>
					<div class="text-center">
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
@section('js')
<script>
	$( document ).ready(function() {
		var message = '{{session('flash-error')}}';
		if(message!=''){
			alert('{{session('flash-error')}}');
		}
	})
</script>
@endsection