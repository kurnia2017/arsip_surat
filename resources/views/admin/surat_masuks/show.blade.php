@extends('layouts.admin')

@section('content')
  <section class="content-header">
    <h1>
      Show Surat Masuk
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><a href="{{URL::to('/suratmasuk/index')}}">Data Surat Masuk</a></li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <table class="table table-striped table-hover">
          <tr>
            <td>ID</td>
            <td>
              {{{$surat_masuks->id}}}
            </td>
          </tr>
<!--             <tr>
            <td width="200px">No Agenda</td>
            <td>
              {{{$surat_masuks->no_agenda}}}
            </td>
          </tr> -->
           <tr>
            <td width="200px">No Surat</td>
            <td>
              {{{$surat_masuks->kode}}}/{{{ $surat_masuks->no_surat}}}
            </td>
          </tr>
          <!-- <tr>
            <td width="200px">Jenis Surat</td>
            <td>
              {{{$surat_masuks->jenis_surat}}}
            </td>
          </tr> -->
          <!-- <tr>
            <td width="200px">Sifat Surat</td>
            <td>
              {{{$surat_masuks->sifat_surat}}}
            </td>
          </tr> -->
           <tr>
            <td width="200px">Tanggal Pengirim</td>
            <td>
              {{{$surat_masuks->tgl_pengirim}}}
            </td>
          </tr>
           <tr>
            <td width="200px">Tanggal Penerima</td>
            <td>
              {{{$surat_masuks->tgl_penerima}}}
            </td>
          </tr>
           <tr>
            <td width="200px">Sumber</td>
            <td>
              {{{$surat_masuks->sumber}}}
            </td>
          </tr>
          <!-- <tr>
            <td width="200px">No Telepon</td>
            <td>
              {{{$surat_masuks->no_telpon}}}
            </td>
          </tr> -->
          <tr>
            <td width="200px">Perihal</td>
            <td>
              {{{$surat_masuks->perihal}}}
            </td>
          </tr>
          <tr>
            <td width="200px">Lampiran</td>
            <td>
              {{{$surat_masuks->lampiran}}}
            </td>
          </tr>
          <!-- <tr>
            <td width="200px">Keterangan</td>
            <td>
              {{{$surat_masuks->keterangan}}}
            </td>
          </tr> -->
            <tr>
            <td width="200px">Kategori</td>
            <td>
              {{{$surat_masuks->kategori}}}
            </td>
          </tr>
          <tr>
            <td width="200px">Text Surat</td>
            <td>
              {{{$surat_masuks->file}}}
              <a href="{{URL::to('/file/pdftotext/'.$surat_masuks->file)}}" class="btn btn-large"><i class="icon-download-alt"> </i></a>
            </td>
          </tr>
        </table>
        <div class="col-md-12 text-center">
            <a href="{{URL::to('/suratmasuk/index')}}" class="btn btn-primary" role="button">Kembali</a>
          </div>
      </div>
    </div>
  </section>
@endsection