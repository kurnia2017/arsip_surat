@extends('layouts.admin')

@section('content')
  <section class="content-header">
    <h1>
      Tanggapan
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><a href="{{URL::to('/tanggapans/index')}}">Rincian Tanggapan</a></li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <table class="table table-striped table-hover">
          <tr>
            <td>ID</td>
            <td>
              {{{$tanggapans->id}}}
            </td>
          </tr>
          <tr>
            <td width="200px">Tanggapan</td>
            <td>
              {{{$tanggapans->tanggapan}}}
            </td>
          </tr>
        </table>
        <p align="center">
            <a href="{{URL::to('/tanggapan/index')}}" class="btn btn-primary" role="button">kembali</a>
          </p>
      </div>
    </div>
  </section>
@endsection