@extends('layouts.admin')

@section('content')
  <section class="content-header">
    <h1>
      User
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><a href="{{URL::to('/user')}}">Data User</a></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        {!! Form::open(['action' => 'admin\UserController@store']) !!}
          <div class='form-group clearfix'>
            {{ Form::label("kode_petugas", "Kode Petugas", ['class' => 'col-md-2 control-label']) }}
              <div class='col-md-4'>
                {{ Form::text("kode_petugas", '',['class' => 'form-control required']) }}
                <span class="error">{{$errors->first('kode_petugas')}}</span>
              </div>
          </div>
          <div class='form-group clearfix'>
            {{ Form::label("nama_belakang", "Nama", ['class' => 'col-md-2 control-label']) }}
              <div class='col-md-4'>
                {{ Form::text("nama_belakang", '',['class' => 'form-control required']) }}
                <span class="error">{{$errors->first('nama_belakang')}}</span>
              </div>
          </div>
          <div class='form-group clearfix'>
            {{ Form::label("email", "Email", ['class' => 'col-md-2 control-label']) }}
              <div class='col-md-4'>
                {{ Form::text("email", '',['class' => 'form-control required']) }}
                <span class="error">{{$errors->first('email')}}</span>
              </div>
          </div>
          <div class="form-group clearfix{{ $errors->has('password') ? ' has-error' : '' }}">
            <label for="password" class="col-md-2 control-label">Password</label>

                <div class="col-md-4">
                    <input id="password" type="password" class="form-control" name="password" required>

                      @if ($errors->has('password'))
                      <span class="help-block">
                         <strong>{{ $errors->first('password') }}</strong>
                      </span>
                @endif
                </div>
          </div>

          <div class="form-group clearfix">
            <label for="password-confirm" class="col-md-2 control-label">Confirm Password</label>

                <div class="col-md-4">
                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                 </div>
          </div>
          <div class="form-group clearfix">
          <label class="col-md-2 control-label">User Group</label>
                <input type="radio" id="r1" name="name" value="1" checked/>
                <label for="r1"><span></span>Admin Master</label>
                <input type="radio" id="r2" name="name" value="2"/>
                <label for="r2"><span></span>Sekretaris</label>
                <input type="radio" id="r2" name="name" value="3"/>
                <label for="r2"><span></span>Lurah</label>

            </div>
          <div class="form-group clearfix">
          <label class="col-md-2 control-label">Status</label>
                <input type="radio" id="r1" name="status" value="Aktif" checked/>
                <label for="r1"><span></span>Aktif</label>
                <input type="radio" id="r2" name="status" value="Non Aktif"/>
                <label for="r2"><span></span>Non Aktif</label>
          </div>
          <div class='form-group'>
            <div class='col-md-4 col-md-offset-2'>
              <button class='btn btn-primary' type='submit' name='save' id='save'><span class='glyphicon glyphicon-save'></span> Simpan</button>
            </div>
          </div>
        {!! Form::close() !!}    
      </div>
    </div>
  </section>
@endsection