@extends('layouts.admin')

@section('content')
  <section class="content-header">
    <h1>
      User
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active"><a href="{{URL::to('/user')}}">Data User</a></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
      	{!! Form::model($users,['method'=>'put','action'=>['admin\UserController@update',$users->id]]) !!}
	         <div class='form-group clearfix'>
            {{ Form::label("kode_petugas", "Kode Petugas", ['class' => 'col-md-2 control-label']) }}
              <div class='col-md-4'>
                {{ Form::text("kode_petugas", null,['class' => 'form-control required']) }}
                <span class="error">{{$errors->first('kode_petugas')}}</span>
              </div>
          </div>
          <div class='form-group clearfix'>
            {{ Form::label("nama_belakang", "Nama", ['class' => 'col-md-2 control-label']) }}
              <div class='col-md-4'>
                {{ Form::text("nama_belakang", null,['class' => 'form-control required']) }}
                <span class="error">{{$errors->first('nama_belakang')}}</span>
              </div>
          </div>
          <div class='form-group clearfix'>
            {{ Form::label("email", "Email", ['class' => 'col-md-2 control-label']) }}
              <div class='col-md-4'>
                {{ Form::text("email", null,['class' => 'form-control required']) }}
                <span class="error">{{$errors->first('email')}}</span>
              </div>
          </div>
          <div class='form-group clearfix'>
            {{ Form::label("name", "User Group", ['class' => 'col-md-2 control-label']) }}
              <div class='col-md-4'>
                {{ Form::text("name", null,['class' => 'form-control required']) }}
                <span class="error">{{$errors->first('name')}}</span>
              </div>
          </div>

          <div class='form-group clearfix'>
            {{ Form::label("status", "Status", ['class' => 'col-md-2 control-label']) }}
              <div class='col-md-4'>
                {{ Form::text("status", null,['class' => 'form-control required']) }}
                <span class="error">{{$errors->first('status')}}</span>
              </div>
          </div>
          <div class='form-group'>
            <div class='col-md-4 col-md-offset-2'>
              <button class='btn btn-primary' type='submit' name='save' id='save'><span class='glyphicon glyphicon-save'></span> Simpan</button>
            </div>
          </div>
      {!! Form::close() !!}
    	</div>
  	</div>
	</section>
@endsection