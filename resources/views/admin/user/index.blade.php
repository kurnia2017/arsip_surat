@extends('layouts.admin')

@section('content')
	<section>
	<div class="col-md-4">
    <h1>
      User
    </h1>
	</div>
		<div class='col-md-2 col-xs-12 pull-right m-b-10 m-t-25'>
		<a href="{{URL::to('/user/create')}}" class="btn btn-primary wod-100"><i class="fa fa-plus-circle"></i> Tambah Dokumen</a>
	</div>
  </section>
	<section class="content">
	  <div class="row">
	    <div class="col-md-12">
	    	<div class="col-md-4 pull-right row">
				<div class="clearfix">
				{!! Form::open(['method'=>'GET','url'=>'searchuser','role'=>'search'])  !!}
         			<div class="input-surat_masuk custom-search-form">
            			<input type="text" class="form-control" name="search" placeholder="Cari...">
              			<span class="input-surat_masuk-btn">
              				<span class="input-surat_masuk-btn">
			       				<button class="btn btn-default" type="submit"><i class="fa fa-search"></i> Cari</button>
			     			</span>
              			</span>
          			</div>
          		{!! Form::close() !!}
				</div>
			</div>
			<br>
				<table class="table table-striped table-hover">
					<thead>
						<tr>
							<th>No.</th>
			       			<th><b>Kode Petugas</b></th>
			       			<th><b>Nama</b></th>
			       			<th><b>Email</b></th>
			       			<th><b>Status</b></th>
			       			<th><b>User Group</b></th>
			       			<th class='text-center action'>Aksi</th>
					</thead>
					<tstatus>
				   		@foreach($users as $i=>$user)
			     			<tr>
			     		 		<td  class="nom-list">{{$start_page}}</td>
			         			<td> {{ $user->kode_petugas}} </td>     
			         			<td> {{ $user->name}} </td>     
			         			<td> {{ $user->email}} </td>     
			         			<td> {{ $user->status}} </td>    
			         			<td> {{ $user->nama}} </td>
			         			<td class="fl-center">
			         			<ul>
									<li><a href='{{URL::action("admin\UserController@edit",array($user->id))}}'><button class="btn btn-info btn-style">Ubah</button></a></li>
									<li><a href='{{URL::action("admin\UserController@show",array($user->id))}}'><button class="btn btn-warning btn-style">Lihat</button></a></li>
									<li>
										<form id="delete_user{{$user->id}}" action='{{URL::action("admin\UserController@destroy",array($user->id))}}' method="POST">
									    <input type="hidden" name="_method" value="delete">
									    <input type="hidden" name="_token" value="{{ csrf_token() }}">
									    <a href="#" onclick="document.getElementById('delete_user{{$user->id}}').submit();"><button class="btn btn-danger btn-style">Hapus</button></a>
										</form>
									</li>
								</ul>
							  	</td>    
			     			</tr>
			     			<?php $start_page = $start_page+1 ?>
				   		@endforeach
					</tstatus>
				</table>
				<div class="text-center">
				{!! $users->render() !!}
				</div>
			</div>
		</div>
	</section>
@endsection
@section('js')
<script>
	$( document ).ready(function() {
		var message = '{{session('flash-error')}}';
		if(message!=''){
			alert('{{session('flash-error')}}');
		}
	})
</script>
@endsection