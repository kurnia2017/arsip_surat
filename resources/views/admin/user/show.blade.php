@extends('layouts.admin')

@section('content')
  <section class="content-header">
    <h1>
        Lihat Data User
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active"><a href="{{URL::to('/user/index')}}">Data Lihat User</a></li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <table class="table table-striped table-hover">
          <tr>
            <td>ID</td>
            <td>
              {{{$users->id}}}
            </td>
          </tr>
          <tr>
            <td width="200px">Kode Petugas</td>
            <td>
              {{{$users->kode_petugas}}}
            </td>
          </tr>
          <tr>
            <td width="200px">Nama</td>
            <td>
              {{{$users->name}}}
            </td>
          </tr>
          <tr>
            <td width="200px">Email</td>
            <td>
              {{{$users->email}}}
            </td>
          </tr>         
          <tr>
            <td width="200px">Status</td>
            <td>
              {{{$users->status}}}
            </td>
          </tr>
        </table>
        <p align="center">
            <a href="{{URL::to('/user/index')}}" class="btn btn-primary" role="button">kembali</a>
          </p>
      </div>
    </div>
  </section>
@endsection