@extends('layouts.admin')

@section('content')
  <section class="content-header">
    <h1>
      List User
    </h1>
    <ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
  </section>
	<section class="content">
	  <div class="row">
	    <div class="col-md-12">
	    	 <div class='pull-right'>
				{!! Form::open(['method'=>'POST','action' => 'admin\UserGroupController@importexcel', 'files'=>true]) !!}
					<div class='form-group clearfix'>
						<div class='col-md-4'>
							<div class="input-group custom-search-form">
								<input type="file" class="form-control" name="file" placeholder="Search...">
								<span class="input-group-btn">
			                    	<span class="input-group-btn">
								    	<button class="btn btn-primary" type="submit"><i class=""></i> Import Excel</button>
								    </span>
			                	</span>
			            	</div>
						</div>
					</div>
				{!! Form::close() !!}
			</div>
				<br>
				<table class="table table-striped table-hover">
					<thead>
						<tr>
						 <th>No.</th>
				       <th><b>nama</b></th>
				       <th><b>status</b></th>
				       <th><b>Role</b></th>
						</tr>
					</thead>
					<tstatus>
				   @foreach($users as $i=>$user)
			     <tr>
			     	<td>{{$start_page}}</td>
			         <td> {{ $user->nama }} </td>
			         <td> {{ $user->status }} </td>
			         <td> {{ $user->name }} </td>	         
			     	</tr>
			     	<?php $start_page = $start_page+1 ?>
				   @endforeach
					</tstatus>
				</table>
				{!! $users->render() !!}
			</div>
		</div>
	</section>
@endsection