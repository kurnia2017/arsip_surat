@extends('layouts.admin')

@section('content')
<section>
	<div class="col-md-4">
    <h1>
      User Groups
    </h1>
	</div>
	<div class='col-md-2 col-xs-12 pull-right m-b-10 m-t-25'>
		<a href="{{URL::to('/user-groups/create')}}" class="btn btn-primary wod-100"><i class="fa fa-plus-circle"></i> Tambah Data</a>
	</div>
  </section>
	<section class="content">
	  <div class="row">
	    <div class="col-md-12">
	    	<div class="col-md-4 pull-right row">
				<div class="clearfix">
					{!! Form::open(['method'=>'GET','url'=>'search-user-groups','role'=>'search'])  !!}
	                <div class="input-group custom-search-form">
	                  <input type="text" class="form-control" name="search" placeholder="Cari...">
	                    <span class="input-group-btn">
	                    	<span class="input-group-btn">
			       				<button class="btn btn-default" type="submit"><i class="fa fa-search"></i> Cari</button>
		     				</span>
	                    </span>
	                </div>
      				{!! Form::close() !!}
					</div>
				</div>
				<div class="table-responsive">
					<table class="table table-striped table-hover">
						<thead>
							<tr>
							 <th>No.</th>
				       <th><b>User</b></th>
				       <th><b>Group</b></th>
				       <th class='text-center' style='width:70px'>Aksi</th>
							</tr>
						</thead>
						<tbody>
					   @foreach($user_groups as $i=>$usergroup)
				     <tr>
			     		 <td class="nom-list">{{$start_page}}</td>
				         <td> {{ $usergroup->user->name }} </td>
				         <td> {{ $usergroup->group->name }} </td>	         
				         <td class="wid-30-per fl-left" style="width: 150px;">
				         	<ul>
								<li><a href='{{URL::action("admin\UserGroupController@edit",array($usergroup->id))}}'><button class="btn btn-info btn-style">Ubah</button></a></li>
								<li><a href='{{URL::action("admin\UserGroupController@show",array($usergroup->id))}}'><button class="btn btn-warning btn-style">Lihat</button></a></li>
								<li>
									<form id="delete_usergroup{{$usergroup->id}}" action='{{URL::action("admin\UserGroupController@destroy",array($usergroup->id))}}' method="POST">
							    	<input type="hidden" name="_method" value="delete">
							    	<input type="hidden" name="_token" value="{{ csrf_token() }}">
							    	<a href="#" onclick="document.getElementById('delete_usergroup{{$usergroup->id}}').submit();"><button class="btn btn-danger btn-style">Hapus</button></a>
									</form>
								</li>
							</ul>
						  </td>
				     	</tr>
				     	<?php $start_page = $start_page+1 ?>
					   @endforeach
						</tbody>
					</table>
				</div>
				<div class="text-center">
				{!! $user_groups->render() !!}
				</div>
			</div>
		</div>
	</section>
@endsection