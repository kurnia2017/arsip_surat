@extends('layouts.admin')

@section('content')
  <section class="content-header">
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <h2>Kategori Surat<label></label></h2>
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-12">
          <div class="col-lg-4">
            <div class="small-box bg-ungu">
              <div class="inner">
                <h3>{{{$jumlah_umum}}}</h3>

                <!-- <p>UMUM</p> -->
                <a href="{{URL::to('kategori/umum')}}" class="small-box-footer">UMUM <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              <div class="icon">
                <i class="fa fa-envelope-o"></i>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="small-box bg-abu">
              <div class="inner">
                <h3>{{{$jumlah_pemerintahan}}}</h3>

                  <a href="{{URL::to('kategori/pemerintahan')}}" class="small-box-footer">PEMERINTAHAN <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              <div class="icon">
                <i class="fa fa-envelope-o"></i>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="small-box bg-ungutua">
              <div class="inner">
                <h3>{{{$jumlah_politik}}}</h3>

                 <a href="{{URL::to('kategori/politik')}}" class="small-box-footer">POLITIK <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              <div class="icon">
                <i class="fa fa-envelope-o"></i>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="col-lg-4">
            <div class="small-box bg-birumuda">
              <div class="inner">
                <h3>{{{$jumlah_keamanan}}}</h3>

                <a href="{{URL::to('kategori/keamanan_dan_ketertiban')}}" class="small-box-footer">KEAMANAN DAN KETERTIBAN <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              <div class="icon">
                <i class="fa fa-envelope-o"></i>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="small-box bg-merahmuda">
              <div class="inner">
                <h3>{{{$jumlah_kesejahteraan}}}</h3>

                <a href="{{URL::to('kategori/kesejahtraan_rakyat')}}" class="small-box-footer">KESEJAHTERAAN RAKYAT <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              <div class="icon">
                <i class="fa fa-envelope-o"></i>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="small-box bg-hijaumuda">
              <div class="inner">
                <h3>{{{$jumlah_perekonomian}}}</h3>

                <a href="{{URL::to('kategori/prekonomian')}}" class="small-box-footer">PEREKONOMIAN <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              <div class="icon">
                <i class="fa fa-envelope-o"></i>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="col-lg-4">
            <div class="small-box bg-maroon">
              <div class="inner">
                <h3>{{{$jumlah_pekerjaanumum}}}</h3>

                <a href="{{URL::to('kategori/pekerjaan_umum_dan_ketenagaan')}}" class="small-box-footer">PEKERJAAN UMUM DAN KETENAGAAN <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              <div class="icon">
                <i class="fa fa-envelope-o"></i>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="small-box bg-aqua">
              <div class="inner">
                <h3>{{{$jumlah_pengawasan}}}</h3>

                <a href="{{URL::to('kategori/pengawasan')}}" class="small-box-footer">PENGAWASAN <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              <div class="icon">
                <i class="fa fa-envelope-o"></i>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="small-box bg-hijau">
              <div class="inner">
                <h3>{{{$jumlah_kepegawaian}}}</h3>

               <a href="{{URL::to('kategori/kepegawaian')}}" class="small-box-footer">KEPEGAWAIAN <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              <div class="icon">
                <i class="fa fa-envelope-o"></i>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12">
          <div class="col-lg-4">
          </div>
          <div class="col-lg-4">
            <div class="small-box bg-kuninghijau">
              <div class="inner">
                <h3>{{{$jumlah_keuangan}}}</h3>

                 <a href="{{URL::to('kategori/keuangan')}}" class="small-box-footer">KEUANGAN <i class="fa fa-arrow-circle-right"></i></a>
              </div>
              <div class="icon">
                <i class="fa fa-envelope-o"></i>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
          </div>
        </div>
        </div>
        <div class="text-center">
        </div>
    </div>
  </section>
@endsection