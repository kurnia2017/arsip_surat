<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Dokumen Manajemen Sistem | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <link rel="icon" href="/img/vavicon.png" type="image/x-icon">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  {!! Html::style('css/bootstrap.min.css') !!}
  <!-- Font Awesome -->
  {!! Html::style('css/font-awesome.css') !!}
  <!-- Ionicons -->
  {!! Html::style('css/ionicons.min.css') !!}
  <!-- Theme style -->
  {!! Html::style('css/AdminLTE.min.css') !!}
  {!! Html::style('css/AdminLTE.min.css') !!}
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  {!! Html::style('css/_all-skins.min.css') !!}
  <!-- Morris chart -->
  {!! Html::style('css/morris.css') !!}
  <!-- jvectormap -->
  {!! Html::style('css/jquery-jvectormap.css') !!}
  <!-- Date Picker -->
  {!! Html::style('css/bootstrap-datepicker.min.css') !!}
  {!! Html::style('css/jquery.datetimepicker.css') !!}
  <!-- Daterange picker -->
  {!! Html::style('css/daterangepicker.css') !!}
  <!-- bootstrap wysihtml5 - text editor -->
  {!! Html::style('css/bootstrap3-wysihtml5.min.css') !!}
  {!! Html::style('css/bootstrap-colorpicker.min.css') !!}
  {!! Html::style('css/customadmn.css') !!}

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <!-- Global site tag (gtag.js) - Google Analytics -->
  <script async src="https://www.googletagmanager.com/gtag/js?id=UA-88229911-2"></script>
  <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-88229911-2');
  </script>
  <style>
#chartdiv {
  width: 100%;
  height: 500px;
}

</style>
  <style type="text/css">
    .table tbody td a + form {
        display: inline;
        padding-left: 12px;
        
    }
    
    .table tbody td > a:first-child {
      padding-left: 0;
    }

    .table tbody td > a {
        padding-left: 16px;
        float: none;
        margin: auto;
    }
    
    .table tbody td:last-child {
        text-align: center;
    }

    .section-pool, *[data-name='pool_choose']{ display: none; }
    .form-filter {
        width: 987px;
    }

    .form-filter  .form-control {
        float: left;
        margin-right: 4px;
        width: 200px;
    }    

    .form-filter .input-group-btn {
        float: left;
        width: auto;
    }
  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <input type="hidden" name="_token" value="{{ csrf_token() }}">
  <header class="main-header">
    <!-- Logo -->
    <a href="/admin-dashboard" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini">
        <img src="/images/logo.png" width="30">
      </span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg">DMS</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="/front/img/vavicon.png" class="user-image" alt="User Image">
              <span class="hidden-xs">{{ Auth::user()->name }}</span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header pad-20-i">
                <img src="/front/img/vavicon.png" class="img-circle" alt="User Image">
                  <p>
                    {{ Auth::user()->name }}
                  </p>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-right wod-100">
                  <a href="{{ url('/logout') }}" class="btn btn-default btn-flat  wod-100"
                      onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                      <i class="fa fa-power-off "></i> <span> Logout </span>
                  </a>
                  <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                      {{ csrf_field() }}
                  </form>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>

          <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <!-- Notifications: style can be found in dropdown.less -->
          <!-- Tasks: style can be found in dropdown.less -->
        </ul>
      </div>
    </nav>
  </header>
  <!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="/front/img/vavicon.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>{{ Auth::user()->name }}</p>
          <p>{{ Auth::user()->user }}</p>
        </div>
      </div>

      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
    @if(Auth::check())
    @if(Auth::user()->groups()->where("name", "=", "Admin Master")->first())
        <li class="header">MAIN NAVIGATION</li>
       <li class="treeview">
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Master Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="{{URL::to('/groups')}}"><i class="fa fa-circle-o"></i> Groups </a></li>
            <li><a href="{{URL::to('/group-roles')}}"><i class="fa fa-circle-o"></i> Groups Roles</a></li>
            <li><a href="{{URL::to('/user-groups')}}"><i class="fa fa-circle-o"></i> User Groups</a></li>
          </ul>
        </li>
     
        <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>Sekretaris</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          <li><a href="{{URL::to('/suratmasuk/index')}}"><i class="fa fa-circle-o"></i>Surat Masuk</a></li>
          <li><a href="{{URL::to('/SuratKeluar/index')}}"><i class="fa fa-circle-o"></i>Surat Keluar</a></li>
          <li><a href="{{URL::to('/document')}}"><i class="fa fa-circle-o"></i>Kategori Surat</a></li>
          <li><a href="{{URL::to('/memo/index')}}"><i class="fa fa-circle-o"></i>Notifikasi</a></li>
          </ul>
          </li>
        </li>
      <li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span>Lurah</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          <li><a href="{{URL::to('/lurah/index')}}"><i class="fa fa-circle-o"></i>Surat Masuk</a></li>
          <li><a href="{{URL::to('/lurah/index')}}"><i class="fa fa-circle-o"></i>Surat Keluar</a></li>
          <li><a href="{{URL::to('/document')}}"><i class="fa fa-circle-o"></i>Kategori Surat</a></li>
          </ul>
          <li class="treeview">
            <a href="#">
              <i class="fa fa-bar-chart"></i> <span>Laporan</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-right pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
             <li><a href="{{URL::to('/laporan-suratmasuk/index')}}"><i class="fa fa-circle-o"></i>Laporan Surat Masuk</a></li>
            <li><a href="{{URL::to('/laporan-suratkeluar/index')}}"><i class="fa fa-circle-o"></i>Laporan Surat Keluar</a></li>
            <li><a href="{{URL::to('/laporan-disposisi/index')}}"><i class="fa fa-circle-o"></i>Laporan Disposisi</a></li>
            </ul>
      @elseif(Auth::user()->groups()->where("name", "=", "Sekretaris")->first())
      <li class="treeview">
          <a href="#">
            <i class="fa fa-file-text"></i> <span>Kelola Dokumen</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          <li><a href="{{URL::to('/suratmasuk/index')}}"><i class="fa fa-circle-o"></i>Data Surat Masuk</a></li>
          <li><a href="{{URL::to('/SuratKeluar/index')}}"><i class="fa fa-circle-o"></i>Data Surat Keluar</a></li>
          <li><a href="{{URL::to('/document')}}"><i class="fa fa-circle-o"></i>Kategori Surat</a></li>
          </ul>
      </li>
<!--             <li class="treeview">
          <a href="#">
            <i class="fa fa-envelope-o"></i> <span>Surat Keluar</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          <li><a href="{{URL::to('/SuratKeluar/create')}}"><i class="fa fa-circle-o"></i>Entri Baru</a></li>
          </ul>
      </li> -->
               <li class="treeview">
          <a href="#">
            <i class="fa fa-pencil-square-o"></i> <span>Kelola Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          <li><a href="{{URL::to('/user/index')}}"><i class="fa fa-circle-o"></i>User</a></li>
          <li><a href="{{URL::to('/penanggung_jawab/index')}}"><i class="fa fa-circle-o"></i>PenanggungJawab Pengelola</a></li>
          <li><a href="{{URL::to('/bagian/index')}}"><i class="fa fa-circle-o"></i>Bagian</a></li>
<!--           <li><a href="{{URL::to('/jenis_surat/index')}}"><i class="fa fa-circle-o"></i>Jenis Surat</a></li>
          <li><a href="{{URL::to('/sifat_surat/index')}}"><i class="fa fa-circle-o"></i>Sifat Surat</a></li> -->
          <li><a href="{{URL::to('/kategori/index')}}"><i class="fa fa-circle-o"></i>Kategori</a></li>
          <li><a href="{{URL::to('/list_kategori/index')}}"><i class="fa fa-circle-o"></i>List Kategori</a></li>
          <li><a href="{{URL::to('/tanggapan/index')}}"><i class="fa fa-circle-o"></i>Tanggapan</a></li>
          <li><a href="{{URL::to('/referensies/index')}}"><i class="fa fa-circle-o"></i>Referensi</a></li>
          </ul>
          </li>
        </li>
        <!-- <li class="treeview">
          <a href="{{URL::to('/memo/index')}}">
            <i class="fa fa-comments"></i> <span>Notifikasi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu"> -->
          <li><a href="{{URL::to('/memo/index')}}"><i class="fa fa-comments"></i>Data Notifikasi</a></li>
<!--           <li><a href="{{URL::to('/document')}}"><i class="fa fa-comments"></i>Data Memo</a></li> -->
          <!-- </ul>
        </li> -->
          <li class="treeview">
          <a href="#">
            <i class="fa fa-bar-chart"></i> <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          <li><a href="{{URL::to('/laporan-suratmasuk/index')}}"><i class="fa fa-circle-o"></i>Laporan Surat Masuk</a></li>
          <li><a href="{{URL::to('/laporan-suratkeluar/index')}}"><i class="fa fa-circle-o"></i>Laporan Surat Keluar</a></li>
          <li><a href="{{URL::to('/laporan-disposisi/index')}}"><i class="fa fa-circle-o"></i>Laporan Disposisi</a></li>
          </ul>
        </li>
         @else
          <li class="treeview">
          <a href="#">
            <i class="fa fa-envelope" ></i> <span>Kelola Dokumen</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
          <li><a href="{{URL::to('/lurah/index')}}"><i class="fa fa-circle-o"></i>Data Surat Masuk</a></li>
          <li><a href="{{URL::to('/lurahsuratkeluar/index')}}"><i class="fa fa-circle-o"></i>Data Surat Keluar</a></li>
          <li><a href="{{URL::to('/document')}}"><i class="fa fa-circle-o"></i>Kategori Surat</a></li>
        </li>
      </ul>
<!--     <li class="treeview">
          <a href="#">
            <i class="fa fa-envelope-o" ></i> <span>Surat Keluar</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
           <ul class="treeview-menu">
      </ul>
    </li> -->
       <li class="treeview">
          <a href="#">
            <i class="fa fa-bar-chart" ></i> <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-right pull-right"></i>
            </span>
          </a>
           <ul class="treeview-menu">
          <li><a href="{{URL::to('/laporan-suratmasuk/index')}}"><i class="fa fa-circle-o"></i>Laporan Surat Masuk</a></li>
          <li><a href="{{URL::to('/laporan-suratkeluar/index')}}"><i class="fa fa-circle-o"></i>Laporan Surat Keluar</a></li>
          <li><a href="{{URL::to('/laporan-disposisi/index')}}"><i class="fa fa-circle-o"></i>Laporan Disposisi</a></li>
      </ul>
    </li>
      @endif
      @endif
      </ul>
      </ul>
      </ul>

    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    @if (Session::has('flash-success'))
      <div>
        <div class="alert alert-success alert-dismissible" role="alert">
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <p>{{ Session::get('flash-success') }}</p>
        </div>
      </div>
    @endif
    <!-- Content Header (Page header) -->
    <section class="content-header">
      @yield('content')
    </section>
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2018 <a href="#">DMS</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <!-- <div class="control-sidebar-bg"></div> -->
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
{!! Html::script('js/jquery-2.2.3.min.js') !!}
<!-- jQuery UI 1.11.4 -->
{!! Html::script('js/jquery-ui.min.js') !!}
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>

<script src="https://www.amcharts.com/lib/4/core.js"></script>
<script src="https://www.amcharts.com/lib/4/charts.js"></script>
<script src="https://www.amcharts.com/lib/4/themes/animated.js"></script>
<!-- Bootstrap 3.3.7 -->
{!! Html::script('js/bootstrap.js') !!}
<!-- Morris.js charts -->
{!! Html::script('js/raphael.min.js') !!}
<!-- {!! Html::script('js/morris.min.js') !!} -->
<!-- Sparkline -->
<!-- {!! Html::script('js/jquery.sparkline.min.js') !!} -->
<!-- jvectormap -->
<!-- jQuery Knob Chart -->
<!-- {!! Html::script('js/jquery.knob.min.js') !!} -->
<!-- daterangepicker -->
<!-- {!! Html::script('js/moment.min.js') !!} -->
{!! Html::script('js/daterangepicker.js') !!}
<!-- datepicker -->
{!! Html::script('js/bootstrap-datepicker.min.js') !!}
<!-- Bootstrap WYSIHTML5 -->
<!-- {!! Html::script('js/bootstrap3-wysihtml5.all.min.js') !!} -->
<!-- Slimscroll -->
<!-- {!! Html::script('js/jquery.slimscroll.min.js') !!} -->
<!-- FastClick -->
<!-- {!! Html::script('js/fastclick.js') !!} -->
<!-- AdminLTE App -->
{!! Html::script('js/adminlte.min.js') !!}
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- AdminLTE for demo purposes -->
<!-- {!! Html::script('js/demo.js') !!} -->
{!! Html::script('js/jquery.min.js') !!}
{!! Html::script('js/select2.full.min.js') !!}
{!! Html::script('js/jquery.inputmask.js') !!}
{!! Html::script('js/moment.min.js') !!}
{!! Html::script('js/bootstrap-datepicker.min.js') !!}
{!! Html::script('js/jquery.datetimepicker.full.min.js') !!}
{!! Html::script('js/bootstrap-colorpicker.min.js') !!}
{!! Html::script('js/icheck.min.js') !!}
{!! Html::script('js/moment.min.js') !!}
{!! Html::script('js/printus.js') !!}
@yield('js')



</body>
</html>
 