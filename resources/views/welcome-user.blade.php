@extends('layouts.admin')

@section('content')
  <section class="content-header">
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
      	<h2>Welcome <label> {{ Auth::user()->name }}</label></h2>
        Selamat datang di aplikasi Dokumen Manajemen Sistem, Anda login dengan level user {{ Auth::user()->user }}. Terimakasih.
    	</div>
  	</div>
	</section>
    <section class="content">
    <div class="row">
        <div class="col-md-12">
          <div class="col-lg-4">
            <div class="small-box bg-aqua">
              <div class="inner">
                <h3>{{$surat_masuk}}</h3>

                <p>Surat Masuk</p>
              </div>
              <div class="icon">
                <i class="fa fa-envelope"></i>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="small-box bg-green">
              <div class="inner">
                <h3>{{$surat_keluar}}</h3>

                <p>Surat Keluar</p>
              </div>
              <div class="icon">
                <i class="fa fa-envelope-o"></i>
              </div>
            </div>
          </div>
          <div class="col-lg-4">
            <div class="small-box bg-yellow">
              <div class="inner">
                <h3>{{$disposisi}}</h3>

                <p>Disposisi Surat</p>
              </div>
              <div class="icon">
                <i class="fa fa-share-square"></i>
              </div>
            </div>
          </div>
        </div>
        </div>
        <div class="text-center">
        </div>
    </div>
  </section>
@endsection
@section('js')
<script>
  // Themes begin
  am4core.useTheme(am4themes_animated);
  // Themes end

  var chart = am4core.create("chartdiv", am4charts.PieChart);
  chart.hiddenState.properties.opacity = 0; // this creates initial fade-in

  chart.data = [
    {
      country: "Surat Masuk",
      value: {{$surat_masuk}}
    },
    {
      country: "Surat Keluar",
      value: {{$surat_keluar}}
    },
    {
      country: "Disposisi",
      value: {{$disposisi}}
    }
  ];
  chart.radius = am4core.percent(70);
  chart.innerRadius = am4core.percent(40);
  chart.startAngle = 180;
  chart.endAngle = 360;  

  var series = chart.series.push(new am4charts.PieSeries());
  series.dataFields.value = "value";
  series.dataFields.category = "country";

  series.slices.template.cornerRadius = 10;
  series.slices.template.innerCornerRadius = 7;
  series.slices.template.draggable = true;
  series.slices.template.inert = true;
  series.alignLabels = false;

  series.hiddenState.properties.startAngle = 90;
  series.hiddenState.properties.endAngle = 90;

  chart.legend = new am4charts.Legend();
</script>
@endsection
