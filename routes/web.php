<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});


// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();


Route::get('/printreal','PrintController@printreal');
Route::get('/prnpriview','PrintController@prnpriview');
Route::get('/prnsuratkeluar','PrintController@prnsuratkeluar');
Route::get('/prndisposisi','PrintController@prndisposisi');

Route::get('/admin-dashboard', 'HomeController@index')->name('admin-dashboard')->middleware('auth');
Route::get('/home', 'HomeController@index')->name('admin-dashboard')->middleware('auth');

Route::resource('user-groups', 'admin\UserGroupController');
Route::get('search-user-groups','admin\UserGroupController@search');
Route::resource('groups', 'admin\GroupController');
Route::get('search-groups','admin\GroupController@search');
Route::resource('group-roles', 'admin\GroupRoleController');
Route::get('search-group-roles','admin\GroupRoleController@search');
Route::resource('roles', 'admin\RoleController');
Route::get('search-roles','admin\RoleController@search');

//kode surat
Route::get('/kode_surat/index','admin\KodeSuratController@index');
Route::get('/kode_surat/create',['as'=>'create','uses'=>'admin\KodeSuratController@create']);
Route::get('/kode_surat/edit/{id}','admin\KodeSuratController@edit');
Route::put('/kode_surat/update/{id}','admin\KodeSuratController@update');
Route::delete('/kode_surat/destroy/{id}','admin\KodeSuratController@destroy');
Route::post('/kode_surat/store',['as'=>'store','uses'=>'admin\KodeSuratController@store']);
Route::get('/searchkode_surat', ['as' => 'searchkode_surat', 'uses' => 'admin\KodeSuratController@search']);
Route::get('/kode_surat/show/{id}','admin\KodeSuratController@show');

//suratmasuk
Route::get('/suratmasuk/metode','admin\SuratMasukController@metode');

Route::get('/suratmasuk/index','admin\SuratMasukController@index');
Route::get('/suratmasuk/show/{id}','admin\SuratMasukController@show');
Route::get('/suratmasuk/create',['as'=>'create','uses'=>'admin\SuratMasukController@create']);
Route::get('/suratmasuk/edit/{id}','admin\SuratMasukController@edit');
Route::put('/suratmasuk/update/{id}','admin\SuratMasukController@update');
Route::delete('/suratmasuk/destroy/{id}','admin\SuratMasukController@destroy');
Route::post('/suratmasuk/store',['as'=>'store','uses'=>'admin\SuratMasukController@store']);
Route::get('/searchsuratmasuk', ['as' => 'searchsuratmasuk', 'uses' => 'admin\SuratMasukController@search']);
Route::get('/searchsuratmasuktgl', ['as' => 'searchsuratmasuk', 'uses' => 'admin\SuratMasukController@searchtgl']);
Route::get('/searchsuratmasukkategori', ['as' => 'searchsuratmasukkategori', 'uses' => 'admin\SuratMasukController@searchkategori']);

Route::get('/suratmasuk/index','admin\SuratMasukController@index');
Route::get('/downloadimage/image/{file}','admin\SuratMasukController@download');
Route::get('/downloadimage/images/{file}','admin\SuratKeluarController@download');


//SuratKeluar
Route::get('/SuratKeluar/index','admin\SuratKeluarController@index');
Route::get('/SuratKeluar/show/{id}','admin\SuratKeluarController@show');
Route::get('/SuratKeluar/create',['as'=>'create','uses'=>'admin\SuratKeluarController@create']);
Route::get('/SuratKeluar/edit/{id}','admin\SuratKeluarController@edit');
Route::put('/SuratKeluar/update/{id}','admin\SuratKeluarController@update');
Route::delete('/SuratKeluar/destroy/{id}','admin\SuratKeluarController@destroy');
Route::post('/SuratKeluar/store',['as'=>'store','uses'=>'admin\SuratKeluarController@store']);
Route::get('/searchSuratKeluar', ['as' => 'searchSuratKeluar', 'uses' => 'admin\SuratKeluarController@search']);
Route::get('/searchSuratKeluars', ['as' => 'searchSuratKeluar', 'uses' => 'admin\SuratKeluarController@searchs']);
Route::get('/searchSuratKeluartgl', ['as' => 'searchSuratKeluar', 'uses' => 'admin\SuratKeluarController@searchss']);
Route::get('/searchsuratkeluarkategori', ['as' => 'searchsuratkeluarkategori', 'uses' => 'admin\SuratKeluarController@searchkategori']);

//user
Route::get('/user/index','admin\UserController@index');
Route::get('/user/show/{id}','admin\UserController@show');
Route::get('/user/create',['as'=>'create','uses'=>'admin\UserController@create']);
Route::get('/user/edit/{id}','admin\UserController@edit');
Route::put('/user/update/{id}','admin\UserController@update');
Route::delete('/user/destroy/{id}','admin\UserController@destroy');
Route::post('/user/store',['as'=>'store','uses'=>'admin\UserController@store']);
Route::get('/searchuser', ['as' => 'searchuser', 'uses' => 'admin\UserController@search']);

//lurahsuratmasuk
Route::get('/lurah/index','admin\LurahController@index');
Route::get('/lurah/disposisi/{id}','admin\LurahController@edit');
Route::put('/lurah/disposisi/{id}','admin\LurahController@update');
Route::get('/lurah/show/{id}','admin\LurahController@show');
Route::get('/searchlurah', ['as' => 'searchlurah', 'uses' => 'admin\LurahController@search']);
Route::get('/searchlurahkategori', ['as' => 'searchlurahkategori', 'uses' => 'admin\LurahController@searchkategory']);
Route::get('/searchlurahsuratkeluaraaa', ['as' => 'searchlurahsuratkeluaraaa', 'uses' => 'admin\LurahController@searchlurahsuratkeluar']);


Route::put('/lurah/update1/{id}','admin\LurahController@prosesreas');
Route::put('/lurah/update/{id}','admin\LurahController@proses');

//lurahsuratkeluar
Route::get('/lurahsuratkeluar/index','admin\LurahController@index1');
Route::get('/lurahsuratkeluar/show/{id}','admin\LurahController@show1');
Route::get('/searchlurahsuratkeluar', ['as' => 'searchlurahsuratkeluar', 'uses' => 'admin\LurahController@search1']);

//Bagian
Route::get('/bagian/index','admin\BagianController@index');
Route::get('/bagian/show/{id}','admin\BagianController@show');
Route::get('/bagian/create',['as'=>'create','uses'=>'admin\BagianController@create']);
Route::get('/bagian/edit/{id}','admin\BagianController@edit');
Route::put('/bagian/update/{id}','admin\BagianController@update');
Route::delete('/bagian/destroy/{id}','admin\BagianController@destroy');
Route::post('/bagian/store',['as'=>'store','uses'=>'admin\BagianController@store']);
Route::get('/searchbagian', ['as' => 'searchbagian', 'uses' => 'admin\BagianController@search']);

//disposisi
Route::post('/disposisi/store/{id}',['as'=>'store','uses'=>'admin\DisposisiController@store']);

//memo
Route::get('/memo/index','admin\MemoController@index');
Route::get('/memo/read','admin\MemoController@read');
Route::get('/memo/unread','admin\MemoController@unread');
Route::get('/memo/detailread/{id}','admin\MemoController@detailread');
Route::get('/memo/detailunread/{id}','admin\MemoController@detailunread');
Route::put('/memo/detailunread/{id}','admin\MemoController@prosesreas');
Route::get('/searchmemo', ['as' => 'searchmemo', 'uses' => 'admin\MemoController@wkwk']);

Route::get('/download-memo/{id}','HomeController@memounreadPrint');

//laporan Surat Masuk
Route::get('/laporan-suratmasuk/index', ['as' => 'User', 'uses' => 'admin\SuratMasukController@laporansurat']);
Route::get('/searchlaporansuratmasuk', ['as' => 'searchlaporansuratmasuk', 'uses' => 'admin\SuratMasukController@searchlaporansurat']);
Route::get('/searchlaporansuratkeluar', ['as' => 'searchlaporansuratkeluar', 'uses' => 'admin\SuratKeluarController@searchlaporansurat']);
Route::get('/searchlaporandisposisis', ['as' => 'searchlaporandisposisi', 'uses' => 'admin\DisposisiController@searchlaporandisposisi']);



//laporan Surat Keluar
Route::get('/laporan-suratkeluar/index','admin\SuratKeluarController@laporansurat');


//laporan Diposisi
Route::get('/laporan-disposisi/index','admin\DisposisiController@laporansurat');
Route::get('/searchlaporandisposisi', ['as' => 'searchlaporandisposisi', 'uses' => 'admin\DisposisiController@search']);

//penanggungjawab pengelola
Route::get('/penanggung_jawab/index','admin\PenanggungJawabController@index');
Route::get('/penanggung_jawab/show/{id}','admin\PenanggungJawabController@show');
Route::get('/penanggung_jawab/edit/{id}','admin\PenanggungJawabController@edit');
Route::put('/penanggung_jawab/update/{id}','admin\PenanggungJawabController@update');
Route::delete('/penanggung_jawab/destroy/{id}','admin\PenanggungJawabController@destroy');
Route::post('/penanggung_jawab/store',['as'=>'store','uses'=>'admin\PenanggungJawabController@store']);
Route::get('/penanggung_jawab/create',['as'=>'create','uses'=>'admin\PenanggungJawabController@create']);
Route::get('/searchpenanggung_jawab', ['as' => 'searchpenanggungjawab_jawab', 'uses' => 'admin\PenanggungJawabController@search']);

//jenis surat
Route::get('/jenis_surat/index','admin\JenisSuratController@index');
Route::get('/jenis_surat/show/{id}','admin\JenisSuratController@show');
Route::get('/jenis_surat/create',['as'=>'create','uses'=>'admin\JenisSuratController@create']);
Route::get('/jenis_surat/edit/{id}','admin\JenisSuratController@edit');
Route::put('/jenis_surat/update/{id}','admin\JenisSuratController@update');
Route::delete('/jenis_surat/destroy/{id}','admin\JenisSuratController@destroy');
Route::post('/jenis_surat/store',['as'=>'store','uses'=>'admin\JenisSuratController@store']);
Route::get('/searchjenis_surat', ['as' => 'searchjenis_surat', 'uses' => 'admin\JenisSuratController@search']);

//kategori
Route::get('/kategori/index','admin\KategoriController@index');
Route::get('/kategori/show/{id}','admin\KategoriController@show');
Route::get('/kategori/create',['as'=>'create','uses'=>'admin\KategoriController@create']);
Route::get('/kategori/edit/{id}','admin\KategoriController@edit');
Route::put('/kategori/update/{id}','admin\KategoriController@update');
Route::delete('/kategori/destroy/{id}','admin\KategoriController@destroy');
Route::post('/kategori/store',['as'=>'store','uses'=>'admin\KategoriController@store']);
Route::get('/searchkategori', ['as' => 'searchkategori', 'uses' => 'admin\KategoriController@search']);

//list kategori
Route::get('/list_kategori/index','admin\ListKategoriController@index');
Route::get('/list_kategori/show/{id}','admin\ListKategoriController@show');
Route::get('/list_kategori/create',['as'=>'create','uses'=>'admin\ListKategoriController@create']);
Route::get('/list_kategori/edit/{id}','admin\ListKategoriController@edit');
Route::put('/list_kategori/update/{id}','admin\ListKategoriController@update');
Route::delete('/list_kategori/destroy/{id}','admin\ListKategoriController@destroy');
Route::post('/list_kategori/store',['as'=>'store','uses'=>'admin\ListKategoriController@store']);
Route::get('/searchlist_kategori', ['as' => 'searchlist_kategori', 'uses' => 'admin\ListKategoriController@search']);


//ajact
Route::get('/ajax-kategoris', ['as' => 'store', 'uses' =>'admin\ListKategoriController@ajax_getcitbyprov']);

//tanggapan
Route::get('/tanggapan/index','admin\TanggapanController@index');
Route::get('/tanggapan/show/{id}','admin\TanggapanController@show');
Route::get('/tanggapan/create',['as'=>'create','uses'=>'admin\TanggapanController@create']);
Route::get('/tanggapan/edit/{id}','admin\TanggapanController@edit');
Route::put('/tanggapan/update/{id}','admin\TanggapanController@update');
Route::delete('/tanggapan/destroy/{id}','admin\TanggapanController@destroy');
Route::post('/tanggapan/store',['as'=>'store','uses'=>'admin\TanggapanController@store']);
Route::get('/searchtanggapan', ['as' => 'searchtanggapan', 'uses' => 'admin\TanggapanController@search']);

//sifat surat
Route::get('/sifat_surat/index','admin\SifatSuratController@index');
Route::get('/sifat_surat/show/{id}','admin\SifatSuratController@show');
Route::get('/sifat_surat/create',['as'=>'create','uses'=>'admin\SifatSuratController@create']);
Route::get('/sifat_surat/edit/{id}','admin\SifatSuratController@edit');
Route::put('/sifat_surat/update/{id}','admin\SifatSuratController@update');
Route::delete('/sifat_surat/destroy/{id}','admin\SifatSuratController@destroy');
Route::post('/sifat_surat/store',['as'=>'store','uses'=>'admin\SifatSuratController@store']);
Route::get('/searchsifat_surat', ['as' => 'searchsifat_surat', 'uses' => 'admin\SifatSuratController@search']);
Route::get('/sifat_surat/biasa','admin\SifatSuratController@biasa');
Route::get('/sifat_surat/sangat_penting','admin\SifatSuratController@sangat_penting');
Route::get('/sifat_surat/penting','admin\SifatSuratController@penting');
Route::get('/sifat_surat/segera','admin\SifatSuratController@segera');

//documen
Route::get('/document','admin\MemoController@document');
// Route::get('/kategori','admin\MemoController@kategori');
// Route::post('/kategori',['as'=>'store','uses'=>'admin\MemoController@kategori']);
Route::put('/kategori','admin\MemoController@kategori');

//perkategory
Route::get('/kategori/umum','admin\SearchController@umum');
Route::get('/kategori/pemerintahan','admin\SearchController@pemerintahan');
Route::get('/kategori/politik','admin\SearchController@politik');
Route::get('/kategori/keamanan_dan_ketertiban','admin\SearchController@keamanan_dan_ketertiban');
Route::get('/kategori/kesejahtraan_rakyat','admin\SearchController@kesejahtraan_rakyat');
Route::get('/kategori/prekonomian','admin\SearchController@prekonomian');
Route::get('/kategori/pekerjaan_umum_dan_ketenagaan','admin\SearchController@pekerjaan_umum_dan_ketenagaan');
Route::get('/kategori/pengawasan','admin\SearchController@pengawasan');
Route::get('/kategori/kepegawaian','admin\SearchController@kepegawaian');
Route::get('/kategori/keuangan','admin\SearchController@keuangan');

//

//search_perkategori
Route::get('/search_umum', ['as' => 'search_umum', 'uses' => 'admin\SearchController@search_umum']);
Route::get('/search_pemerintahan', ['as' => 'search_pemerintahan', 'uses' => 'admin\SearchController@search_pemerintahan']);
Route::get('/search_politik', ['as' => 'search_politik', 'uses' => 'admin\SearchController@search_politik']);
Route::get('/search_keamanan_dan_ketertiban', ['as' => 'search_keamanan_dan_ketertiban', 'uses' => 'admin\SearchController@search_keamanan_dan_ketertiban']);
Route::get('/search_kesejahtraan_rakyat', ['as' => 'search_kesejahtraan_rakyat', 'uses' => 'admin\SearchController@search_kesejahtraan_rakyat']);
Route::get('/search_prekonomian', ['as' => 'search_prekonomian', 'uses' => 'admin\SearchController@search_prekonomian']);
Route::get('/search_pekerjaan_umum_dan_ketenagaan', ['as' => 'search_pekerjaan_umum_dan_ketenagaan', 'uses' => 'admin\SearchController@search_pekerjaan_umum_dan_ketenagaan']);
Route::get('/search_pengawasan', ['as' => 'search_pengawasan', 'uses' => 'admin\SearchController@search_pengawasan']);
Route::get('/search_kepegawaian', ['as' => 'search_kepegawaian', 'uses' => 'admin\SearchController@search_kepegawaian']);
Route::get('/search_keuangan', ['as' => 'search_keuangan', 'uses' => 'admin\SearchController@search_keuangan']);

//referensi
Route::get('/referensies/index','admin\ReferensiController@index');
Route::get('/referensies/show/{id}','admin\ReferensiController@show');
Route::get('/referensies/create',['as'=>'create','uses'=>'admin\ReferensiController@create']);
Route::get('/referensies/edit/{id}','admin\ReferensiController@edit');
Route::put('/referensies/update/{id}','admin\ReferensiController@update');
Route::delete('/referensies/destroy/{id}','admin\ReferensiController@destroy');
Route::post('/referensies/store',['as'=>'store','uses'=>'admin\ReferensiController@store']);
Route::get('/searchreferensies', ['as' => 'searchreferensies', 'uses' => 'admin\ReferensiController@search']);